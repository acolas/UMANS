#ifndef LIB_IF_SOURCE_H
#define LIB_IF_SOURCE_H

#include <vector>

class InteractionField;
class Vector2D;

enum IFSourceType { AgentSource, ObstacleSource };

struct IFSourceReference
{
	IFSourceType objectType;
	size_t objectID;

	IFSourceReference(IFSourceType type, size_t id) : objectType(type), objectID(id) {}
};

struct IFWithLinkObjects
{
	InteractionField* field;
	std::vector<IFSourceReference> linkObjects;
};

class IFSource
{

private:
	///<summary>List of the velocity interaction fields for which this object is the source.</summary>
	std::vector<IFWithLinkObjects> interactionFieldsVelocity_;
	///<summary>List of the orientation interaction fields for which this object is the source.</summary>
	std::vector<IFWithLinkObjects> interactionFieldsOrientation_;

public:
	virtual const Vector2D& getPosition() const = 0;
	virtual const Vector2D& getViewingDirection() const = 0;
	virtual const Vector2D& getVelocity() const = 0;
	virtual const float& getParameter() const = 0;

	inline const std::vector<IFWithLinkObjects>& getInteractionFieldsVelocity() const { return interactionFieldsVelocity_; }
	inline const std::vector<IFWithLinkObjects>& getInteractionFieldsOrientation() const { return interactionFieldsOrientation_; }

	inline void addInteractionFieldVelocity(IFWithLinkObjects& intField) { interactionFieldsVelocity_.push_back(intField); }
	inline void addInteractionFieldOrientation(IFWithLinkObjects& intField) { interactionFieldsOrientation_.push_back(intField); }

};

#endif


