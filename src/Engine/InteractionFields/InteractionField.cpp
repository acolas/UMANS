//#define _USE_MATH_DEFINES

#include "InteractionField.h"

#include <cmath>
#include <math.h>

#include <core/worldBase.h>
#include <core/agent.h>
#include <tools/vectormatrix.h>
#include <InteractionFields/Cell.h>

#include <memory>
#include <array>
#include <random>

#include <iostream>
#include <sstream>
#include <fstream>

using namespace std;

InteractionField::~InteractionField()
{
	for (auto& matrix : matrices_)
		delete matrix.second;
	matrices_.clear();
}

InteractionField::InteractionField(const InteractionField::Settings& set, size_t id)
	: settings_(set), id_(id) {}



Vector2D InteractionField::computeVectorAtPosition(const Vector2D& position, const IFSource* source, const std::vector<IFSourceReference>& linkObjects, const WorldBase* world, bool& result_pointInsideIF) const
{
	// find the matrix to use, or the two matrices to interpolate between
	const InteractionFieldMatrix *matrix0 = nullptr, *matrix1 = nullptr;
	float mu;
	getMatricesForInterpolation(source, matrix0, matrix1, mu, world, linkObjects);

	// compute the result vector for matrix0
	const Vector2D& result0 = _computeVectorAtPosition(matrix0, position, source, linkObjects, world, result_pointInsideIF);
	
	// if applicable, compute the result vector for matrix1, and return a linear interpolation of the two
	if (matrix1 != nullptr)
	{
		const Vector2D& result1 = _computeVectorAtPosition(matrix1, position, source, linkObjects, world, result_pointInsideIF);
		return (1 - mu) * result0 + mu * result1;
	}

	// otherwise, just return the single result
	return result0;
}

//function create for the dll (communication with unity) that sends thecurrent parameter used for the parametric fields
float InteractionField::getParameterValue(const IFSource* source, const WorldBase* world, const std::vector<IFSourceReference>& linkObjects) const {
	// for parametric IFs, get the parameter value to currently use
	float parameterValue = 0;
	if (settings_.parameter_ == ParameterType::SourceSpeed) {
		//if the source is an obstacle and the parameter is the speed, we use the speed of the linked object if agent
		if (dynamic_cast<const Obstacle*>(source) != nullptr) {
			size_t nrLinks = linkObjects.size();
			for (size_t i = 0; i < nrLinks; ++i) {
				const IFSourceReference& linkObjectRef = linkObjects[i];
				const IFSource* linkObject = nullptr;
				if (linkObjectRef.objectType == IFSourceType::AgentSource)
					parameterValue = world->GetAgent_const(linkObjectRef.objectID)->getVelocity().magnitude();
				else
					linkObject = world->GetObstacle_const(linkObjectRef.objectID);

			}
		}
		else {
			parameterValue = source->getVelocity().magnitude();
		}
	}

	else if (settings_.parameter_ == ParameterType::ManualParameter)
		parameterValue = source->getParameter();
	// find the matrix (or two matrices) to use for this parameter value
	return parameterValue;
}

void InteractionField::getMatricesForInterpolation(const IFSource* source, const InteractionFieldMatrix*& result0, const InteractionFieldMatrix*& result1, float& result_mu, const WorldBase* world, const std::vector<IFSourceReference>& linkObjects) const
{
	// for parametric IFs, get the parameter value to currently use
	float parameterValue = 0;
	if (settings_.parameter_ == ParameterType::SourceSpeed) {
		//if the source is an obstacle and the parameter is the speed, we use the speed of the linked object if agent
		if (dynamic_cast<const Obstacle*>(source) != nullptr) {
			size_t nrLinks = linkObjects.size();
			for (size_t i = 0; i < nrLinks; ++i){
				const IFSourceReference& linkObjectRef = linkObjects[i];
				const IFSource* linkObject = nullptr;
				if (linkObjectRef.objectType == IFSourceType::AgentSource)
					parameterValue = world->GetAgent_const(linkObjectRef.objectID)->getVelocity().magnitude();
				else
					linkObject = world->GetObstacle_const(linkObjectRef.objectID);

			}
		}
		else {
			parameterValue = source->getVelocity().magnitude();
		}
	}
		
	else if (settings_.parameter_ == ParameterType::ManualParameter)
		parameterValue = source->getParameter();
	// find the matrix (or two matrices) to use for this parameter value
	for (std::map<float, const InteractionFieldMatrix*>::const_iterator it = matrices_.begin(); it != matrices_.end(); ++it)
	{
		float paramMax = it->first;
		if (parameterValue <= paramMax)
		{
			// check if we should interpolate between this matrix and the previous
			if (parameterValue < paramMax && it != matrices_.begin())
			{
				const auto& prev = std::prev(it);
				float paramMin = prev->first;
				result0 = prev->second;
				result1 = it->second;
				result_mu = (parameterValue - paramMin) / (paramMax - paramMin);
			}
			
			// if there's no interpolation to be done, use only this matrix
			else
			{
				result0 = it->second;
				result1 = nullptr;
				result_mu = 0;
			}

			return;
		}
	}

	// parameter is larger than the max; just use the last matrix
	result0 = matrices_.rbegin()->second;
	result1 = nullptr;
	result_mu = 0;
}

Vector2D InteractionField::_computeVectorAtPosition(const InteractionFieldMatrix* field, const Vector2D& position, const IFSource* source, const std::vector<IFSourceReference>& linkObjects, const WorldBase* world, bool& result_pointInsideIF) const
{

	float IFRotation = getRotationAngleFromLinks(field, source, linkObjects, world);

	const Vector2D& gridPosition = convertPositionToGridCoordinates(field, position, source->getPosition(), IFRotation);
	result_pointInsideIF = field->matrix_.isInside(gridPosition.x, gridPosition.y);
	if (!result_pointInsideIF)
		return Vector2D(0, 0);

	// find the 4 grid points to interpolate between; in edge cases, x0 and x1 (or y0 and y1) might be the same.
	int x0 = (int)floor(gridPosition.x);
	int y0 = (int)floor(gridPosition.y);
	int x1 = (int)ceil(gridPosition.x);
	int y1 = (int)ceil(gridPosition.y);

	// if we are at the border of the grid, there are fewer cells to interpolate between.
	// This is easiest to simulate by just treating x0 and x1 (or y0 and y1) as the same value.
	if (x0 < 0) x0 = x1;
	if (x1 >= field->matrix_.getCols()) x1 = x0;
	if (y0 < 0) y0 = y1;
	if (y1 >= field->matrix_.getRows()) y1 = y0;

	// compute interpolation weights
	float weightX = gridPosition.x - x0;
	float weightY = gridPosition.y - y0;

	// first interpolate horizontally between the bottom 2 points, and between the top 2 points
	const Vector2D& result_bottom = (1 - weightX) * field->matrix_.at(y0, x0) + weightX * field->matrix_.at(y0, x1);
	const Vector2D& result_top = (1 - weightX) * field->matrix_.at(y1, x0) + weightX * field->matrix_.at(y1, x1);

	// then interpolate vertically between these two results
	const Vector2D& result_middle = (1 - weightY) * result_bottom + weightY * result_top;

	// rotate back
	return rotateCounterClockwise(result_middle, -IFRotation);


}




Vector2D InteractionField::_computeVectorAtPositionContinuus(const InteractionFieldMatrix* field, const Vector2D& position, const IFSource* source, const std::vector<IFSourceReference>& linkObjects, const WorldBase* world, bool& result_pointInsideIF) const
{


	float IFRotation = getRotationAngleFromLinks(field, source, linkObjects, world);

	const Vector2D& gridPosition = convertPositionToGridCoordinates(field, position, source->getPosition(), IFRotation);
	const Vector2D& rotatedPosition = rotateCounterClockwiseAroundPoint(position, source->getPosition(), IFRotation);
	const Vector2D& translatedPosition = field->sourcePosition_ + (rotatedPosition - source->getPosition());

	result_pointInsideIF = field->matrix_.isInside(gridPosition.x, gridPosition.y);
	array<int, 2> indice{ (int)floor(gridPosition.x), (int)floor(gridPosition.y) };
	Vector2D result = Vector2D(0, 0);
	for (auto &ind : field->zeroArea_) {
		if (indice == ind)
			return result;
	}
	
	if (!result_pointInsideIF)
		return result;

	float p = 1.85f;
	if (field->controlVectors_.empty()) {

		
		return Vector2D(0, 0);
	}
	else {

		Vector2D num(0, 0);
		float denom = 0.0;

				for (int k = 0; k < field->controlVectors_.size(); k++) {
					const Vector2D& vectorControl = field->controlVectors_[k];
					const Vector2D& vectControlPosition = field->controlVectorPos_[k];;
					float w = pow(1 / float((vectControlPosition - translatedPosition).magnitude()), p);
					denom += w;
					num = num + w * vectorControl;
				}

		result = num / denom;


	}

	// rotate back
	return  rotateCounterClockwise(result, -IFRotation);
	

	
}


Vector2D InteractionField::convertPositionToGridCoordinates(const InteractionFieldMatrix* field, const Vector2D& position, const Vector2D& sourcePosition, const float IFRotation) const
{
	// compute the agent's coordinates in the IF grid:
	// - get the position relative to the source
	const Vector2D& posDiff = position - sourcePosition;
	const Vector2D& posDiff_rotated = rotateCounterClockwise(posDiff, IFRotation);
	// - add it to the source coordinates, knowing that grid cells are not necessarily 1x1 meter,
	//   and that the matrix stores vectors for cell *centers* rather than cell *corners*
	float resultX = (field->sourcePosition_.x + posDiff_rotated.x) / field->cellWidth_ - 0.5f;
	float resultY = field->matrix_.getRows() - (field->sourcePosition_.y + posDiff_rotated.y) / field->cellHeight_ - 0.5f;

	return Vector2D(resultX, resultY);
}

float InteractionField::getRotationAngleFromLinks(const InteractionFieldMatrix* field, const IFSource* source, const std::vector<IFSourceReference>& linkObjects, const WorldBase* world) const
{
	// TODO: How to combine multiple links? Right now we use only the first link to compute a rotation angle.

	const Vector2D unitVectorDown(0, -1);
	const Vector2D& sourcePosition = source->getPosition();

	size_t nrLinks = std::min(field->links_.size(), linkObjects.size());
	for (size_t i = 0; i < nrLinks; ++i)
	{
		const Link& link = field->links_[i];
		const IFSourceReference& linkObjectRef = linkObjects[i];
		const IFSource* linkObject = nullptr;
		if (linkObjectRef.objectType == IFSourceType::AgentSource)
			linkObject = world->GetAgent_const(linkObjectRef.objectID);
		else
			linkObject = world->GetObstacle_const(linkObjectRef.objectID);

		// if this link is an angular relation with another object, use the current angle with that object
		if (linkObject != nullptr && link.getType() == Link::LinkType::Rotation)
			return link.getRotationAngle(source->getPosition(), linkObject->getPosition());
	}

	// otherwise, use the viewing angle that the source currently has
	return clockwiseAngle(unitVectorDown, source->getViewingDirection());
}




float InteractionField::getRotationAngle(const IFSource* source, const std::vector<IFSourceReference>& linkObjects, const WorldBase* world) const
{
	// find the matrix to use, or the two matrices to interpolate between
	const InteractionFieldMatrix *matrix0 = nullptr, *matrix1 = nullptr;
	float mu;
	float angle;
	getMatricesForInterpolation(source, matrix0, matrix1, mu, world, linkObjects);
	const Vector2D unitVectorDown(0, -1);
	// if there is only one matrix to use, visualize it in a straight-forward way
	if (matrix1 == nullptr)
		angle = getRotationAngleFromLinks(matrix0, source, linkObjects, world);

	// otherwise, draw an "interpolated matrix" that lies between matrix0 and matrix1.
	// The grid rectangle is an interpolation of matrix0 and matrix1, which may have different sizes.
	// We cannot draw one of these matrices directly. Instead, we should create a new grid and fill it with interpolated values.
	else{
		// - the rotation is interpolated between matrix0 and matrix1
		float angle0 = getRotationAngleFromLinks(matrix0, source, linkObjects, world);
		float angle1 = getRotationAngleFromLinks(matrix1, source, linkObjects, world);
		angle = (1 - mu)*angle0 + mu * angle1;
	}

	return angle;
}
IFVisualizationGrid InteractionField::buildVisualizationGrid(const IFSource* source, const std::vector<IFSourceReference>& linkObjects, const WorldBase* world) const
{
	// find the matrix to use, or the two matrices to interpolate between
	const InteractionFieldMatrix *matrix0 = nullptr, *matrix1 = nullptr;
	float mu;
	getMatricesForInterpolation(source, matrix0, matrix1, mu, world, linkObjects);

	// if there is only one matrix to use, visualize it in a straight-forward way
	if (matrix1 == nullptr)
		return buildVisualizationGrid_singleMatrix(matrix0, source, linkObjects, world);

	// otherwise, draw an "interpolated matrix" that lies between matrix0 and matrix1.
	// The grid rectangle is an interpolation of matrix0 and matrix1, which may have different sizes.
	// We cannot draw one of these matrices directly. Instead, we should create a new grid and fill it with interpolated values.

	// - the rotation is interpolated between matrix0 and matrix1
	float angle0 = getRotationAngleFromLinks(matrix0, source, linkObjects, world);
	float angle1 = getRotationAngleFromLinks(matrix1, source, linkObjects, world);
	float angle = (1 - mu)*angle0 + mu * angle1;

	// - the grid's origin is interpolated as well
	const Vector2D& sourcePosition = (1 - mu)*matrix0->sourcePosition_ + mu * matrix1->sourcePosition_;
	const Vector2D& currentSourcePosition = source->getPosition();
	const Vector2D& translation = currentSourcePosition - sourcePosition;

	// - the grid's width/height is interpolated as well
	float w = (1 - mu)*matrix0->GetWidth() + mu * matrix1->GetWidth();
	float h = (1 - mu)*matrix0->GetHeight() + mu * matrix1->GetHeight();
	int cols = (int)round((1 - mu)*matrix0->matrix_.getCols() + mu * matrix1->matrix_.getCols());
	int rows = (int)round((1 - mu)*matrix0->matrix_.getRows() + mu * matrix1->matrix_.getRows());
	float cellWidth = w / cols;
	float cellHeight = h / rows;

	IFVisualizationGrid grid;
	grid.gridPoints.resize(rows, std::vector<IFVisualizationGridPoint>(cols));

	// fill in all grid points
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
		{
			const Vector2D basePosition((j + 0.5f) * cellWidth, (rows - i - 0.5f) * cellHeight);
			const Vector2D& rotatedPosition = rotateCounterClockwiseAroundPoint(basePosition, sourcePosition, -angle);
			const Vector2D& worldPosition = rotatedPosition + translation;

			// compute the interpolated IF vector at this position
			bool inside;
			const Vector2D& IFVector =
				(1 - mu)*_computeVectorAtPosition(matrix0, worldPosition, source, linkObjects, world, inside)
				+ mu * _computeVectorAtPosition(matrix1, worldPosition, source, linkObjects, world, inside);

			grid.gridPoints[i][j] = { IFVector, worldPosition };
		}
	}

	// create the boundary points
	Vector2D corner(0, 0);
	grid.boundary.push_back(rotateCounterClockwiseAroundPoint(corner, sourcePosition, -angle) + translation);
	grid.boundary.push_back(rotateCounterClockwiseAroundPoint(corner + Vector2D(w, 0), sourcePosition, -angle) + translation);
	grid.boundary.push_back(rotateCounterClockwiseAroundPoint(corner + Vector2D(w, h), sourcePosition, -angle) + translation);
	grid.boundary.push_back(rotateCounterClockwiseAroundPoint(corner + Vector2D(0, h), sourcePosition, -angle) + translation);

	return grid;

}

IFVisualizationGrid InteractionField::buildVisualizationGrid_singleMatrix(const InteractionFieldMatrix* field, const IFSource* source, const std::vector<IFSourceReference>& linkObjects, const WorldBase* world) const
{
	const VectorMatrix* matrix_ = &field->matrix_;

	float angle = getRotationAngleFromLinks(field, source, linkObjects, world);

	const Vector2D& currentSourcePosition = source->getPosition();
	const Vector2D& translation = currentSourcePosition - field->sourcePosition_;

	IFVisualizationGrid grid;
	grid.gridPoints.resize(matrix_->getRows(), std::vector<IFVisualizationGridPoint>(matrix_->getCols()));

	// fill in all grid points
	for (int i = 0; i < matrix_->getRows(); ++i)
	{
		for (int j = 0; j < matrix_->getCols(); ++j)
		{
			const Vector2D basePosition((j + 0.5f) * field->cellWidth_, (matrix_->getRows() - i - 0.5f) * field->cellHeight_);
			const Vector2D& rotatedPosition = rotateCounterClockwiseAroundPoint(basePosition, field->sourcePosition_, -angle);
			const Vector2D& worldPosition = rotatedPosition + translation;

			const Vector2D& baseIFVector = matrix_->at(i, j);
			const Vector2D& rotatedIFVector = rotateCounterClockwise(baseIFVector, -angle);

			grid.gridPoints[i][j] = { rotatedIFVector, worldPosition };
		}
	}

	// create the boundary points
	Vector2D corner(0, 0);
	grid.boundary.push_back(rotateCounterClockwiseAroundPoint(corner, field->sourcePosition_, -angle) + translation);
	grid.boundary.push_back(rotateCounterClockwiseAroundPoint(corner + Vector2D(field->GetWidth(), 0), field->sourcePosition_, -angle) + translation);
	grid.boundary.push_back(rotateCounterClockwiseAroundPoint(corner + Vector2D(field->GetWidth(), field->GetHeight()), field->sourcePosition_, -angle) + translation);
	grid.boundary.push_back(rotateCounterClockwiseAroundPoint(corner + Vector2D(0, field->GetHeight()), field->sourcePosition_, -angle) + translation);

	return grid;
}


IFVisualizationGrid InteractionField::buildVisualizationGridFromControlVectors_singleMatrix(const InteractionFieldMatrix* field, const IFSource* source, const std::vector<IFSourceReference>& linkObjects, const WorldBase* world) const
{
	const VectorMatrix* matrix_ = &field->matrix_;

	float angle = getRotationAngleFromLinks(field, source, linkObjects, world);

	const Vector2D& currentSourcePosition = source->getPosition();
	const Vector2D& translation = currentSourcePosition - field->sourcePosition_;

	IFVisualizationGrid grid;
	grid.gridPoints.resize(matrix_->getRows(), std::vector<IFVisualizationGridPoint>(matrix_->getCols()));

	// fill in all grid points
	float p = 1.85f;
	for (int i = 0; i < matrix_->getRows(); ++i)
	{
		for (int j = 0; j < matrix_->getCols(); ++j)
		{

			const Vector2D basePosition((j + 0.5f) * field->cellWidth_, (matrix_->getRows() - i - 0.5f) * field->cellHeight_);

			Vector2D num(0, 0);
			float denom = 0.0;
			for (int k = 0; k < field->controlVectors_.size(); k++) {

				Vector2D vectorControl = field->controlVectors_[k];
				Vector2D vectControlPosition = field->controlVectorPos_[k];
				float w = pow(1 / float((vectControlPosition - basePosition).magnitude()), p);
				denom += w;
				num = num + w * vectorControl;
			}
			const Vector2D& result = num / denom;

			const Vector2D& rotatedPosition = rotateCounterClockwiseAroundPoint(basePosition, field->sourcePosition_, -angle);
			const Vector2D& worldPosition = rotatedPosition + translation;

			const Vector2D& baseIFVector = result;
			const Vector2D& rotatedIFVector = rotateCounterClockwise(baseIFVector, -angle);

			grid.gridPoints[i][j] = { rotatedIFVector, worldPosition };
		}
	}

	// create the boundary points
	Vector2D corner(0, 0);
	grid.boundary.push_back(rotateCounterClockwiseAroundPoint(corner, field->sourcePosition_, -angle) + translation);
	grid.boundary.push_back(rotateCounterClockwiseAroundPoint(corner + Vector2D(field->GetWidth(), 0), field->sourcePosition_, -angle) + translation);
	grid.boundary.push_back(rotateCounterClockwiseAroundPoint(corner + Vector2D(field->GetWidth(), field->GetHeight()), field->sourcePosition_, -angle) + translation);
	grid.boundary.push_back(rotateCounterClockwiseAroundPoint(corner + Vector2D(0, field->GetHeight()), field->sourcePosition_, -angle) + translation);

	return grid;
}

bool InteractionField::loadMatrixFromFile(const string& filename, float parameterValue)
{
	if (filename.empty())
		return false;

	// open the file
	ifstream myfile(filename);
	if (!myfile.is_open())
	{
		cerr << "Error: Unable to open InteractionField matrix file \"" << filename << "\"." << endl
			<< "This matrix will not be loaded." << endl;
		return false;
	}

	// --- read the first line: contains the number or rows and columns
	string line;
	if (!getline(myfile, line))
	{
		cerr << "Error: Unable to read 1st line of InteractionField matrix file \"" << filename << "\"." << endl
			<< "This matrix will not be loaded." << endl;
		return false;
	}
	auto tokens = HelperFunctions::SplitString(line, ';');

	int rows = HelperFunctions::ParseInt(tokens[0]);
	int cols = HelperFunctions::ParseInt(tokens[1]);

	// prepare a result with a VectorMatrix of this size
	InteractionFieldMatrix* result = new InteractionFieldMatrix(rows, cols);

	// --- new file version: the first line also contains the width and height of a cell,
	//     and the second line contains the source position and all links
	bool newFileType = (tokens.size() >= 4);
	if (newFileType)
	{
		result->cellHeight_ = HelperFunctions::ParseFloat(tokens[2]);
		result->cellWidth_ = HelperFunctions::ParseFloat(tokens[3]);

		// read the second line: contains the source object's position, and possibly information about links
		if (!getline(myfile, line))
		{
			cerr << "Error: Unable to read 1st line of InteractionField matrix file \"" << filename << "\"." << endl
				<< "This matrix will not be loaded." << endl;
			return false;
		}
		tokens = HelperFunctions::SplitString(line, ';');

		// source position
		result->sourcePosition_ = Vector2D(HelperFunctions::ParseFloat(tokens[0]), HelperFunctions::ParseFloat(tokens[1]));
		result->namefileMatrice_ = filename;
		// read the links one by one
		size_t index = 2;
		while (index + 2 < tokens.size())
		{
			// link type
			Link::LinkType linkType;
			if (tokens[index] == "r")
				linkType = Link::Rotation;
			else if (tokens[index] == "s")
				linkType = Link::Scaling;
			else if (tokens[index] == "t")
				linkType = Link::Translation;

			// object position
			Vector2D otherObjectPosition(HelperFunctions::ParseFloat(tokens[index+1]), HelperFunctions::ParseFloat(tokens[index + 2]));

			// store this link in the result
			result->links_.push_back(Link(linkType, result->sourcePosition_, otherObjectPosition)); 

			index += 3;
		}
	}

	// read the vectors one by one
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			if (getline(myfile, line))
			{
				const auto& tokens = HelperFunctions::SplitString(line, ';'); 

				// --- old file version: "s" indicates the the source is on this grid cell, 
				//     and it is followed by information about links
				if (tokens[0] == "s")
				{
					// the matrix should store the zero vector here
					result->matrix_.setAt(i, j, Vector2D(0, 0));

					// convert this grid coordinate to a position for the source object
					result->sourcePosition_ = Vector2D(j * result->cellWidth_, (rows - i) * result->cellHeight_);

					int index = 1;
					while (index < tokens.size()) 
					{
						// load a link
						Link::LinkType linkType;
						Vector2D otherObjectPosition(0,0);
						if (tokens[index] == "r")
						{
							linkType = Link::Rotation;

							// we don't know the exact position of the other object, but we can guess it from the angle
							float angle = HelperFunctions::ParseFloat(tokens[index + 4]);
							otherObjectPosition = result->sourcePosition_ + rotateCounterClockwise(Vector2D(0, -10), angle);
						}
						else if (tokens[index] == "s")
							linkType = Link::Scaling;
						else if (tokens[index] == "t")
							linkType = Link::Translation;

						// store this link in the result
						result->links_.push_back(Link(linkType, result->sourcePosition_, otherObjectPosition));

						index += 5;
					}
				}

				// vector
				else
				{
					Vector2D xy(stof(tokens[0]), stof(tokens[1]));
					// for old IF files, clamp the vector to a maximum magnitude (because vectors in those files are very big)
					if (!newFileType)
						xy = clampVector(xy, 1.8f);
					float mag = xy.magnitude();
					result->matrix_.setAt(i, j, xy);
				}
			}
		}
	}

	myfile.close();

	// store the result
	matrices_[parameterValue] = result;

	return true;
}

bool InteractionField::loadMatrixFromFileQuick(const string& filename, float parameterValue)
{
	
	if (filename.empty())
		return false;

	// open the file
	ifstream myfile(filename);
	if (!myfile.is_open())
	{
		cerr << "Error: Unable to open InteractionField matrix file \"" << filename << "\"." << endl
			<< "This matrix will not be loaded." << endl;
		return false;
	}

	// --- read the first line: contains the number or rows and columns
	string line;
	if (!getline(myfile, line))
	{
		cerr << "Error: Unable to read 1st line of InteractionField matrix file \"" << filename << "\"." << endl
			<< "This matrix will not be loaded." << endl;
		return false;
	}
	
	auto tokens = HelperFunctions::SplitString(line, ';');

	int rows = HelperFunctions::ParseInt(tokens[0]);
	int cols = HelperFunctions::ParseInt(tokens[1]);

	// prepare a result with a VectorMatrix of this size
	InteractionFieldMatrix* result = new InteractionFieldMatrix(rows, cols);
	result->namefileMatrice_ = filename;
	// --- new file version: the first line also contains the width and height of a cell,
	//     and the second line contains the source position and all links
	bool newFileType = (tokens.size() >= 4);
	int numberLine;
	if (newFileType)
	{
		result->cellHeight_ = HelperFunctions::ParseFloat(tokens[2]);
		result->cellWidth_ = HelperFunctions::ParseFloat(tokens[3]);

		// read the second line: contains the source object's position, and possibly information about links
		if (!getline(myfile, line))
		{
			cerr << "Error: Unable to read 1st line of InteractionField matrix file \"" << filename << "\"." << endl
				<< "This matrix will not be loaded." << endl;
			return false;
		}
		tokens = HelperFunctions::SplitString(line, ';');

		// source position
		result->sourcePosition_ = Vector2D(HelperFunctions::ParseFloat(tokens[0]), HelperFunctions::ParseFloat(tokens[1]));

		// read the links one by one
		size_t index = 2;
		while (index + 2 < tokens.size())
		{
			// link type
			Link::LinkType linkType;
			if (tokens[index] == "r")
				linkType = Link::Rotation;
			else if (tokens[index] == "s")
				linkType = Link::Scaling;
			else if (tokens[index] == "t")
				linkType = Link::Translation;

			// object position
			Vector2D otherObjectPosition(HelperFunctions::ParseFloat(tokens[index + 1]), HelperFunctions::ParseFloat(tokens[index + 2]));

			// store this link in the result
			result->links_.push_back(Link(linkType, result->sourcePosition_, otherObjectPosition));

			index += 3;
		}
	}

	//read the number of control vectors
	if (getline(myfile, line))
		numberLine =  stoi(line, nullptr, 10);
	// read the curve one by one
	for (int i = 0; i < numberLine; i++)
	{
		if (getline(myfile, line)) {
			const auto& tokens = HelperFunctions::SplitString(line, ';');
			float coeff = stof(tokens[0]); //the first float is the coeff of the vector;
			vector<Vector2D> pts;
			for (int j = 1; j < tokens.size(); j++) { //the rest is the list of all the node of the control curve seperated as "x y"
				const auto& tokensPts = HelperFunctions::SplitString(tokens[j],' ');
				pts.push_back(Vector2D(stof(tokensPts[0]), stof(tokensPts[1])));

			}
			generateControlVectors(result, pts, coeff);
		}
	}
	while(getline(myfile, line)){//after the list of curves, the list of zero area grid cell by index "i;j"
		const auto& tokens = HelperFunctions::SplitString(line, ';');
		array<int, 2> index{ stoi(tokens[0], nullptr, 10), stoi(tokens[1], nullptr, 10) };
		result->zeroArea_.push_back(index);
	}
		
	


	myfile.close();

	// store the result
	matrices_[parameterValue] = result;

	return true;
}

bool InteractionField::updateMatrixFromFile(bool quick)
{
	for (auto &matrice : matrices_) {
		float parameter = matrice.first;
		const InteractionFieldMatrix *matrix = matrice.second;
		if (matrix->namefileMatrice_.empty())
			return false;
		// open the file
		ifstream myfile(matrix->namefileMatrice_);
		if (!myfile.is_open())
		{
			cerr << "Error: Unable to open InteractionField matrix file \"" << matrix->namefileMatrice_ << "\"." << endl
				<< "This matrix will not be loaded." << endl;
			return false;
		}

		// --- read the first line: contains the number or rows and columns
		string line;
		if (!getline(myfile, line))
		{
			cerr << "Error: Unable to read 1st line of InteractionField matrix file \"" << matrix->namefileMatrice_ << "\"." << endl
				<< "This matrix will not be loaded." << endl;
			return false;
		}

		auto tokens = HelperFunctions::SplitString(line, ';');

		int rows = HelperFunctions::ParseInt(tokens[0]);
		int cols = HelperFunctions::ParseInt(tokens[1]);

		// prepare a result with a VectorMatrix of this size
		InteractionFieldMatrix* result = new InteractionFieldMatrix(rows, cols);
		result->namefileMatrice_ = matrix->namefileMatrice_;
		// --- new file version: the first line also contains the width and height of a cell,
		//     and the second line contains the source position and all links
		bool newFileType = (tokens.size() >= 4);
		int numberLine;
		if (newFileType)
		{
			result->cellHeight_ = HelperFunctions::ParseFloat(tokens[2]);
			result->cellWidth_ = HelperFunctions::ParseFloat(tokens[3]);

			// read the second line: contains the source object's position, and possibly information about links
			if (!getline(myfile, line))
			{
				cerr << "Error: Unable to read 1st line of InteractionField matrix file \"" << matrix->namefileMatrice_ << "\"." << endl
					<< "This matrix will not be loaded." << endl;
				return false;
			}
			tokens = HelperFunctions::SplitString(line, ';');

			// source position
			result->sourcePosition_ = Vector2D(HelperFunctions::ParseFloat(tokens[0]), HelperFunctions::ParseFloat(tokens[1]));

			// read the links one by one
			size_t index = 2;
			while (index + 2 < tokens.size())
			{
				// link type
				Link::LinkType linkType;
				if (tokens[index] == "r")
					linkType = Link::Rotation;
				else if (tokens[index] == "s")
					linkType = Link::Scaling;
				else if (tokens[index] == "t")
					linkType = Link::Translation;

				// object position
				Vector2D otherObjectPosition(HelperFunctions::ParseFloat(tokens[index + 1]), HelperFunctions::ParseFloat(tokens[index + 2]));

				// store this link in the result
				result->links_.push_back(Link(linkType, result->sourcePosition_, otherObjectPosition));

				index += 3;
			}
		}
		if (quick) {
			//read the number of control vectors
			if (getline(myfile, line))
				numberLine = stoi(line, nullptr, 10);
			// read the curve one by one
			for (int i = 0; i < numberLine; i++)
			{
				if (getline(myfile, line)) {
					const auto& tokens = HelperFunctions::SplitString(line, ';');
					float coeff = stof(tokens[0]); //the first float is the coeff of the vector;
					vector<Vector2D> pts;
					for (int j = 1; j < tokens.size(); j++) { //the rest is the list of all the node of the control curve seperated as "x y"
						const auto& tokensPts = HelperFunctions::SplitString(tokens[j], ' ');
						pts.push_back(Vector2D(stof(tokensPts[0]), stof(tokensPts[1])));

					}
					generateControlVectors(result, pts, coeff);
				}
			}
			while (getline(myfile, line)) {//after the list of curves, the list of zero area grid cell by index "i;j"
				const auto& tokens = HelperFunctions::SplitString(line, ';');
				array<int, 2> index{ stoi(tokens[0], nullptr, 10), stoi(tokens[1], nullptr, 10) };
				result->zeroArea_.push_back(index);
			}

		}
		else {
			// read the vectors one by one
			for (int i = 0; i < rows; i++)
			{
				for (int j = 0; j < cols; j++)
				{
					if (getline(myfile, line))
					{
						const auto& tokens = HelperFunctions::SplitString(line, ';');

						// --- old file version: "s" indicates the the source is on this grid cell, 
						//     and it is followed by information about links
						if (tokens[0] == "s")
						{
							// the matrix should store the zero vector here
							result->matrix_.setAt(i, j, Vector2D(0, 0));

							// convert this grid coordinate to a position for the source object
							result->sourcePosition_ = Vector2D(j * result->cellWidth_, (rows - i) * result->cellHeight_);

							int index = 1;
							while (index < tokens.size())
							{
								// load a link
								Link::LinkType linkType;
								Vector2D otherObjectPosition(0, 0);
								if (tokens[index] == "r")
								{
									linkType = Link::Rotation;

									// we don't know the exact position of the other object, but we can guess it from the angle
									float angle = HelperFunctions::ParseFloat(tokens[index + 4]);
									otherObjectPosition = result->sourcePosition_ + rotateCounterClockwise(Vector2D(0, -10), angle);
								}
								else if (tokens[index] == "s")
									linkType = Link::Scaling;
								else if (tokens[index] == "t")
									linkType = Link::Translation;

								// store this link in the result
								result->links_.push_back(Link(linkType, result->sourcePosition_, otherObjectPosition));

								index += 5;
							}
						}

						// vector
						else
						{
							Vector2D xy(stof(tokens[0]), stof(tokens[1]));
							// for old IF files, clamp the vector to a maximum magnitude (because vectors in those files are very big)
							if (!newFileType)
								xy = clampVector(xy, 1.8f);
							result->matrix_.setAt(i, j, xy);
						}
					}
				}
			}

		}
		myfile.close();
		// store the result
		matrices_[parameter] = result;

	}
	
	return true;
}

void InteractionField::generateControlVectors(InteractionFieldMatrix* field,std::vector<Vector2D> curve, float coeff)
{
	for (auto &vector : curve) {

		//float n = 40.f;
		//float subsection =n/ (float)curve.size();

		//for (int i = 0; i < curve.size()-1; i++) {
		//	Vector2D subVector = (curve[i+1] - curve[i]) / subsection;
		//	float sec = 0.f;
		//	Vector2D startPos = curve[i];
		//	Vector2D endPos = curve[i];
		//	while ((endPos-curve[i]).magnitude()<(curve[i+1]-curve[i]).magnitude()){
		//		endPos = startPos + subVector;
		//		field->controlVectors_.push_back((endPos - startPos).getnormalized()*coeff / 100.0f);
		//		field->controlVectorPos_.push_back(startPos);
		//		startPos = endPos;
		//		float dis = distance(endPos, curve[i + 1]);

		//	}
		for (int i = 0; i < curve.size() - 1; i++) {
				Vector2D startPos = curve[i];
				Vector2D endPos = curve[i+1];
				//field->controlVectors_.push_back((endPos - startPos).getnormalized()*coeff / 100.0f);
				field->controlVectors_.push_back((endPos - startPos));
				field->controlVectorPos_.push_back(startPos);


		}

	}
}

