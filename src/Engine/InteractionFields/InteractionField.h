#ifndef LIB_INTERACTION_FIELD_H
#define LIB_INTERACTION_FIELD_H

#include <tools/vectormatrix.h>

#include <InteractionFields/IFSource.h>
#include <InteractionFields/Cell.h>
#include <InteractionFields/Link.h>

#include <array>
#include <map>

class WorldBase;

typedef std::pair<Vector2D, Vector2D> IFVisualizationGridPoint;
struct IFVisualizationGrid
{
	std::vector< std::vector<IFVisualizationGridPoint>> gridPoints;
	std::vector<Vector2D> boundary;
};

/// <summary>Represents an interaction field (possibly a parametric one) that can affect the velocity or orientation of agents.</summary>
class InteractionField
{

public:

	enum ParameterType { None, SourceSpeed, ManualParameter };

	/// <summary>A struct containing the settings of an interaction field that typically do not change over time.</summary>
	struct Settings
	{
		/// <summary>The type value impacted by the field by default.</summary>
		bool isVelocityIF_ = true;
		/// <summary>The time appearance of the field. By default, the field is unlimited.</summary>
		float appearanceTime_ = 0.0f;
		///<summary> the weight of the field </summary>
		float weight_ = 1.0f;
		///<summary> the if the IF is for a certain group: the group id, if not group=-1</summary>
		int group_ = -1;
		/// <summary>The type of parameter that makes this IF parametric. "None" implies that the IF is not parametric.</summary>
		ParameterType parameter_;
	};

	/// <summary>A struct for a single interaction field matrix with a source position and a set of links.</summary>
	struct InteractionFieldMatrix
	{
		VectorMatrix matrix_;
		Vector2D sourcePosition_;
		std::vector<Link> links_;
		std::vector<std::array<int, 2>> zeroArea_;
		std::vector<Vector2D> controlVectors_;
		std::vector<Vector2D> controlVectorPos_;
		std::string namefileMatrice_;
		///<summary>The height of a matrix cell in meters</summary>
		float cellHeight_ = 0.28f;
		///<summary>The width of a matrix cell in meters</summary>
		float cellWidth_ = 0.28f;

		InteractionFieldMatrix(int rows=1, int cols=1) : matrix_(rows, cols) {}

		float GetWidth() const { return cellWidth_ * matrix_.getCols(); }
		float GetHeight() const { return cellHeight_ * matrix_.getRows(); }
		std::string GetName() const { return namefileMatrice_; }

	};

private:

	size_t id_;
	InteractionField::Settings settings_;
	/// <summary>The IF "keyframes" inside this full interaction field. 
	/// If this IF is not parametric, this list has only one element. 
	/// Otherwise, it has an element per keyframe matrix that was specified in the scenario.</summary>
	std::map<float, const InteractionFieldMatrix*> matrices_;

public:

	InteractionField(const InteractionField::Settings& settings, size_t id);
	~InteractionField();

	void generateControlVectors(InteractionFieldMatrix* field, std::vector<Vector2D>, float coeff); //extract eh right control vectors from the curves of the GUI.
	Vector2D computeVectorAtPosition(const Vector2D& position, const IFSource* source, const std::vector<IFSourceReference>& linkObjects, const WorldBase* world, bool& result_pointInsideIF) const;

	float getParameterValue(const IFSource * source, const WorldBase * world, const std::vector<IFSourceReference>& linkObjects) const;

	float getRotationAngle(const IFSource * source, const std::vector<IFSourceReference>& linkObjects, const WorldBase * world) const;

	IFVisualizationGrid buildVisualizationGrid(const IFSource* source, const std::vector<IFSourceReference>& linkObjects, const WorldBase* world) const;



	bool loadMatrixFromFile(const std::string& file, float parameterValue);

	bool loadMatrixFromFileQuick(const std::string & filename, float parameterValue);
	bool updateMatrixFromFile(bool quick);
	/// <summary>Returns whether this IF concerns velocity vectors.(Otherwise, it concerns orientation vectors.)</summary>
	inline bool getIsVelocity() const { return settings_.isVelocityIF_; }
	inline float getTimeOn() const { return settings_.appearanceTime_; }
	//inline const std::vector<Link>& getLinks() const { return links_; };
	inline size_t getID() const { return id_; }
	inline float getWeight() const { return settings_.weight_; }
	inline const int getGroupID() const { return settings_.group_; }
	inline std::vector<Vector2D> getControlVectors(float key) const { return matrices_.at(key)->controlVectors_; }
	inline std::vector<Vector2D> getControlVectorsPosition(float key) const { return matrices_.at(key)->controlVectorPos_; }


private:

	void getMatricesForInterpolation(const IFSource* source, const InteractionFieldMatrix*& result0, const InteractionFieldMatrix*& result1, float& result_mu, const WorldBase* world, const std::vector<IFSourceReference>& linkObjects) const;
	float getRotationAngleFromLinks(const InteractionFieldMatrix* field, const IFSource* source, const std::vector<IFSourceReference>& linkObjects, const WorldBase* world) const;
	Vector2D _computeVectorAtPositionContinuus(const InteractionFieldMatrix * field, const Vector2D & position, const IFSource * source, const std::vector<IFSourceReference>& linkObjects, const WorldBase * world, bool & result_pointInsideIF) const;
	Vector2D convertPositionToGridCoordinates(const InteractionFieldMatrix* field, const Vector2D& position, const Vector2D& sourcePosition, const float IFRotation) const;
	Vector2D _computeVectorAtPosition(const InteractionFieldMatrix* field, const Vector2D& position, const IFSource* source, const std::vector<IFSourceReference>& linkObjects, const WorldBase* world, bool& result_pointInsideIF) const;

	IFVisualizationGrid buildVisualizationGrid_singleMatrix(const InteractionFieldMatrix* field, const IFSource* source, const std::vector<IFSourceReference>& linkObjects, const WorldBase* world) const;

	IFVisualizationGrid buildVisualizationGridFromControlVectors_singleMatrix(const InteractionFieldMatrix* field, const IFSource* source, const std::vector<IFSourceReference>& linkObjects, const WorldBase* world) const;
};

#endif


