#ifndef LIB_LINK_H
#define LIB_LINK_H

#include <tools/vector2D.h>
#include <InteractionFields/IFSource.h>

#include <vector>
#include <string>
#include <array>

struct Link
{
public:
	enum LinkType { Rotation, Translation, Scaling };

private:
	
	LinkType linkType;
	Vector2D basePosition_source;
	Vector2D basePosition_otherObject;
	float baseDistance;

public:

	// empty constructor (otherwise vector<Link> is not allowed)
	Link() {}
	Link::Link(LinkType type, const Vector2D& sourcePos, const Vector2D& otherPos)
		: linkType(type), basePosition_source(sourcePos), basePosition_otherObject(otherPos)
	{
		baseDistance = distance(basePosition_source, basePosition_otherObject);
	}
	~Link() {}

	inline LinkType getType() const { return linkType; }

	inline float getRotationAngle(const Vector2D& currentSourcePos, const Vector2D& currentOtherPos) const
	{
		return clockwiseAngle(basePosition_otherObject - basePosition_source, currentOtherPos - currentSourcePos);
	}

	inline float getDistanceScaleFactor(const Vector2D& currentSourcePos, const Vector2D& currentOtherPos) const
	{
		return distance(currentSourcePos, currentOtherPos) / baseDistance;
	}
};
#endif //LIB_LINK_H