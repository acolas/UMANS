/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettr�
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#include <CostFunctions/ContactForces_Helbing.h>
#include <core/agent.h>
#include <core/worldBase.h>

Vector2D ContactForces_Helbing::ComputeAgentInteractionForce(const Agent* agent, const PhantomAgent& other) const
{
	if (scale_agents_ <= 0)
		return Vector2D(0, 0);

	const auto& diff = agent->getPosition() - other.GetPosition();
	const float intersectionDistance = agent->getRadius() + other.realAgent->getRadius() - diff.magnitude();
	if (intersectionDistance > 0)
		return diff.getnormalized() * (scale_agents_ * intersectionDistance);

	return Vector2D(0, 0);
}

Vector2D ContactForces_Helbing::ComputeObstacleInteractionForce(const Agent* agent, const LineSegment2D& obstacle) const
{
	if (scale_obstacles_ <= 0)
		return Vector2D(0, 0);

	const Vector2D& position = agent->getPosition();
	if (isPointLeftOfLine(position, obstacle.first, obstacle.second))
		return Vector2D(0,0);

	const auto& nearest = nearestPointOnLine(position, obstacle.first, obstacle.second, true);
	if (nearest == obstacle.second)
		return Vector2D(0, 0);

	Vector2D diff = position - nearest;
	float intersectionDistance = agent->getRadius() - diff.magnitude();
	if (intersectionDistance > 0)
		return diff.getnormalized() * (scale_obstacles_ * intersectionDistance);

	return Vector2D(0, 0);
}

void ContactForces_Helbing::parseParameters(const CostFunctionParameters & params)
{
	ObjectInteractionForces::parseParameters(params);
	params.ReadFloat("scale_agents", scale_agents_);
	params.ReadFloat("scale_obstacles", scale_obstacles_);
}
