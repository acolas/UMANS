/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettr�
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#ifndef VELOCITY_REACHING_FORCE_H
#define VELOCITY_REACHING_FORCE_H

#include <CostFunctions/ForceBasedFunction.h>

/// @ingroup costfunctions
/// <summary>An abstract force that attracts the agents towards a particular velocity.</summary>
/// <remarks>
/// ### Definition:
/// F = (targetVelocity - agent.currentVelocity) / max(dt, relaxationTime).
/// We disallow relaxation times smaller than dt (the simulation time step) because this would yield undesired behavior.
/// 
/// ### Parameters:
/// <list>
/// <item>relaxationTime (float): Determines how quickly the agent will be steered to the target velocity.</item>
/// </list>
/// </remarks>
class VelocityReachingForce : public ForceBasedFunction
{
public:
	VelocityReachingForce(const PolicyStep_CostFunctions* step) : ForceBasedFunction(step) { range_ = 0; }
	virtual ~VelocityReachingForce() {}

protected:
	/// <summary>Computes a 2D force vector that steers the agent towards its preferred velocity.</summary>
	/// <param name="agent">The agent for which a force is requested.</param>
	/// <param name="world">The world in which the simulation takes place.</param>
	/// <returns>A force vector that steers the agent towards its preferred velocity.</returns>
	virtual Vector2D ComputeForce(Agent* agent, const WorldBase* world) const override;

	/// <summary>Gets the velocity towards which this force should steer the agent.
	/// This is different per child class of VelocityReachingForce.</summary>
	virtual Vector2D GetTargetVelocity(Agent* agent) const = 0;
};

#endif //VELOCITY_REACHING_FORCE_H
