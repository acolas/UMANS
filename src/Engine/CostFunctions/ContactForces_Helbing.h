/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettr�
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#ifndef CONTACTFORCES_HELBING_H
#define CONTACTFORCES_HELBING_H

#include <CostFunctions/ObjectInteractionForces.h>

/// @ingroup costfunctions
/// <summary>A simple contact force as used by Helbing et al,
/// in the paper "Simulating dynamical features of escape panic" (2000).</summary>
/// <remarks>
/// ### Notes:
/// <list>
/// <item>This force does not perform any navigation: it only responds to current collisions between agents and agents/obstacles.</item>
/// <item>This force is used by many of our example policies. 
/// It is good practice to use contact forces in any simulation, because collision avoidance may fail.</item>
/// </list>
/// 
/// ### Name in XML files:
/// <tt>%ContactForces_Helbing</tt>
/// 
/// ### Parameters:
/// <list>
/// <item>scale_agents (float): A scaling factor for the force exerted by colliding agents.</item>
/// <item>scale_obstacles_(float): A scaling factor for the force exerted by colliding obstacles.</item>
/// </list>
/// </remarks>
class ContactForces_Helbing : public ObjectInteractionForces
{
private:
	/// <summary>A scaling factor to apply to contact forces among agents. Use 0 to disable these forces completely.</summary>
	float scale_agents_ = 5000.f / 80.f; // Helbing (2000): k = 1.2*10^5 kg/s^2, agent mass = 80 kg
	/// <summary>A scaling factor to apply to contact forces etween agents and obstacles. Use 0 to disable these forces completely.</summary>
	float scale_obstacles_ = 2500.f / 80.f; // Helbing (2000): k = 2.4*10^5 kg/s^2, agent mass = 80 kg

public:
	ContactForces_Helbing(const PolicyStep_CostFunctions* step) : ObjectInteractionForces(step) { range_ = 1; }
	virtual ~ContactForces_Helbing() {}
	const static std::string GetName() { return "ContactForces_Helbing"; }
	void parseParameters(const CostFunctionParameters & params) override;

protected:
	virtual Vector2D ComputeAgentInteractionForce(const Agent* agent, const PhantomAgent& other) const override;
	virtual Vector2D ComputeObstacleInteractionForce(const Agent* agent, const LineSegment2D& obstacle) const override;
};

#endif //CONTACTFORCES_HELBING_H
