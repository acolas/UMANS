/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettr�
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#ifndef PREFERRED_VELOCITY_FORCE_H
#define PREFERRED_VELOCITY_FORCE_H

#include <CostFunctions/VelocityReachingForce.h>
#include <core/agent.h>

/// @ingroup costfunctions
/// <summary>A force that attracts the agents towards its (previously computed) preferred velocity.</summary>
/// <remarks>
/// ### Definition:
/// F = (agent.preferredVelocity - agent.currentVelocity) / max(dt, policy.relaxationTime).
/// We disallow relaxation times smaller than dt (the simulation time step) because this would yield undesired behavior.
/// ### Name in XML files:
/// <tt>%PreferredVelocityForce</tt></remarks>
class PreferredVelocityForce : public VelocityReachingForce
{
public:
	PreferredVelocityForce(const PolicyStep_CostFunctions* step) : VelocityReachingForce(step) {}
	virtual ~PreferredVelocityForce() {}
	const static std::string GetName() { return "PreferredVelocityForce"; }

protected:
	virtual Vector2D GetTargetVelocity(Agent* agent) const override;
};

#endif //PREFERRED_VELOCITY_FORCE_H
