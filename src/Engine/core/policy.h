/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettr�
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#ifndef LIB_POLICY_H
#define LIB_POLICY_H

#include <tools/vector2D.h>
#include <core/costFunction.h>
#include <3rd-party/tinyxml/tinyxml2.h>
#include <vector>
#include <map>

class PolicyStep;

/// <summary>A policy that an agent can use for local navigation. It consists of one or more PolicyStep objects.</remarks>
class Policy
{
public:
	typedef std::vector<PolicyStep*> PolicyStepList;
private:
	int id_;
	std::map<std::string, PolicyStepList> phases_;

public:
	Policy(int id) : id_(id) {}
	~Policy();
	void AddStep(const std::string& phase, PolicyStep* step);
	float GetInteractionRange() const;

	inline int GetID() const { return id_; }

	const PolicyStepList* GetStepsForPhase(const std::string& name)
	{ 
		const auto& it = phases_.find(name);
		if (it == phases_.end())
			return nullptr;
		return &it->second;
	}

	const size_t GetNumberOfSteps()
	{
		size_t result = 0;
		for (const auto& phase : phases_)
			result += phase.second.size();
		return result;
	}

	void SynchronizeDeltaTimes(float newDeltaTime_coarse, float newDeltaTime_fine);
};

#endif //LIB_POLICY_H
