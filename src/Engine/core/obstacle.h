#ifndef LIB_OBSTACLE_H
#define LIB_OBSTACLE_H

#include <tools/Polygon2D.h>
#include <InteractionFields/InteractionField.h>

/// <summary>An obstacle in the simulation.</summary>
class Obstacle : public Polygon2D, public IFSource
{

private:

	/// <summary>The width of the obstacle (in meters).</summary>
	float width_;
	/// <summary>The height of the obstacle (in meters).</summary>
	float height_;
	size_t id_;

	Vector2D position_;
	Vector2D velocity_ = Vector2D(0, 0);
	float parameter_ = 1.;
	Vector2D viewing_direction_;
	bool tangible_ = false;
	std::string color_;

public:
	~Obstacle();

#pragma region [Simulation-loop methods]

	Obstacle();
	Obstacle(size_t id, const std::vector<Vector2D>& vertices);

	inline const size_t& getID() const { return id_; }
	inline void setID(size_t id) { id_ = id; }
	inline float getHeight() const { return width_; }
	inline float getWidth() const { return height_; }
	inline bool getisSolid() const { return tangible_; }
	inline void setisSolid(bool tangible) { tangible_ = tangible; }
	inline const Vector2D& getPosition() const { return position_; }
	inline const Vector2D& getViewingDirection() const { return viewing_direction_; }
	inline const Vector2D& getVelocity() const { return velocity_; }
	inline void setVelocity(Vector2D vel) { velocity_ = vel; }
	inline const float& getParameter() const { return parameter_; }
	inline void setParameter(float param) { parameter_ = param; }

private:
	void initialize();

};
#endif //LIB_OBSTACLE_H