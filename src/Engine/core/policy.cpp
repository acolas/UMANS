/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettré
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#include <core/policy.h>
#include <core/PolicyStep.h>
#include <algorithm>

Policy::~Policy()
{
	// delete all steps
	for (auto& phase : phases_)
	{
		for (PolicyStep* step : phase.second)
			delete step;
		phase.second.clear();
	}
		
	phases_.clear();
}

void Policy::AddStep(const std::string& phase, PolicyStep* step)
{
	PolicyStepList& list = phases_[phase];
	step->SetID(list.size());
	list.push_back(step);
}

float Policy::GetInteractionRange() const
{
	float result = 0;

	for (const auto& phase : phases_)
		for (const PolicyStep* step : phase.second)
			result = std::max(result, step->getInteractionRange());

	return result;
}

void Policy::SynchronizeDeltaTimes(float newDeltaTime_coarse, float newDeltaTime_fine)
{
	for (const auto& phase : phases_)
		for (PolicyStep* step : phase.second)
			step->SynchronizeDeltaTime(newDeltaTime_coarse, newDeltaTime_fine);
}