#include <core/obstacle.h>

using namespace std;

Obstacle::Obstacle(size_t id, const std::vector<Vector2D>& vertices) 
	: Polygon2D(vertices), id_(id)
{
	initialize();
}

Obstacle::Obstacle() 
	: Polygon2D(), id_(0)
{
	initialize();
}

void Obstacle::initialize()
{
	// compute width, height, and center of mass
	Vector2D sum(0, 0);
	float xmin = MaxFloat;
	float xmax = -MaxFloat;
	float ymin = MaxFloat;
	float ymax = -MaxFloat;

	for (auto vertex : GetVertices())
	{
		xmin = std::min(xmin, vertex.x);
		xmax = std::max(xmax, vertex.x);
		ymin = std::min(ymin, vertex.y);
		ymax = std::max(ymax, vertex.y);

		sum += vertex;
	}

	height_ = ymax - ymin;
	width_ = xmax - xmin;
	position_ = sum / (float)GetVertices().size();

	viewing_direction_ = Vector2D(0,-1);
}

Obstacle::~Obstacle()
{
}
