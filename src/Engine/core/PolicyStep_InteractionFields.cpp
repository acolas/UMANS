
/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettré
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#include <core/PolicyStep_InteractionFields.h>

#include <core/agent.h>
#include <core/worldBase.h>

using namespace std;

Vector2D PolicyStep_InteractionFields::ComputeAcceleration(Agent* agent, WorldBase * world)
{
	// obtain a new velocity proposed by interaction fields
	const Vector2D& bestVelocity = getNewVelocityFromIFs(agent, world);

	// - convert this to an acceleration using a relaxation time
	//   Note: the relaxation time should be at least the length of a frame.
	return (bestVelocity - agent->getVelocity()) / std::max(relaxationTime_, deltaTime_);
}

Vector2D PolicyStep_InteractionFields::ComputeNewViewingDirection(Agent* agent, WorldBase* world)
{
	return getNewViewingDirectionFromIFs(agent, world);
}

Vector2D PolicyStep_InteractionFields::ComputePredictionPosition(Agent * agent, WorldBase * world, int i)
{
	if (i == 0) {
		return (agent->getPosition() + agent->getVelocity()*0.33);
	}
	else {
		Vector2D vel = getNewVelocityPredictionFromIFs(agent, world, i );
		Vector2D pos = agent->getFuturePos(i-1);
		float magvel = vel.magnitude();
		//Vector2D res = pos + vel*std::max(relaxationTime_, deltaTime_);
		Vector2D res = pos + vel * 0.33; //0.33 because it is the time step use for MxM animation library between each animation frame
		
		return(res);
	}
}

Vector2D PolicyStep_InteractionFields::ComputePredictionViewingDirection(Agent * agent, WorldBase * world, int i)
{
	return getNewViewingDirectionPredictionFromIFs(agent, world, i);/// check i ou i-1
}



void processSingleIF(const InteractionField* field, const std::vector<IFSourceReference>& linkObjects, const IFSource* source, Agent* agent, WorldBase* world, Vector2D& result_totalVector, float& result_totalWeight)
{
	// museum scenario: if the agent is looking at a painting, disable the IF that lets the agent move through the corridor
	//const Obstacle* sourceObstacle = dynamic_cast<const Obstacle*>(source);
	//if (sourceObstacle == nullptr && (field->getID() == 11  || field->getID() ==23 || field->getID() == 25) && (agent->getClock(3) > 0 || agent->getClock(10) > 0 || agent->getClock(13) > 0 || agent->getClock(14) > 0 || agent->getClock(16) > 0))
	//	return;
	 
	//const Agent* sourceAgent = dynamic_cast<const Agent*>(source);
	//if (sourceAgent == nullptr && (field->getID() == 11) && (agent->getClock(11) > 0))
	//	return;
	

		//Hiders and seeker, if the hider is close to the sseker, he switch to group 3 and follow the seeker
	//if ((agent->getGroupID() == 1) && ((world->GetAgent(0)->getPosition() - agent->getPosition()).magnitude() < 1)) {
	//	agent->setGroupID(3);
	//}
	// HideAndSeek scenario: the more the distance with a hidding spot and the seeker is important, the more the hidder wants to hide there: the depend on the distance
	float addWeight = 1;

	//if (agent->getGroupID() == 1 || agent->getGroupID()==2 && (world->GetAgent(0)->getPosition() - source->getPosition()).magnitude()!=0){

	//	addWeight =(world->GetAgent(0)->getPosition() - source->getPosition()).magnitude();
	//	
	//}


	// if the IF only applies to certain agent groups
	if (field->getGroupID() >= 0 && field->getGroupID() != agent->getGroupID())
		return;

	// compute the vector that the IF proposes; this method also computes whether the is vector inside the IF
	bool insideIF;
	Vector2D result = field->computeVectorAtPosition(agent->getPosition(), source, linkObjects, world, insideIF);
	float mag = result.magnitude();
	//if (field->getTimeOn() > 0 && !insideIF && agent->getClock((int)field->getID()) == -1) {
	//	agent->setClock((int)field->getID(), 0);
	//}
	if (!insideIF)
		return;
	// if the IF is enabled for a limited time, check if we should use it now, and maybe update the agent's timers

	//cross road
	//const Agent* sourceAgent = dynamic_cast<const Agent*>(source);
	//if (sourceAgent != nullptr && ((agent->getGroupID()==6 )|| (agent->getGroupID()==5))) {
	//	if ((sourceAgent->getGroupID() == 2) || (sourceAgent->getGroupID()==3)) {
	//		agent->setGroupID(1);
	//	}
	//	else {
	//		agent->setGroupID(6);
	//	}
	//}
	//else if (agent->getGroupID() == 1) {
	//	
	//		agent->setGroupID(6);
	//	
	//}

	//end cross road
	// clamp to the agent's maximum speed if old file IF
	result = clampVector(result, agent->getMaximumSpeed());

	result = result * agent->getMaximumSpeed();// when it is not continuus




	//for the cats demo laval scenario, to change if you want to go back to museum (earlier commit)
	if (field->getTimeOn() > 0)
	{
		int neighborFieldID = (int)field->getID();			
		float time = agent->getClock(neighborFieldID);
		if (!(result.isZero()) && (agent->getClock(neighborFieldID)) == -1) {
			agent->setClock((int)field->getID(), 0);
			result_totalVector += Vector2D(0, 0);
		}
		// if the agent has not used this IF yet, start using it now, and store the current time
		else if (!(result.isZero()) && (agent->getClock(neighborFieldID) == 0)) {
			agent->setClock(neighborFieldID, world->GetCurrentTime());
			result_totalVector += Vector2D(0,0);
		}
		else if (!(result.isZero()) && ((world->GetCurrentTime() - agent->getClock(neighborFieldID)) < agent->getTimeRatio()*field->getTimeOn()))
		{
			result_totalVector += Vector2D(0, 0);
		}
		// if the agent has used this IF for long enough, stop using it
		else if (result.isZero()) {
			agent->setClock(neighborFieldID, -1);
			result_totalVector += result * field->getWeight()*addWeight;
			result_totalWeight += field->getWeight();
		}
		else {
			
			result_totalVector += result * field->getWeight()*addWeight;
			result_totalWeight += field->getWeight();
		}


	}

	else {

		result_totalVector += result * field->getWeight()*addWeight;
		result_totalWeight += field->getWeight();
	}

	//const Agent* sourceAgent = dynamic_cast<const Agent*>(source);
	//if (sourceAgent!=nullptr && (sourceAgent->getGroupID() == 2 || sourceAgent->getGroupID() == 3)) {
	//	result_totalVector = result * field->getWeight()/**addWeight*/;
	//	result_totalWeight = field->getWeight();

	//}

	
}


void processSingleIFPrediction(const InteractionField* field, const std::vector<IFSourceReference>& linkObjects, const IFSource* source, Agent* agent,int indexPrediction, WorldBase* world, Vector2D& result_totalVector, float& result_totalWeight)
{
	// museum scenario: if the agent is looking at a painting, disable the IF that lets the agent move through the corridor
	const Obstacle* sourceObstacle = dynamic_cast<const Obstacle*>(source);
	//if (sourceObstacle != nullptr && (field->getID() == 24 || field->getID() == 23 || field->getID() == 25) && (agent->getClock(3) > 0 || agent->getClock(10) > 0 || agent->getClock(13) > 0 || agent->getClock(14) > 0 || agent->getClock(16) > 0))
	//	return;

	// HideAndSeek scenario: the more the distance with a hidding spot and the seeker is important, the more the hidder wants to hide there: the depend on the distance
	float addWeight = 1;
	//if (agent->getGroupID() == 1 || agent->getGroupID()==2 && (world->GetAgent(0)->getPosition() - source->getPosition()).magnitude()!=0){

	//	addWeight =(world->GetAgent(0)->getPosition() - source->getPosition()).magnitude();
	//	
	//}



	// if the IF only applies to certain agent groups
	if (field->getGroupID() >= 0 && field->getGroupID() != agent->getGroupID())
		return;

	//if the source is an agent, prentend that the position of the source is the predicted position
	const Agent* sourceAgent = dynamic_cast<const Agent*>(source);

	Vector2D result;
	// compute the vector that the IF proposes; this method also computes whether the is vector inside the IF
	bool insideIF;
	if (sourceAgent != nullptr) {
		Agent* predictedAgent =new Agent(*sourceAgent);
		Vector2D predictedPos = sourceAgent->getFuturePos(indexPrediction - 1);
		predictedAgent->setPosition(predictedPos);
		predictedAgent->setViewingDirection(sourceAgent->getFutureOrient(indexPrediction - 1));
		result = field->computeVectorAtPosition(agent->getFuturePos(indexPrediction - 1), const_cast<const Agent*>(predictedAgent), linkObjects, world, insideIF);		
		delete predictedAgent;		
	}
	else {
		 result = field->computeVectorAtPosition(agent->getFuturePos(indexPrediction - 1), source, linkObjects, world, insideIF);
		 
	}
		
	
	

	//if (field->getTimeOn() > 0 && !insideIF && agent->getClock((int)field->getID()) == -1) {
	//	agent->setClock((int)field->getID(), 0);
	//}
	if (!insideIF)
		return;
	// if the IF is enabled for a limited time, check if we should use it now, and maybe update the agent's timers
	//if (field->getTimeOn() > 0)
	//{
	//	int neighborFieldID = (int)field->getID();

	//	// if the agent is not allowed to use this IF anymore, don't do it
	//	if (agent->getClock(neighborFieldID) == -1)
	//		return;

	//	// if the agent has not used this IF yet, start using it now, and store the current time
	//	if (agent->getClock(neighborFieldID) == 0)
	//		agent->setClock(neighborFieldID, world->GetCurrentTime());

	//	// if the agent has used this IF for long enough, stop using it
	//	else if (world->GetCurrentTime() - agent->getClock(neighborFieldID) >= agent->getTimeRatio()*field->getTimeOn())
	//	{
	//		agent->setClock(neighborFieldID, -1);
	//		return;
	//	}
	//}
		// clamp to the agent's maximum speed if old file IF
	result = clampVector(result, agent->getMaximumSpeed());

	result = result * agent->getMaximumSpeed(); //if not continuus
	if (field->getTimeOn() > 0)
	{
		int neighborFieldID = (int)field->getID();
		float time = agent->getClock(neighborFieldID);
		if (!(result.isZero()) && (agent->getClock(neighborFieldID)) == -1) {
			agent->setClock((int)field->getID(), 0);
			result_totalVector += Vector2D(0, 0);
		}
		// if the agent has not used this IF yet, start using it now, and store the current time
		else if (!(result.isZero()) && (agent->getClock(neighborFieldID) == 0)) {
			agent->setClock(neighborFieldID, world->GetCurrentTime());
			result_totalVector += Vector2D(0, 0);
		}
		else if (!(result.isZero()) && ((world->GetCurrentTime() - agent->getClock(neighborFieldID)) < agent->getTimeRatio()*field->getTimeOn()))
		{
			result_totalVector += Vector2D(0, 0);
		}
		// if the agent has used this IF for long enough, stop using it
		else if (result.isZero()) {
			agent->setClock(neighborFieldID, -1);
			result_totalVector += result * field->getWeight()*addWeight;
			result_totalWeight += field->getWeight();
		}
		else {

			result_totalVector += result * field->getWeight()*addWeight;
			result_totalWeight += field->getWeight();
		}


	}

	else {

		result_totalVector += result * field->getWeight()*addWeight;
		result_totalWeight += field->getWeight();
	}
	// clamp to the agent's maximum speed
	//result = clampVector(result, agent->getMaximumSpeed());
	//printf("%6.4lf", agent->getMaximumSpeed());
	

	

}

Vector2D PolicyStep_InteractionFields::getNewVelocityFromIFs(Agent* agent, WorldBase* world)
{
	Vector2D totalVelocity(0, 0);
	float totalWeight = 0.f;

	const NeighborList& neighbors = agent->getNeighbors();

	if (useAgentSources_)
	{
		if (useObstacleSources_)
		{
			// check all obstacles
			for (const auto& neighborObstacle : neighbors.second)
				for (const auto& neighborField : neighborObstacle->getInteractionFieldsVelocity())

					processSingleIF(neighborField.field, neighborField.linkObjects, neighborObstacle, agent, world, totalVelocity, totalWeight);
		}

		// check all agents
		for (const auto& neighborAgent : neighbors.first)
			for (const auto& neighborField : neighborAgent.realAgent->getInteractionFieldsVelocity())
				//special case for museum scenario, if agents are fom the same group, they should only consider the velocity field of the group for the member of the group
				//if (agent->getGroupID() == 1 && agent->getGroupID() == neighborAgent.realAgent->getGroupID() && neighborField.field->getGroupID() != neighborAgent.realAgent->getGroupID())
				//	totalVelocity += Vector2D(0, 0);
				//else {
				//	processSingleIF(neighborField.field, neighborField.linkObjects, neighborAgent.realAgent, agent, world, totalVelocity, totalWeight);
				//}

				processSingleIF(neighborField.field, neighborField.linkObjects, neighborAgent.realAgent, agent, world, totalVelocity, totalWeight);
				
	}


	if (totalWeight == 0)
		return Vector2D(0, 0);

	totalVelocity = totalVelocity / totalWeight;
	if (totalVelocity.sqrMagnitude() < 0.01)
		return Vector2D(0, 0);

	return totalVelocity;	
}

Vector2D PolicyStep_InteractionFields::getNewViewingDirectionFromIFs(Agent* agent, WorldBase* world)
{
	Vector2D totalDirection(0, 0);
	float totalWeight = 0.f;

	const NeighborList& neighbors = agent->getNeighbors();

	if (useAgentSources_)
	{
		// check all agents
		for (const auto& neighborAgent : neighbors.first)
			for (const auto& neighborField : neighborAgent.realAgent->getInteractionFieldsOrientation())
				processSingleIF(neighborField.field, neighborField.linkObjects, neighborAgent.realAgent, agent, world, totalDirection, totalWeight);
	}

	if (useObstacleSources_)
	{
		// check all obstacles
		for (const auto& neighborObstacle : neighbors.second)
			for (const auto& neighborField : neighborObstacle->getInteractionFieldsOrientation())
				processSingleIF(neighborField.field, neighborField.linkObjects, neighborObstacle, agent, world, totalDirection, totalWeight);
	}

	if (totalWeight == 0)
		return Vector2D(0, 0);

	totalDirection = totalDirection / totalWeight;
	if (totalDirection.sqrMagnitude() < 0.01)
		return Vector2D(0, 0);
	
	return totalDirection.getnormalized();
}

Vector2D PolicyStep_InteractionFields::getNewVelocityPredictionFromIFs(Agent * agent, WorldBase * world, int indexPrediction)
{
	Vector2D totalVelocity(0, 0);
	float totalWeight = 0.f;

	const NeighborList& neighbors = agent->getNeighbors();

	if (useAgentSources_)
	{
		// check all agents
		for (const auto& neighborAgent : neighbors.first)
			for (const auto& neighborField : neighborAgent.realAgent->getInteractionFieldsVelocity())
				processSingleIFPrediction(neighborField.field, neighborField.linkObjects, neighborAgent.realAgent, agent, indexPrediction, world, totalVelocity, totalWeight);
				//special case for museum scenario, if agents are fom the same group, they should only consider the velocity field of the group for the member of the group
				//if (agent->getGroupID() == 1 && agent->getGroupID() == neighborAgent.realAgent->getGroupID() && neighborField.field->getGroupID() != neighborAgent.realAgent->getGroupID())
				//	totalVelocity += Vector2D(0, 0);
				//else {
				//	processSingleIF(neighborField.field, neighborField.linkObjects, neighborAgent.realAgent, agent, world, totalVelocity, totalWeight);
				//}

				

	}

	if (useObstacleSources_)
	{
		// check all obstacles
		for (const auto& neighborObstacle : neighbors.second)
			for (const auto& neighborField : neighborObstacle->getInteractionFieldsVelocity())

				processSingleIFPrediction(neighborField.field, neighborField.linkObjects, neighborObstacle, agent, indexPrediction, world, totalVelocity, totalWeight);
	}

	if (totalWeight == 0)
		return Vector2D(0, 0);

	totalVelocity = totalVelocity / totalWeight;
	if (totalVelocity.sqrMagnitude() < 0.01)
		return Vector2D(0, 0);

	return totalVelocity;
}


Vector2D PolicyStep_InteractionFields::getNewViewingDirectionPredictionFromIFs(Agent * agent, WorldBase * world, int indexPrediction)
{
	Vector2D totalDirection(0, 0);
	float totalWeight = 0.f;

	const NeighborList& neighbors = agent->getNeighbors();

	if (useAgentSources_)
	{
		// check all agents
		for (const auto& neighborAgent : neighbors.first)
			for (const auto& neighborField : neighborAgent.realAgent->getInteractionFieldsOrientation())
				processSingleIFPrediction(neighborField.field, neighborField.linkObjects, neighborAgent.realAgent, agent, indexPrediction, world, totalDirection, totalWeight);
	}

	if (useObstacleSources_)
	{
		// check all obstacles
		for (const auto& neighborObstacle : neighbors.second)
			for (const auto& neighborField : neighborObstacle->getInteractionFieldsOrientation())
				processSingleIFPrediction(neighborField.field, neighborField.linkObjects, neighborObstacle, agent,indexPrediction, world, totalDirection, totalWeight);
	}

	if (totalWeight == 0)
		return Vector2D(0, 0);

	totalDirection = totalDirection / totalWeight;
	if (totalDirection.sqrMagnitude() < 0.01)
		return Vector2D(0, 0);

	return totalDirection.getnormalized();
}



