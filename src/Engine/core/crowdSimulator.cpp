/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettré
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#include <core/crowdSimulator.h>

#include <tools/HelperFunctions.h>
#include <tools/TrajectoryCSVWriter.h>
#include <core/worldInfinite.h>
#include <core/worldToric.h>

#include <core/policy.h>
#include <core/PolicyStep_CostFunctions.h>
#include <core/PolicyStep_InteractionFields.h>
#include <core/costFunctionFactory.h>
#include <memory>
#include <clocale>
#include <filesystem>

#include <tools/TrajectoryCSVReader.h>
#include <tools/OCSReceiverHandle.h>

CrowdSimulator::CrowdSimulator()
{
	CostFunctionFactory::RegisterAllCostFunctions();
	writer_ = nullptr;
	end_time_ = MaxFloat;
}

void CrowdSimulator::StartCSVOutput(const std::string &dirname, bool flushImmediately)
{
	// create the CSV writer if it did not exist yet
	if (writer_ == nullptr)
		writer_ = new TrajectoryCSVWriter(flushImmediately);

	// try to set the directory
	if (!writer_->SetOutputDirectory(dirname))
	{
		std::cerr << "Error: Could not set CSV output directory to " << dirname << "." << std::endl
			<< "The program will be unable to write CSV output." << std::endl;
		delete writer_;
		writer_ = nullptr;
	}
}

void CrowdSimulator::StopCSVOutput()
{
	if (writer_ != nullptr)
	{
		delete writer_;
		writer_ = nullptr;
	}
}

CrowdSimulator::~CrowdSimulator()
{
	// delete the CSV writer?
	if (writer_ != nullptr)
		delete writer_;

	// clear all cost-function creation functions
	CostFunctionFactory::ClearRegistry();
}

void CrowdSimulator::RunCoarseSimulationSteps(int nrSteps)
{
	for (int i = 0; i < nrSteps; ++i)
	{
		// run one or more fine simulation steps
		do { world_->DoStep(); } while (!world_->IsCurrentFrameCoarseStep());

		// write output to a file?
		if (writer_ != nullptr)
		{
			double t = world_->GetCurrentTime();
			const auto& agents = world_->GetAgents();

			AgentTrajectoryPoints data;
			for (const Agent* agent : agents)
				data[agent->getID()] = TrajectoryPoint(t, agent->getPosition(), agent->getViewingDirection());

			writer_->AppendAgentData(data);
		}
	}
}

void CrowdSimulator::RunSimulationUntilEnd(bool showProgressBar, bool measureTime)
{
	if (end_time_ == MaxFloat || end_time_ <= 0)
	{
		std::cerr << "Error: The end time of the simulation is not correctly specified." << std::endl
			<< "The simulation cannot be run because it is unclear when it should end." << std::endl;
		return;
	}

	const int nrIterations_ = (int)ceilf(end_time_ / world_->GetDeltaTime_Coarse());

	// get the current system time; useful for time measurements later on
	const auto& startTime = HelperFunctions::GetCurrentTime();

	if (showProgressBar)
	{
		// do the simulation in K blocks, and show a progress bar with K blocks
		const int nrProgressBarBlocks = 20;
		int nrIterationsPerBlock = nrIterations_ / nrProgressBarBlocks;
		int nrIterationsDone = 0;

		// - print an empty progress bar first, to indicate how long the real bar will be
		std::string progressBar = "Simulation progress: |";
		for (int i = 0; i < nrProgressBarBlocks; ++i)
			progressBar += "-";
		progressBar += "|100%";
		std::cout << std::endl << progressBar << std::endl;

		// - on a new line, start writing a new progress bar
		std::cout << "                     [";
		for (int i = 0; i < nrProgressBarBlocks; ++i)
		{
			// run a block of iterations
			int nrIterationsToDo = (i+1 == nrProgressBarBlocks ? nrIterations_ - nrIterationsDone : nrIterationsPerBlock);
			RunCoarseSimulationSteps(nrIterationsToDo);

			// augment the progress bar
			std::cout << "#" << std::flush;
			nrIterationsDone += nrIterationsToDo;
		}
		std::cout << "]" << std::endl << std::endl;
	}
	else
	{
		// do all steps at once without any printing
		RunCoarseSimulationSteps(nrIterations_);
	}

	if (measureTime)
	{
		// report running times
		const auto& endTime = HelperFunctions::GetCurrentTime();
		const auto& timeSpent = HelperFunctions::GetIntervalMilliseconds(startTime, endTime);

		std::cout << "Time simulated: " << world_->GetCurrentTime() << " seconds." << std::endl;
		std::cout << "Computation time used: " << timeSpent / 1000 << " seconds." << std::endl;
	}

	if (writer_ != nullptr)
		writer_->Flush();
}

#pragma region [Loading a configuration file]

std::string getFolder(const std::string& filename)
{
/* 	std::filesystem::path path(filename);
	path.remove_filename();
	return path.string(); */
	auto endOfPath = filename.find_last_of('/');
	return (endOfPath == std::string::npos ? "" : filename.substr(0, endOfPath + 1));
}

bool CrowdSimulator::FromConfigFile_loadWorld(const tinyxml2::XMLElement* worldElement)
{
	// load the world type
	WorldBase::Type worldType;
	const char* type = worldElement->Attribute("type");
	if (type == nullptr || !WorldBase::WorldTypeFromString(type, worldType))
	{
		std::cerr << "Warning: No valid world type specified in the XML file. Selecting default type (Infinite)." << std::endl;
		worldType = WorldBase::Type::INFINITE_WORLD;
	}

	if (worldType == WorldBase::Type::INFINITE_WORLD)
	{
		world_ = std::make_unique<WorldInfinite>();
		//std::cout << "Created Infinite world." << std::endl;
	}

	// for toric worlds, load the width and height
	else if (worldType == WorldBase::Type::TORIC_WORLD)
	{
		float width = -1, height = -1;
		worldElement->QueryFloatAttribute("width", &width);
		worldElement->QueryFloatAttribute("height", &height);

		if (width > 0 && height > 0)
		{
			world_ = std::make_unique<WorldToric>(width, height);
			//std::cout << "Created Toric world, width " << width << " and height " << height << "." << std::endl;
		}
		else
		{
			std::cerr << "Error: No valid size specified for the toric world in the XML file." << std::endl
				<< "Make sure to specify a non-negative width and height." << std::endl;
			return false;
		}
	}

	return true;
}

bool CrowdSimulator::FromConfigFile_loadPoliciesBlock_ExternallyOrNot(const tinyxml2::XMLElement* xmlBlock, const std::string& fileFolder)
{
	// - if this block refers to another file, read it
	const char* externalFilename = xmlBlock->Attribute("file");
	if (externalFilename != nullptr)
	{
		tinyxml2::XMLDocument externalDoc;
		externalDoc.LoadFile((fileFolder + externalFilename).data());
		if (externalDoc.ErrorID() != 0)
		{
			std::cerr << "Could not load or parse Policies XML file at " << (fileFolder + externalFilename) << "." << std::endl
				<< "Please check this file location, or place your Policies in the main XML file itself." << std::endl;
			return false;
		}

		return FromConfigFile_loadPoliciesBlock(externalDoc.FirstChildElement("Policies"));
	}

	// - otherwise, read the agents straight from the file itself
	return FromConfigFile_loadPoliciesBlock(xmlBlock);
}

bool CrowdSimulator::FromConfigFile_loadAgentsBlock_ExternallyOrNot(const tinyxml2::XMLElement* xmlBlock, const std::string& fileFolder)
{
	// - if this block refers to another file, read it
	const char* externalFilename = xmlBlock->Attribute("file");
	if (externalFilename != nullptr)
	{
		tinyxml2::XMLDocument externalDoc;
		externalDoc.LoadFile((fileFolder + externalFilename).data());
		if (externalDoc.ErrorID() != 0)
		{
			std::cerr << "Could not load or parse Agents XML file at " << (fileFolder + externalFilename) << "." << std::endl
				<< "Please check this file location, or place your Agents in the main XML file itself." << std::endl;
			return false;
		}

		return FromConfigFile_loadAgentsBlock(externalDoc.FirstChildElement("Agents"));
	}

	// - otherwise, read the agents straight from the file itself
	return FromConfigFile_loadAgentsBlock(xmlBlock);
}

bool CrowdSimulator::FromConfigFile_loadObstaclesBlock_ExternallyOrNot(const tinyxml2::XMLElement* xmlBlock, const std::string& fileFolder)
{
	// - if this block refers to another file, read it
	const char* externalFilename = xmlBlock->Attribute("file");
	if (externalFilename != nullptr)
	{
		tinyxml2::XMLDocument externalDoc;
		externalDoc.LoadFile((fileFolder + externalFilename).data());
		if (externalDoc.ErrorID() != 0)
		{
			std::cerr << "Could not load or parse Obstacles XML file at " << (fileFolder + externalFilename) << "." << std::endl
				<< "Please check this file location, or place your Obstacles in the main XML file itself." << std::endl;
			return false;
		}

		return FromConfigFile_loadObstaclesBlock(externalDoc.FirstChildElement("Obstacles"));
	}

	// - otherwise, read the agents straight from the file itself
	return FromConfigFile_loadObstaclesBlock(xmlBlock);
}

bool CrowdSimulator::FromConfigFile_loadInteractionFieldsBlock_ExternallyOrNot(const tinyxml2::XMLElement* xmlBlock, const std::string& fileFolder)
{
	// - if this block refers to another file, read it
	const char* externalFilename = xmlBlock->Attribute("file");
	if (externalFilename != nullptr)
	{
		const std::string& fullPath = fileFolder + externalFilename;
		tinyxml2::XMLDocument externalDoc;
		externalDoc.LoadFile(fullPath.data());
		if (externalDoc.ErrorID() != 0)
		{
			std::cerr << "Could not load or parse IF XML file at " << (fileFolder + externalFilename) << "." << std::endl
				<< "Please check this file location, or place your IF in the main XML file itself." << std::endl;
			return false;
		}

		auto lastSlash = fullPath.find_last_of("/");
		const std::string& fullPathFolder = (lastSlash == std::string::npos ? fileFolder : fullPath.substr(0,lastSlash+1));

		return FromConfigFile_loadInteractionFieldsBlock(externalDoc.FirstChildElement("InteractionFields"), fullPathFolder);
	}

	// - otherwise, read the agents straight from the file itself
	return FromConfigFile_loadInteractionFieldsBlock(xmlBlock, fileFolder);
}

bool CrowdSimulator::FromConfigFile_loadPoliciesBlock(const tinyxml2::XMLElement* xmlBlock)
{
	// load the elements one by one
	const tinyxml2::XMLElement* element = xmlBlock->FirstChildElement();
	while (element != nullptr)
	{
		// load a single element
		if (!FromConfigFile_loadSinglePolicy(element))
			return false;

		// go to the next element
		element = element->NextSiblingElement("Policy");
	}

	return true;
}

bool CrowdSimulator::FromConfigFile_loadAgentsBlock(const tinyxml2::XMLElement* xmlBlock)
{
	// load the elements one by one
	const tinyxml2::XMLElement* element = xmlBlock->FirstChildElement();
	while (element != nullptr)
	{
		// load a single element
		if (!FromConfigFile_loadSingleAgent(element))
			return false;

		// go to the next element
		element = element->NextSiblingElement("Agent");
	}

	return true;
}

bool CrowdSimulator::FromConfigFile_loadObstaclesBlock(const tinyxml2::XMLElement* xmlBlock)
{
	// load the elements one by one
	const tinyxml2::XMLElement* element = xmlBlock->FirstChildElement();
	while (element != nullptr)
	{
		// load a single element
		if (!FromConfigFile_loadSingleObstacle(element))
			return false;

		// go to the next element
		element = element->NextSiblingElement("Obstacle");
	}

	return true;
}

bool CrowdSimulator::FromConfigFile_loadInteractionFieldsBlock(const tinyxml2::XMLElement* xmlBlock, const std::string& fileFolder)
{
	// load the elements one by one
	const tinyxml2::XMLElement* element = xmlBlock->FirstChildElement();
	while (element != nullptr)
	{
		// load a single element
		if (!FromConfigFile_loadSingleInteractionField(element, fileFolder))
			return false;

		// go to the next element
		element = element->NextSiblingElement("InteractionField");
	}

	return true;
}


bool CrowdSimulator::FromConfigFile_loadSinglePolicy(const tinyxml2::XMLElement* policyElement)
{
	// --- Read mandatory parameters

	// Unique policy ID
	int policyID;
	policyElement->QueryIntAttribute("id", &policyID);

	// --- Create the policy

	Policy* policy = new Policy(policyID);

	// --- Load the phases one by one

	auto* phaseElement = policyElement->FirstChildElement("Phase");
	while (phaseElement != nullptr)
	{
		const char* phaseName = phaseElement->Attribute("Result");
		if (phaseName == nullptr)
		{
			std::cerr << "Error: Policy " << policyID << ": A 'Phase' child element does not have a 'Result' attribute." << std::endl;
			delete policy;
			return false;
		}

		// Within a phase, load the steps one by one

		auto* stepElement = phaseElement->FirstChildElement("Step");
		while (stepElement != nullptr)
		{
			bool success = FromConfigFile_loadSinglePolicyStep(stepElement, policy, policyID, phaseName);
			if (!success)
			{
				std::cerr << "Error: Policy " << policyID << ": Failed to load one or more 'Step' child elements." << std::endl;
				delete policy;
				return false;
			}
			stepElement = stepElement->NextSiblingElement("Step");
		}
		
		// Go to the next phase

		phaseElement = phaseElement->NextSiblingElement("Phase");
	}

	// --- Save the policy in the simulator

	if (!world_->AddPolicy(policy))
	{
		std::cerr << "Error: Failed to add Policy " << policyID << " because its ID is already taken." << std::endl;
		delete policy;
		return false;
	}

	return true;
}

bool CrowdSimulator::FromConfigFile_loadSinglePolicyStep(const tinyxml2::XMLElement* policyStepElement, Policy* policyUnderConstruction, size_t policyID, const std::string& phaseName)
{
	PolicyStep* result = nullptr;

	// --- Read parameters that apply to all types of PolicyStep

	// Time step size
	
	// - default value: coarse simulation timestep
	PolicyStep::DeltaTimeType deltaTimeType = PolicyStep::DeltaTimeType::COARSE;
	float deltaTime = world_->GetDeltaTime_Coarse();

	// - specific supported value: fine simulation timestep
	float dt_fine = world_->GetDeltaTime_Fine();
	const char* dt_attribute = policyStepElement->Attribute("DeltaTime");
	if (dt_attribute != nullptr && std::string(dt_attribute) == "fine")
	{
		deltaTimeType = PolicyStep::DeltaTimeType::FINE;
		deltaTime = dt_fine;
	}

	// - other: a custom timestep
	else
	{
		float customDeltaTime;
		if (policyStepElement->QueryFloatAttribute("DeltaTime", &customDeltaTime) == tinyxml2::XML_SUCCESS)
		{
			if (customDeltaTime <= 0)
			{
				std::cerr << "Error: Policy " << policyID << ", step in phase \"" << phaseName << "\": Attribute 'DeltaTime' is invalid." << std::endl
					<< "Please specify a positive number, or either of the keywords 'fine' or 'coarse'." << std::endl;
				return false;
			}

			deltaTimeType = PolicyStep::DeltaTimeType::CUSTOM;
			deltaTime = customDeltaTime;
		}
	}

	// Type
	const char* stepTypeInput = policyStepElement->Attribute("Type");
	const std::string& stepType = (stepTypeInput == nullptr ? "CostFunctions" : stepTypeInput);

	// === PolicyStep based on cost functions

	if (stepType == "CostFunctions")
	{
		// (Optional; default = true) Whether or not this policy step should let the agent stop at the goal
		bool stopAtGoal = true;
		policyStepElement->QueryBoolAttribute("StopAtGoal", &stopAtGoal);

		// Optimization method
		auto methodName = policyStepElement->Attribute("OptimizationMethod");
		PolicyStep_CostFunctions::OptimizationMethod method;
		if (!PolicyStep_CostFunctions::OptimizationMethodFromString(methodName == nullptr ? "" : methodName, method))
		{
			std::cerr << "Error: Policy " << policyID << ", step in phase \"" << phaseName << "\": Attribute 'OptimizationMethod' is invalid or missing." << std::endl;
			return false;
		}

		// Sampling parameters
		PolicyStep_CostFunctions::SamplingParameters params;
		if (method == PolicyStep_CostFunctions::OptimizationMethod::SAMPLING)
		{
			policyStepElement->QueryAttribute("SamplingAngle", &params.angle);
			policyStepElement->QueryAttribute("SpeedSamples", &params.speedSamples);
			policyStepElement->QueryAttribute("AngleSamples", &params.angleSamples);
			policyStepElement->QueryAttribute("RandomSamples", &params.randomSamples);
			policyStepElement->QueryBoolAttribute("IncludeBaseAsSample", &params.includeBaseAsSample);

			// type of sampling (= random or regular)
			const char * res = policyStepElement->Attribute("SamplingType");
			if (res != nullptr && !PolicyStep_CostFunctions::SamplingParameters::TypeFromString(res, params.type))
				std::cerr << "Warning: Policy " << policyID << ", step in phase \"" << phaseName << "\": Attribute 'SamplingType' is invalid or missing, using default (regular)." << std::endl;

			// base velocity (= origin of the cone or circle being sampled)
			res = policyStepElement->Attribute("SamplingBase");
			if (res != nullptr && !PolicyStep_CostFunctions::SamplingParameters::BaseFromString(res, params.base))
				std::cerr << "Warning: Policy " << policyID << ", step in phase \"" << phaseName << "\": Attribute 'SamplingBase' is invalid or missing, using default (zero)." << std::endl;

			// base direction (= central direction of the cone or circle)
			res = policyStepElement->Attribute("SamplingBaseDirection");
			if (res != nullptr && !PolicyStep_CostFunctions::SamplingParameters::BaseDirectionFromString(res, params.baseDirection))
				std::cerr << "Warning: Policy " << policyID << ", step in phase \"" << phaseName << "\": Attribute 'SamplingBaseDirection' is invalid or missing, using default (current velocity)." << std::endl;

			// radius (= radius of the cone or circle)
			res = policyStepElement->Attribute("SamplingRadius");
			if (res != nullptr && !PolicyStep_CostFunctions::SamplingParameters::RadiusFromString(res, params.radius))
				std::cerr << "Warning: Policy " << policyID << ", step in phase \"" << phaseName << "\": Attribute 'SamplingRadius' invalid or missing, using default (preferred speed)." << std::endl;
		}

		// --- Create the policy step

		PolicyStep_CostFunctions* ps = new PolicyStep_CostFunctions(deltaTimeType, deltaTime, stopAtGoal, method, params);

		// --- Read and create cost functions

		// read all cost functions that belong to the policy; instantiate them one by one
		auto* funcElement = policyStepElement->FirstChildElement("CostFunction");
		while (funcElement != nullptr)
		{
			const auto& costFunctionName = funcElement->Attribute("name");
			CostFunction* costFunction = CostFunctionFactory::CreateCostFunction(costFunctionName, ps);
			if (costFunction != nullptr)
				ps->AddCostFunction(costFunction, CostFunctionParameters(funcElement));

			funcElement = funcElement->NextSiblingElement();
		}

		if (ps->GetNumberOfCostFunctions() == 0)
		{
			std::cerr << "Error: Policy " << policyID << ", step in phase \"" << phaseName << "\" needs at least one 'CostFunction' child element." << std::endl;
			delete ps;
			return false;
		}

		result = ps;
	}

	else if (stepType == "InteractionFields")
	{
		// (Optional; default = MaxFloat) Interaction range
		float interactionRange = MaxFloat;
		policyStepElement->QueryFloatAttribute("InteractionRange", &interactionRange);

		// (Optional; default = all) Types of IF sources to check: agents, obstacles, or all
		bool useAgentSources, useObstacleSources;
		const char* sourceTextInput = policyStepElement->Attribute("Source");
		const std::string& sourceText = (sourceTextInput == nullptr ? "all" : sourceTextInput);

		if (sourceText == "agents")
		{
			useAgentSources = true;
			useObstacleSources = false;
		}
		else if (sourceText == "obstacles")
		{
			useAgentSources = false;
			useObstacleSources = true;
		}
		else if (sourceText == "all")
		{
			useAgentSources = true;
			useObstacleSources = true;
		}
		else
		{
			std::cerr << "Error: Policy " << policyID << ", step in phase \"" << phaseName << "\": Attribute 'Source' has an invalid value." << std::endl
				<< "It should be 'agents', 'obstacles', or 'all'. You may also omit the attribute; then a default value of 'all' will be used." << std::endl;
			return false;
		}

		result = new PolicyStep_InteractionFields(deltaTimeType, deltaTime, interactionRange, useAgentSources, useObstacleSources);
	}

	// === Unknown type

	else
	{
		std::cerr << "Error: Policy " << policyID << ", step in phase \"" << phaseName << "\": Attribute 'Type' has an invalid value." << std::endl
			<< "It should be 'CostFunctions' or 'InteractionFields'. You may also omit the attribute; then a default value of 'CostFunctions' will be used." << std::endl;
		return false;
	}

	// --- Read optional parameters

	// Relaxation time
	float relaxationTime = 0;
	if (policyStepElement->QueryFloatAttribute("RelaxationTime", &relaxationTime) == tinyxml2::XMLError::XML_SUCCESS)
		result->setRelaxationTime(relaxationTime);

	// --- Save the step in the policy

	policyUnderConstruction->AddStep(phaseName, result);

	return true;
}

bool CrowdSimulator::FromConfigFile_loadSingleInteractionFieldReference(const tinyxml2::XMLElement* fieldElement, IFWithLinkObjects& result)
{
	// read the interaction field ID, and get the corresponding field from the world
	int IFID;
	fieldElement->QueryIntAttribute("id", &IFID);
	result.field = world_->GetInteractionField(IFID);
	if (result.field == nullptr)
	{
		std::cerr << "Error: The interaction field with id " << IFID << " doesn't exist." << std::endl;
		return false;
	}

	// (optional) read the list of object IDs that are link targets
	auto* linkElement = fieldElement->FirstChildElement("LinkObject");
	while (linkElement != nullptr)
	{
		const std::string& typeName = linkElement->Attribute("type");
		IFSourceType objectType = (typeName == "agent" ? IFSourceType::AgentSource : IFSourceType::ObstacleSource);
		size_t objectID = (size_t)linkElement->IntAttribute("id");
		result.linkObjects.push_back(IFSourceReference(objectType, objectID));

		linkElement = linkElement->NextSiblingElement("LinkObject");
	}

	return true;
}

bool CrowdSimulator::FromConfigFile_loadSingleAgent(const tinyxml2::XMLElement* agentElement)
{
	// optional ID
	int agentID = -1;
	agentElement->QueryIntAttribute("id", &agentID);

	// optional agent parameters (if they are not provided, we use the default ones)
	Agent::Settings settings;
	agentElement->QueryFloatAttribute("rad", &settings.radius_);
	agentElement->QueryFloatAttribute("pref_speed", &settings.preferred_speed_);
	agentElement->QueryFloatAttribute("max_speed", &settings.max_speed_);
	agentElement->QueryFloatAttribute("max_acceleration", &settings.max_acceleration_);
	agentElement->QueryFloatAttribute("mass", &settings.mass_);
	agentElement->QueryBoolAttribute("remove_at_goal", &settings.remove_at_goal_);
	agentElement->QueryFloatAttribute("time_ratio", &settings.timeRatio_);
	agentElement->QueryIntAttribute("group", &settings.group_);

	// optional color
	auto* colorElement = agentElement->FirstChildElement("color");
	if (colorElement)
	{
		 int r = -1, g = -1, b = -1;
		 colorElement->QueryIntAttribute("r", &r);
		 colorElement->QueryIntAttribute("g", &g);
		 colorElement->QueryIntAttribute("b", &b);
		 settings.color_ = Color((unsigned short)r, (unsigned short)g, (unsigned short)b);
	}

	// position
	float x, y;
	auto* positionElement = agentElement->FirstChildElement("pos");
	if (!positionElement)
	{
		std::cerr << "Error: Agent " << agentID << " needs a 'pos' child element." << std::endl;
		return false;
	}
	positionElement->QueryFloatAttribute("x", &x);
	positionElement->QueryFloatAttribute("y", &y);
	Vector2D position(x, y);

	// policy	
	auto* policyElement = agentElement->FirstChildElement("Policy");
	if (!policyElement)
	{
		std::cerr << "Error: Agent " << agentID << " needs a 'Policy' child element." << std::endl;
		return false;
	}

	int agentPolicyID;
	policyElement->QueryIntAttribute("id", &agentPolicyID);
	auto policy = world_->GetPolicy(agentPolicyID);
	if (policy == nullptr)
	{
		std::cerr << "Error: Agent " << agentID << " refers to policy ID " << agentPolicyID << ", but this policy does not exist." << std::endl;
		return false;
	}

	settings.policy_ = policy;



	// optional start time
	float startTime = 0;
	agentElement->QueryFloatAttribute("start_time", &startTime);

	// --- Add the agent to the world.

	Agent* agent = world_->AddAgent(position, settings, (agentID >= 0 ? (size_t)agentID : std::numeric_limits<size_t>::max()), startTime);

	// interaction fields
	auto* fieldElement = agentElement->FirstChildElement("InteractionField");
	while (fieldElement != nullptr)
	{
		IFWithLinkObjects result;
		if (FromConfigFile_loadSingleInteractionFieldReference(fieldElement, result))
		{
			if (result.field->getIsVelocity())
				agent->addInteractionFieldVelocity(result);
			else
				agent->addInteractionFieldOrientation(result);
		}

		fieldElement = fieldElement->NextSiblingElement("InteractionField");
	}

	// optional goal
	Vector2D goal = position;
	auto* goalElement = agentElement->FirstChildElement("goal");
	if (!goalElement)
	{
		std::cerr << "Warning: Agent " << agentID << " has no 'goal' child element." << std::endl;
		std::cerr << "This agent will not move." << std::endl;
	}
	else
	{
		goalElement->QueryFloatAttribute("x", &x);
		goalElement->QueryFloatAttribute("y", &y);
		goal = Vector2D(x, y);
	}

	agent->setGoal(goal);


	



	// optional trajectory	
	auto* trajectoryElement = agentElement->FirstChildElement("Trajectory");
	if (trajectoryElement != nullptr)
	{
		const std::string& filename = trajectoryElement->Attribute("csv");
		const Trajectory& traj = TrajectoryCSVReader::ReadTrajectoryFromCSVFile(filename);
		agent->setTrajectory(traj);
		agent->setGoal(Vector2D(1.5, 0));

	}

	return true;
}

bool CrowdSimulator::FromConfigFile_loadSingleObstacle(const tinyxml2::XMLElement* obstacleElement)
{
	// optional ID
	int obstacleID = -1;
	obstacleElement->QueryIntAttribute("id", &obstacleID);

	// optional attribute tangible
	bool tangible= true;
	obstacleElement->QueryBoolAttribute("tangible", &tangible);

	std::vector<Vector2D> points;

	// load coordinates
	auto* pointElement = obstacleElement->FirstChildElement("Point");
	while (pointElement != nullptr)
	{
		float x, y;
		pointElement->QueryFloatAttribute("x", &x);
		pointElement->QueryFloatAttribute("y", &y);
		pointElement = pointElement->NextSiblingElement("Point");
		points.push_back(Vector2D(x, y));
	}
	// create the obstacle and set its position
	Obstacle* obstacle = new Obstacle(obstacleID, points);
	obstacle->setisSolid(tangible);


	// add obstacle to world
	world_->AddObstacle(obstacleID, obstacle);

	// interaction fields
	auto* fieldElement = obstacleElement->FirstChildElement("InteractionField");
	while (fieldElement != nullptr)
	{
		IFWithLinkObjects result;
		if (FromConfigFile_loadSingleInteractionFieldReference(fieldElement, result))
		{
			if (result.field->getIsVelocity())
				obstacle->addInteractionFieldVelocity(result);
			else
				obstacle->addInteractionFieldOrientation(result);
		}

		fieldElement = fieldElement->NextSiblingElement("InteractionField");
	}

	return true;
}

bool CrowdSimulator::FromConfigFile_loadSingleInteractionField(const tinyxml2::XMLElement* interactionFieldElement, const std::string& fileFolder)
{
	// optional ID
	int id = -1;
	interactionFieldElement->QueryIntAttribute("id", &id);
	size_t interactionFieldID = (size_t)id;

	// optional interaction field parameters (if they are not provided, we use the default ones)
	InteractionField::Settings settings;
	interactionFieldElement->QueryFloatAttribute("weight", &settings.weight_);
	interactionFieldElement->QueryFloatAttribute("action_time", &settings.appearanceTime_);
	interactionFieldElement->QueryBoolAttribute("isVelocity", &settings.isVelocityIF_);
	interactionFieldElement->QueryIntAttribute("group", &settings.group_);

	// check if the interaction field is parametric
	const char* parameter = nullptr; 
	if (interactionFieldElement->QueryStringAttribute("parameter", &parameter) == tinyxml2::XML_SUCCESS)
	{
		if (std::string(parameter) == "speed")
			settings.parameter_ = InteractionField::ParameterType::SourceSpeed;
		if (std::string(parameter) == "manual")
			settings.parameter_ = InteractionField::ParameterType::ManualParameter;
	}

	InteractionField *intField = new InteractionField(settings, interactionFieldID);
	
	// there should be at least one Matrix element
	auto* matrixElement = interactionFieldElement->FirstChildElement("Matrix");
	if (matrixElement == nullptr)
	{
		std::cerr << "Error: Interaction Field " << interactionFieldID << " needs a Matrix element." << std::endl;
		return false;
	}

	// read the matrices one by one
	while (matrixElement != nullptr)
	{
		const std::string& filePath = matrixElement->Attribute("file");
		float parameterValue = 0;
		matrixElement->QueryFloatAttribute("parameterValue", &parameterValue);
		bool success = intField->loadMatrixFromFile(fileFolder + filePath, parameterValue);
		if (!success)
			return false;

		matrixElement = matrixElement->NextSiblingElement("Matrix");
	}

	if (!world_->AddInteractionField((int)interactionFieldID, intField))
	{
		std::cerr << "Error: Failed to add Interaction Field " << interactionFieldID << " because its ID is already taken" << std::endl;
		delete intField;
		return false;
	}
	return true;
}


bool CrowdSimulator::FromID_loadSingleInteractionFieldReference(int IFID, IFWithLinkObjects& result)
{
	// read the interaction field ID, and get the corresponding field from the world
	result.field = world_->GetInteractionField(IFID);
	if (result.field == nullptr)
	{
		std::cerr << "Error: The interaction field with id " << IFID << " doesn't exist." << std::endl;
		return false;
	}

	return true;
}

bool CrowdSimulator::FromID_assignAgentToIF(IFWithLinkObjects& result, int idAgent)
{
	if (result.field == nullptr)
	{
		std::cerr << "Error: The interaction field is wrong during linking agent" << std::endl;
		return false;
	}

	// (optional) read the list of object IDs that are link targets
	IFSourceType objectType = IFSourceType::AgentSource;
	result.linkObjects.push_back(IFSourceReference(objectType, idAgent));

	std::cout << "Link " << objectType << " " << idAgent << " to " << result.field->getID() << std::endl;

	return true;
}

bool CrowdSimulator::FromID_assignObstToIF(IFWithLinkObjects& result, int idObst)
{
	if (result.field == nullptr)
	{
		std::cerr << "Error: The interaction field is wrong during linking obst" << std::endl;
		return false;
	}

	// (optional) read the list of object IDs that are link targets
	IFSourceType objectType = IFSourceType::ObstacleSource;
	result.linkObjects.push_back(IFSourceReference(objectType, idObst));

	std::cout << "Link " << objectType << " " << idObst << " to " << result.field->getID() << std::endl;

	return true;
}


CrowdSimulator* CrowdSimulator::FromConfigFile(const std::string& filename)
{
	//std::setlocale(LC_NUMERIC, "en_US.UTF-8");

	// Parse the XML into the property tree.
	// If the path cannot be resolved, an exception is thrown.
	tinyxml2::XMLDocument doc;
	doc.LoadFile(filename.data());
	if (doc.ErrorID() != 0)
	{
		std::cerr << "Error: Could not load or parse XML file at " << filename << std::endl;
		return nullptr;
	}

	const std::string& fileFolder = getFolder(filename);

	// --- Check if this is a "main" config file that refers to another config file.

	tinyxml2::XMLElement* simConfigPathElement = doc.FirstChildElement("configPath");
	if (simConfigPathElement != nullptr)
	{
		// Location of the config file should be relative to the location of the *main* config file.
		// Check if the main config file lies in a subfolder.
		return CrowdSimulator::FromConfigFile(fileFolder + simConfigPathElement->Attribute("path"));
	}

	// --- Otherwise, we assume that this is a "regular" config file that contains the simulation itself.

	tinyxml2::XMLElement* simulationElement = doc.FirstChildElement("Simulation");
	if (simulationElement == nullptr)
	{
		std::cerr << "Error: No main 'Simulation' element in the XML file." << std::endl;
		return nullptr;
	}

	CrowdSimulator* crowdsimulator = new CrowdSimulator();
	crowdsimulator->scenarioFilename_ = filename;

	//
	// --- Read the world parameters
	//

	tinyxml2::XMLElement* worldElement = simulationElement->FirstChildElement("World");
	if (worldElement == nullptr)
	{
		std::cerr << "Error: No 'World' element in the XML file. The simulation cannot be loaded." << std::endl;
		delete crowdsimulator;
		return nullptr;
	}
	if (!crowdsimulator->FromConfigFile_loadWorld(worldElement))
	{
		std::cerr << "Error while loading the 'World' element. The simulation cannot be loaded." << std::endl;
		delete crowdsimulator;
		return nullptr;
	}

	//
	// --- Read the simulation parameters
	//

	// the coarse time step, specified as either "delta_time" or "delta_time_coarse"
	float delta_time_coarse = -1;
	simulationElement->QueryFloatAttribute("delta_time", &delta_time_coarse);
	if (delta_time_coarse <= 0)
		simulationElement->QueryFloatAttribute("delta_time_coarse", &delta_time_coarse);
	if (delta_time_coarse <= 0)
	{
		std::cout << "Error: 'Simulation' element: Attribute 'delta_time_coarse' is invalid or missing." << std::endl;
		delete crowdsimulator;
		return nullptr;
	}

	// the fine time step (optional; default value = equal to the coarse time step)
	float delta_time_fine = delta_time_coarse;
	simulationElement->QueryFloatAttribute("delta_time_fine", &delta_time_fine);
	if (delta_time_fine > delta_time_coarse)
	{
		std::cout << "Warning: 'Simulation' element: Attribute 'delta_time_fine' has a larger value than 'delta_time_coarse'." << std::endl
			<< "This setting will be ignored." << std::endl;
		delta_time_fine = delta_time_coarse;
	}

	crowdsimulator->GetWorld()->SetDeltaTime(delta_time_coarse, delta_time_fine);

	// the total simulation time (optional)
	float end_time = -1;
	simulationElement->QueryFloatAttribute("end_time", &crowdsimulator->end_time_);

	// time integration method (optional)
	const char * tim = simulationElement->Attribute("integration");
	WorldBase::TimeIntegrationMethod timeIntegrationMethod;
	if (tim != nullptr && WorldBase::TimeIntegrationMethodFromString(tim, timeIntegrationMethod))
		crowdsimulator->GetWorld()->SetTimeIntegrationMethod(timeIntegrationMethod);

	//
	// --- Read policies
	//

	// read the block with all policies
	tinyxml2::XMLElement* policiesElement = simulationElement->FirstChildElement("Policies");
	if (policiesElement != nullptr && !crowdsimulator->FromConfigFile_loadPoliciesBlock_ExternallyOrNot(policiesElement, fileFolder))
	{
		std::cerr << "Error while loading the 'Policies' element. The simulation cannot be loaded." << std::endl;
		delete crowdsimulator;
		return nullptr;
	}

	// if there are no policies at this point, print an error, and stop loading
	if (!crowdsimulator->GetWorld()->HasPolicies())
	{
		std::cerr
			<< "Error: Failed to load any policies for the simulation." << std::endl
			<< "A simulation needs a 'Policies' child element with at least one valid 'Policy' element." << std::endl;
		delete crowdsimulator;
		return nullptr;
	}

	//
	// --- Read interaction fields
	//

	// read the block with all interaction fields
	tinyxml2::XMLElement* interactionFieldsElement = simulationElement->FirstChildElement("InteractionFields");
	if (interactionFieldsElement != nullptr && !crowdsimulator->FromConfigFile_loadInteractionFieldsBlock_ExternallyOrNot(interactionFieldsElement, fileFolder))
	{
		std::cerr << "Error while loading interaction field. The simulation cannot be loaded." << std::endl;
		delete crowdsimulator;
		return nullptr;
	}
	//
	// --- Read agents
	//

	// read the block with all agents
	tinyxml2::XMLElement* agentsElement = simulationElement->FirstChildElement("Agents");
	if (agentsElement != nullptr && !crowdsimulator->FromConfigFile_loadAgentsBlock_ExternallyOrNot(agentsElement, fileFolder))
	{
		std::cerr << "Error while loading the 'Agents' element. The simulation cannot be loaded." << std::endl;
		delete crowdsimulator;
		return nullptr;
	}

	// if there are no agents at this point, print a warning (but not an error, because an empty crowd is allowed)
	if (crowdsimulator->GetWorld()->GetAgents().empty())
	{
		std::cerr << "Warning: Failed to load any agents for the simulation." << std::endl
			<< "The simulation will start without agents." << std::endl;
	}

	// 
	// --- Read obstacles
	//

	// read the block with all obstacles
	tinyxml2::XMLElement* obstaclesElement = worldElement->FirstChildElement("Obstacles");
	if (obstaclesElement != nullptr && !crowdsimulator->FromConfigFile_loadObstaclesBlock_ExternallyOrNot(obstaclesElement, fileFolder))
	{
		std::cerr << "Error while loading the 'Obstacles' element. The simulation cannot be loaded." << std::endl;
		delete crowdsimulator;
		return nullptr;
	}

	std::cout << "Simulation successfully loaded." << std::endl;
	if (delta_time_coarse != delta_time_fine)
		std::cout << "The simulation will use two step sizes: " << delta_time_coarse << " seconds (coarse) and " << delta_time_fine << " seconds (fine)." << std::endl << std::endl;
	else
		std::cout << "The simulation step size will be " << delta_time_fine << " seconds." << std::endl << std::endl;

	return crowdsimulator;
}





#pragma endregion
