/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettré
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#ifndef LIB_WORLD_BASE_H
#define LIB_WORLD_BASE_H

#include <core/PhantomAgent.h>
#include <tools/Polygon2D.h>
#include <core/obstacle.h>
#include <core/AgentKDTree.h>

#include <queue>
#include <unordered_map>
typedef std::unordered_map<size_t, size_t> AgentIDMap;

/// <summary>An abstract class describing a world in which a simulation can take place.</summary>
class WorldBase
{
public:

	/// <summary>An enum containing the types of world in which a simulation takes place.</summary>
	enum Type { INFINITE_WORLD, TORIC_WORLD };
	static bool WorldTypeFromString(const std::string& type, WorldBase::Type& result);

	/// <summary>An enum describing the possible time integration methods used by the simulation.</summary>
	enum class TimeIntegrationMethod
	{
		/// <summary>Indicates that the simulation uses Euler integration to update its velocity and position.</summary>
		EULER,
		/// <summary>Indicates that the simulation uses Verlet integration to update its velocity and position.</summary>
		VERLET
	};
	static bool TimeIntegrationMethodFromString(const std::string &method, TimeIntegrationMethod& result);

private:

	typedef std::pair<Agent*, double> AgentTimePair; 
	struct AgentTimePairComparator
	{
		bool operator()(const AgentTimePair& a, const AgentTimePair& b)
		{
			return a.second > b.second;
		}
	};

	typedef std::priority_queue<AgentTimePair, std::vector<AgentTimePair>, AgentTimePairComparator> AgentQueue;
	
	/// <summary>A list of agents (sorted by time) that will be added to the simulation in the future.</summary>
	AgentQueue agentsToAdd;

	/// <summary>A mapping from agent IDs to positions in the agents_ list.</summary>
	/// <remarks>Because agents can be removed during the simulation, the ID of an agent is not necessarily the same 
	/// as its position in the list. This is why we need this extra administration.
	/// (We could also put all agents directly in a map, but then we could not loop over the agents in parallel with OpenMP.)</remarks>
	AgentIDMap agentPositionsInVector;

	std::vector<size_t> interactionField_ids;

	/// <summary>The agent ID that will be used for the next agent that gets added.</summary>
	size_t nextUnusedAgentID;
	size_t nextUnusedObstacleID;

	/// <summary>The time integration method used by this Policy.</summary>
	TimeIntegrationMethod timeIntegrationMethod_;

	/// <summary>A list containing all navigation policies, ordered by ID.
	/// Each agent uses one of these policies for its navigation.</summary>
	std::map<int, Policy*> policies_;

	/// <summary>A list containing all interaction fields, ordered by ID.
	/// Agents can use one or more of these IFs for their behavior.</summary>
	std::map<int, InteractionField*> interactionFields_;

protected:
	 
	/// <summary>The type of this world, e.g. infinite or toric.</summary>
	const Type type_;

	std::vector<Obstacle*> obstacles_;

	/// <summary>A list containing all agents that are currently in the crowd.</summary>
	std::vector<Agent*> agents_;

	/// <summary>A kd-tree of agent positions, used for nearest-neighbor queries.</summary>
	AgentKDTree* agentKDTree;

	/// <summary>The length (in seconds) if a coarse simulation step.</summary>
	float delta_time_coarse_;

	/// <summary>The length (in seconds) of a fine simulation step.</summary>
	float delta_time_fine_;

	/// <summary>The simulation time (in seconds) that has passed so far.</summary>
	double time_;
	size_t frameNr_;
	size_t nrFramesPerCoarseStep_;

	/// <summary>Whether or not an agent has been removed in the last frame.</summary>
	bool agentRemoved_;
	
public:

#pragma region [Basic getters]
	/// @name Basic getters
	/// Methods that directly return a value stored in the world.
	/// @{

	/// <summary>Returns the list of agents. Use this if you want to retrieve information from all agents in an arbitrary order.</summary>
	/// <returns>A non-mutable reference to the list of Agent objects.</returns>
	inline const std::vector<Agent*>& GetAgents() const { return agents_; }

	/// <summary>Returns the list of obstacles.</summary>
	/// <returns>A non-mutable reference to the list of Polygon2D objects, each representing one obstacle in the world.</returns>
	inline const std::vector<Obstacle*>& GetObstacles() const { return obstacles_; }

	/// <summary>Returns the current simulation time (in seconds).</summary>
	/// <returns>The time (in seconds) that has been simulated since the simulation started.</returns>
	inline double GetCurrentTime() const { return time_; }

	inline size_t GetCurrentFrameNumber() const { return frameNr_; }

	/// <summary>Returns the duration of a coarse simulation time step (in seconds), i.e. the time after which the simulation updates any aspects that do not use the fine delta time.</summary>
	/// <returns>The duration of a coarse simulation time step (in seconds).</summary>
	inline float GetDeltaTime_Coarse() const { return delta_time_coarse_; }

	/// <summary>Returns the duration of a fine simulation time step (in seconds), i.e. the time that is simulated in a single execution of DoStep().</summary>
	/// <returns>The duration of a fine simulation time step (in seconds).</summary>
	inline float GetDeltaTime_Fine() const { return delta_time_fine_; }
	
	/// <summary>Returns whether or not a full coarse timestep has passed in the current fine timestep.</summary>
	/// <returns>true if the current simulation frame marks a new coarse timestep; false otherwise.</returns>
	bool IsCurrentFrameCoarseStep() const;

	/// <summary>Returns the type of this world, i.e. infinite or toric.</summary>
	/// <returns>The value of the Type enum describing the type of this world.</returns>
	inline Type GetType() { return type_; }

	/// <summary>Returns the time integration method used by the simulation.</summary>
	TimeIntegrationMethod GetTimeIntegrationMethod() const { return timeIntegrationMethod_; }

	/// @}
#pragma endregion

#pragma region [Basic setters]
	/// @name Basic setters
	/// Methods that directly change the world's internal status.
	/// @{

	/// <summary>Sets the number of parallel threads that this class may use for the simulation.</summary>
	/// <remarks>The given number is sent to OpenMP. Whether this number can be used depends on the numer of (virtual) cores of your machine.</remarks>
	/// <param name="nrThreads">The desired number of threads to use.</param>
	void SetNumberOfThreads(int nrThreads);

	/// <summary>Sets the length of coarse sand fine simulation timesteps.</summary>
	/// <remarks>Note: This also updates any policies that explicitly refer to the coarse or fine timestep.</remarks>
	/// <param name="dt_coarse">The desired length (in seconds) of a coarse simulation frame.</param>
	/// <param name="dt_fine">The desired length (in seconds) of a fine simulation frame.</param>
	void SetDeltaTime(float dt_coarse, float dt_fine);

	/// <summary>Sets the time integration method of the simulation to the specified value.</summary>
	inline void SetTimeIntegrationMethod(TimeIntegrationMethod tim) { timeIntegrationMethod_ = tim; }

	/// @}
#pragma endregion

	/// <summary>Performs a single fine simulation step. 
	/// In one step, all agents move forward based on their last computed acceleration. 
	/// Agents may also perform other tasks, depending on the framerates in their navigation policy.</summary>
	void DoStep();
	
	/// <summary>Computes and returns a list of all agents that are within a given radius of a given position.</summary>
	/// <remarks>Subclasses of WorldBase must implement this method, because the result may depend on special properties (e.g. the wrap-around effect in WorldToric).</remarks>
	virtual NeighborList ComputeNeighbors(const Vector2D& position, float search_radius, const Agent* queryingAgent) const = 0;

#pragma region [Finding, adding, and removing agents]
	/// @name Finding, adding, and removing agents
	/// Methods for finding, adding, and removing agents in the simulation.
	/// @{

	/// <summary>Finds and returns the agent with the given ID, if it exists.</summary>
	/// <param name="id">The ID of the agent to find.</param>
	/// <returns>A mutable pointer to the Agent stored under the given ID, or nullptr if this agent does not exist.</returns>
	Agent* GetAgent(size_t id);

	/// <summary>Finds and returns the agent with the given ID, if it exists.</summary>
	/// <param name="id">The ID of the agent to find.</param>
	/// <returns>A non-mutable pointer to the Agent stored under the given ID, or nullptr if this agent does not exist.</returns>
	const Agent* GetAgent_const(size_t id) const;

	Obstacle* GetObstacle(size_t id);
	const Obstacle* GetObstacle_const(size_t id) const;

	bool AddInteractionField(int id, InteractionField * intField);
	InteractionField * GetInteractionField(int id);

	/// <summary>Creates a new Agent object to be added to the simulation at the given time.</summary>
	/// <remarks>If the given time has already been reached, the agent will be added to the simulation immediately. 
	/// Otherwise, the agent will be scheduled for insertion at the given moment.</remarks>
	/// <param name="position">The start position of the agent.</param>
	/// <param name="settings">The simulation settings of the agent.</param>
	/// <param name="desiredID">(optional) The desired ID of the agent. 
	/// If it is not set, or if this ID is already taken, a suitable ID will be chosen automatically.</param>
	/// <param name="startTime">(optional) The simulation time at which the agent should be added. 
	/// If it is not set (or set to a value lower than the current simulation time), the agent will be added immediately.
	/// Otherwise, the agent will be scheduled for insertion at the given moment.</param>
	/// <returns>A pointer to the newly created Agent object. The agent may have a different ID than the desired one.
	/// Also, the WorldBase class manages the memory of agents, so you do not have to delete this object yourself.</returns>
	Agent* AddAgent(const Vector2D& position, const Agent::Settings& settings, size_t desiredID = std::numeric_limits<size_t>::max(), float startTime = 0);

	/// <summary>Tries to remove the agent with the given ID from the simulation.</summary>
	/// <param name="id">The ID of the agent to remove.</param>
	/// <returns>true if the agent was successfully removed; false otherwise, i.e. if the agent with the given ID does not exist.</returns>
	bool RemoveAgent(size_t id);

	/// @}
#pragma endregion

#pragma region [Policies]

	/// <summary>Stores a navigation policy inside the simulation, using its unique ID as an index.</summary>
  /// <param name="policy">A Policy object.</param>
  /// <returns>true if the policy was succesfully added; false otherwise, i.e. if the ID was already taken.</returns>
	bool AddPolicy(Policy* policy);

	/// <summary>Finds and returns the navigation policy with the given ID, or returns nullptr if it does not exist.</summary>
	/// <param name="id">The ID of the policy to find.</param>
	/// <returns>A pointer to the Policy stored under the given ID, or nullptr if no such Policy exists.</returns>
	Policy* GetPolicy(int id);

	inline bool HasPolicies() const { return !policies_.empty(); }

#pragma endregion

	/// <summary>Adds an obstacle with the given vertices to the world.</summary>
	/// <param name="points">A sequence of 2D points defining the obstacle's boundary vertices.</param>
	virtual void AddObstacle(size_t desiredID, Obstacle* obstacle);

	/// <summary>Cleans up this WorldBase object for removal.</summary>
	virtual ~WorldBase();

protected:

	/// <summary>Creates a WorldBase object of the given type.</summary>
	WorldBase(Type type);

	/// <summary>Computes a list of all agents that lie within a given radius of a given position.</summary>
	/// <param name="position">A query position.</param>
	/// <param name="search_radius">A query radius.</param>
	/// <param name="queryingAgent">A pointer to the Agent object performing the query. This agent will be excluded from the results.
	/// Use nullptr to not exclude any agents.</param>
	/// <param name="result">[out] Will store a list of pointers to agents queryingAgent lie within "search_radius" meters of "position", 
	/// excluding the agent denoted by "queryingAgent" (if it exists).</param>
	void computeNeighboringAgents_Flat(const Vector2D& position, float search_radius, const Agent* queryingAgent, std::vector<const Agent*>& result) const;

	void computeNeighboringObstacles_Flat(const Vector2D& position, float search_radius, std::vector<const Obstacle*>& result) const;

	/// <summary>Subroutine of DoStep() that can override the positions of agent if a specific world type demands this.</summary>
	/// <remarks>By default, this method does nothing. Subclasses of WorldBase may override this method if they require special behavior (e.g. the wrap-around effect in WorldToric).</remarks>
	virtual void DoStep_CheckAgentPositions() {}

private:

	void DoStep_MainTasks(bool agentAdded);

	/// Adds a (previously created) agent to the simulation.
	void addAgentToList(Agent* agent);

	/// Removes the agent at a given position in the list, 
	/// and does the necessary management to keep this list valid.
	void removeAgentAtListIndex(size_t index);

	//void sendOSCMessage(int id, float x, float y);
	//void sendOSCMessageVector(std::vector<Vector2D, float> positions);
};

#endif //LIB_WORLD_BASE_H


