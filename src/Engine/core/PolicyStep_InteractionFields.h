/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettr�
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#ifndef LIB_POLICYSTEP_INTERACTIONFIELDS_H
#define LIB_POLICYSTEP_INTERACTIONFIELDS_H

#include <core/PolicyStep.h>

class PolicyStep_InteractionFields : public PolicyStep
{
private:
	bool useAgentSources_;
	bool useObstacleSources_;
	float interactionRange_;

public:

	PolicyStep_InteractionFields(DeltaTimeType deltaTimeType, float deltaTime, float interactionRange, bool useAgentSources, bool useObstacleSources)
		: PolicyStep(deltaTimeType, deltaTime), useAgentSources_(useAgentSources), interactionRange_(interactionRange), useObstacleSources_(useObstacleSources) {}
	
	~PolicyStep_InteractionFields() {}

	virtual Vector2D ComputeAcceleration(Agent* agent, WorldBase* world) override;
	virtual Vector2D ComputeNewViewingDirection(Agent* agent, WorldBase* world) override;
	virtual Vector2D ComputePredictionPosition(Agent* agent, WorldBase* world, int indexPrediction) override;
	virtual Vector2D ComputePredictionViewingDirection(Agent* agent, WorldBase* world, int indexPrediction) override;

	virtual float getInteractionRange() const override { return interactionRange_; }

private:

	Vector2D getNewVelocityFromIFs(Agent * agent, WorldBase * world);
	Vector2D getNewViewingDirectionFromIFs(Agent *agent, WorldBase * world);
	Vector2D getNewVelocityPredictionFromIFs(Agent * agent, WorldBase * world, int indexPrediction);
	Vector2D getNewViewingDirectionPredictionFromIFs(Agent *agent, WorldBase * world, int indexPrediction);
};

#endif //LIB_POLICYSTEP_INTERACTIONFIELDS_H
