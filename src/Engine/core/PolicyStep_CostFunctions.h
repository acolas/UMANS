/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettr�
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#ifndef LIB_POLICYSTEP_COSTFUNCTIONS_H
#define LIB_POLICYSTEP_COSTFUNCTIONS_H

#include <core/PolicyStep.h>
#include <3rd-party/tinyxml/tinyxml2.h>

class WorldBase;
class Agent;
class Obstacle;
class CostFunction;
class CostFunctionParameters;

typedef std::vector<std::pair<const CostFunction*, float>> CostFunctionList;

class PolicyStep_CostFunctions : public PolicyStep
{
public:
	/// <summary>A set of sampling parameters that can be used by a Policy.</summary>
	struct SamplingParameters
	{
		/// <summary>An enum describing the possible types of sampling: regular or random.</summary>
		enum class Type
		{
			REGULAR,
			RANDOM
		};
		static bool TypeFromString(const std::string &method, Type& result);

		/// <summary>An enum describing the possible base velocities for sampling, i.e. the center of the disk in which samples are taken.</summary>
		enum class Base
		{
			ZERO, CURRENT_VELOCITY
		};
		static bool BaseFromString(const std::string &method, Base& result);

		/// <summary>An enum describing the possible base directions for sampling, i.e. the 'neutral' direction around which samples are taken.</summary>
		enum class BaseDirection { UNIT, CURRENT_VELOCITY, PREFERRED_VELOCITY };
		static bool BaseDirectionFromString(const std::string &method, BaseDirection& result);

		/// <summary>An enum describing the possible radii for sampling, i.e. the radius of the disk in which samples are taken.</summary>
		enum class Radius { PREFERRED_SPEED, MAXIMUM_SPEED, MAXIMUM_ACCELERATION };
		static bool RadiusFromString(const std::string &method, Radius& result);

		// Default parameters for sampling:

		Type type = Type::REGULAR;
		Base base = Base::ZERO;
		BaseDirection baseDirection = BaseDirection::CURRENT_VELOCITY;
		Radius radius = Radius::PREFERRED_SPEED;
		float angle = 180;
		int speedSamples = 4;
		int angleSamples = 11;
		int randomSamples = 100;
		bool includeBaseAsSample = false;

		/// <summary>Creates a SamplingParameters object with the default settings for approximating global optimization.</summary>
		static SamplingParameters ApproximateGlobalOptimization()
		{
			SamplingParameters params;
			params.type = Type::REGULAR;
			params.base = Base::ZERO;
			params.baseDirection = BaseDirection::UNIT;
			params.radius = Radius::MAXIMUM_SPEED;
			params.angle = 360;
			params.angleSamples = 36;
			params.speedSamples = 11;
			params.includeBaseAsSample = true;
			return params;
		}
	};

	/// <summary>An enum describing the possible optimization methods of a PolicyStep.</summary>
	enum class OptimizationMethod
	{
		/// <summary>Indicates that a PolicyStep computes an acceleration by following the gradient of its cost functions.</summary>
		GRADIENT,
		/// <summary>Indicates that a PolicyStep uses sampling to create several candidate velocities, 
		/// computes the cost of each candidate, and returns an acceleration towards the candidate with the lowest cost.</summary>
		SAMPLING,
		/// <summary>Indicates that a PolicyStep computes a "best velocity" by global optimization of its cost function, 
		/// and returns an acceleration towards this best velocity.
		/// This option is useful for cost functions that have a closed-form solution for finding the optimum.</summary>
		GLOBAL
	};
	static bool OptimizationMethodFromString(const std::string &method, OptimizationMethod& result);

private:
	/// <summary>A weighted list of cost functions used by this Policy.</summary>
	CostFunctionList cost_functions_;
	/// <summary>The optimization method used by this Policy.</summary>
	OptimizationMethod optimizationMethod_ = OptimizationMethod::GRADIENT;
	/// <summary>The sampling parameters used by this Policy. Only used if the optimization method is OptimizationMethod::SAMPLING.</summary>
	SamplingParameters samplingParameters_;

	bool stopAtGoal_;

public:
	/// <summary>Creates a PolicyStep_CostFunctions with the given details.</summary>
	/// <param name="deltaTimeType">Whether the PolicyStep should use a custom deltaTime or one of the simulation's default framerates.</param>
	/// <param name="deltaTime">The frame length (in seconds) that this step of the policy sould use.</param>
	/// <param name="method">The optimization method that this PolicyStep should use.</param>
	/// <param name="samplingParameters">The sampling parameters that this PolicyStep should use. 
	/// Only used if the optimization method is OptimizationMethod::SAMPLING.</param>
	PolicyStep_CostFunctions(DeltaTimeType deltaTimeType, float deltaTime, bool stopAtGoal, OptimizationMethod method, SamplingParameters params)
		: PolicyStep(deltaTimeType, deltaTime), stopAtGoal_(stopAtGoal), optimizationMethod_(method), samplingParameters_(params) {}

	/// <summary>Destroys this PolicyStep_CostFunctions and all cost functions inside it.</summary>
	~PolicyStep_CostFunctions();

	/// <summary>Computes and returns a new acceleration vector for a given agent, using the cost functions and optimization method of this Policy.</summary>
	/// <param name="agent">The agent for which a new acceleration should be computed.</param>
	/// <param name="world">The world in which the simulation takes place.</param>
	virtual Vector2D ComputeAcceleration(Agent* agent, WorldBase* world) override;

	virtual Vector2D ComputePredictionPosition(Agent* agent, WorldBase* world, int indexPrediction)override;

	/// <summary>Adds a cost function to this Policy's list of cost functions.</summary>
	/// <param name="costFunction">A pointer to an alraedy created cost function.</param>
	/// <param name="params">An XML object containing all parameters that the cost function may want to read.</param>
	void AddCostFunction(CostFunction* costFunction, const CostFunctionParameters& params);

	/// <summary>Returns the number of cost functions used by this Policy.</summary>
	size_t GetNumberOfCostFunctions() const { return cost_functions_.size(); }

	/// <summary>Finds and returns the range of interaction of this policy.</summary>
	/// <remarks>This is the largest "range" value among all cost functions in this policy.
	/// It is the radius (in meters) that the agent should use for its nearest-neighbor query.</remarks>
	virtual float getInteractionRange() const override;

	/// <summary>Uses sampling to approximate the global minimum of a list of cost functions.
	/// <remarks>This method tries out several candidate velocities (sampled according to 'params'), 
	/// computes the total cost for each candidate (combining all functions in 'costFunctions'), 
	/// and returns the velocity with the lowest cost.</remarks>
	/// <param name="agent">The agent for which the optimal velocity is requested.</param>
	/// <param name="world">The world in which the simulation takes place.</param>
	/// <param name="params">Parameters for sampling the velocity space.</param>
	/// <param name="costFunctions">A list of cost functions to evaluate.</param>
	/// <returns>The sample velocity for which the sum of all cost-function values is lowest.</param>
	static Vector2D ApproximateGlobalMinimumBySampling(Agent* agent, const WorldBase* world,
		const PolicyStep_CostFunctions::SamplingParameters& params, const CostFunctionList& costFunctions, const float dt);

private:
	/// <summary>Computes an acceleration vector for an agent by following the gradient of this Policy's cost functions.</summary>
	Vector2D getAccelerationFromGradient(Agent* agent, WorldBase* world);
	/// <summary> Computes the best new velocity for an agent by finding the global minimum of this Policy's cost functions. 
	/// If the cost function does not have a closed-form global optimum, or if the Policy has more than one cost function,approximateGlobalMinimumBySampling
	/// this method will use sampling to *approximate* the solution.</summary>
	Vector2D getBestVelocityGlobal(Agent* agent, WorldBase* world);
	/// <summary>Computes the best velocity for an agent by approaching the global minimum of this Policy's cost function via sampling.</summary>
	Vector2D getBestVelocitySampling(Agent* agent, WorldBase* world, const SamplingParameters& params);
};

#endif //LIB_POLICYSTEP_COSTFUNCTIONS_H
