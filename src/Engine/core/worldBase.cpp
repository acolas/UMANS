/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettr�
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#include <core/worldBase.h>
#include <core/policy.h>
#include <core/agent.h>
#include <omp.h>
//#include <tools/OCSReceiverHandle.h>

//using namespace osc;
using namespace nanoflann;
using namespace std;

bool WorldBase::WorldTypeFromString(const std::string& type, WorldBase::Type& result)
{
	if (type == "Infinite")
		result = Type::INFINITE_WORLD;
	else if (type == "Toric")
		result = Type::TORIC_WORLD;
	else
		return false;
	return true;
}

bool WorldBase::TimeIntegrationMethodFromString(const std::string &method, WorldBase::TimeIntegrationMethod& result)
{
	if (method == "Euler")
		result = TimeIntegrationMethod::EULER;
	else if (method == "Verlet")
		result = TimeIntegrationMethod::VERLET;
	else
		return false;
	return true;
}

WorldBase::WorldBase(WorldBase::Type type) : type_(type)
{
	time_ = 0;
	frameNr_ = 0;
	agentRemoved_ = false; 
	timeIntegrationMethod_ = TimeIntegrationMethod::EULER;
	agentKDTree = nullptr;
	SetNumberOfThreads(8);
	nextUnusedAgentID = 0;
	nextUnusedObstacleID = 0;
}

void WorldBase::SetNumberOfThreads(int nrThreads)
{
	omp_set_num_threads(nrThreads);
}

void WorldBase::computeNeighboringAgents_Flat(const Vector2D& position, float search_radius, const Agent* queryingAgent, std::vector<const Agent*>& result) const
{
	agentKDTree->FindAllAgentsInRange(position, search_radius, queryingAgent, result);
}

void WorldBase::computeNeighboringObstacles_Flat(const Vector2D& position, float search_radius, std::vector<const Obstacle*>& result) const
{
	// TODO: implement more efficient neighbor search for obstacles
	const float radiusSquared = search_radius * search_radius;

	// loop over all obstacle edges
	for (const auto& obs : obstacles_)
	{
		// check all the edges of the obstacle
		for (const auto& edge : obs->GetEdges())
		{
			// if this line segment is close enough, add it to the result if it is not already
			if (distanceToLineSquared(position, edge.first, edge.second, true) <= radiusSquared)
			{
				result.push_back(obs);
				break;
			}
		}
	}
}

bool WorldBase::IsCurrentFrameCoarseStep() const
{
	return (frameNr_ % nrFramesPerCoarseStep_ == 0);
}

void WorldBase::DoStep()
{
	/*
	OSCReceiverHandle receiver;
	UdpListeningReceiveSocket s(
		IpEndpointName(IpEndpointName::ANY_ADDRESS,
			7000),
		&receiver);
	std::cout << "press ctrl-c to end\n";
	s.RunUntilSigInt();
	GetAgent(receiver.getID())->setPosition(receiver.getPosition());
	*/

	// Before the simulation frame begins, add agents that need to be added now
	bool agentAdded = false;
	while (!agentsToAdd.empty() && agentsToAdd.top().second <= time_)
	{
		addAgentToList(agentsToAdd.top().first);
		agentsToAdd.pop();
		agentAdded = true;
	}

	int n = (int)agents_.size();

	if (timeIntegrationMethod_ == TimeIntegrationMethod::EULER)
	{
		// do all main simulation tasks
		DoStep_MainTasks(agentAdded);

		// compute new velocities and positions
	#pragma omp parallel for
		for (int i = 0; i < n; i++)
			agents_[i]->UpdateVelocityAndPosition_Euler(this);

		// override the agent positions if a specific world type demands this
		DoStep_CheckAgentPositions();
	}

	else if (timeIntegrationMethod_ == TimeIntegrationMethod::VERLET)
	{
		// update positions
#pragma omp parallel for
		for (int i = 0; i < n; ++i)
			agents_[i]->UpdatePosition_Verlet(this);

		// override the agent positions if a specific world type demands this
		DoStep_CheckAgentPositions();

		// do all main simulation tasks
		DoStep_MainTasks(agentAdded);

		// update velocities
#pragma omp parallel for
		for (int i = 0; i < (int)agents_.size(); i++)
			agents_[i]->UpdateVelocity_Verlet(this);
	}

	// increase the time that has passed
	time_ += delta_time_fine_;
	++frameNr_;

	// remove agents who have reached their goal
	agentRemoved_ = false;
	for (int i = n - 1; i >= 0; --i)
		if (agents_[i]->getRemoveAtGoal() && agents_[i]->hasReachedGoal())
			removeAgentAtListIndex(i);

	// update each interactionField 
	for (auto &field : interactionFields_) {
		field.second->updateMatrixFromFile(false);
	}


}

void WorldBase::DoStep_MainTasks(bool agentAdded)
{
	int n = (int)agents_.size();
	bool isCoarseStep = IsCurrentFrameCoarseStep();

	if (agentAdded || agentRemoved_ || isCoarseStep)
	{
		// 1. build the KD tree for nearest-neighbor computations
		if (agentKDTree != nullptr)
			delete agentKDTree;
		agentKDTree = new AgentKDTree(agents_);

		// 2. compute nearest neighbors for each agent
#pragma omp parallel for
		for (int i = 0; i < n; ++i)
			agents_[i]->ComputeNeighbors(this);
	}
	else
	{
		// 1-2 alternative: update nearest neighbors without recomputing them
#pragma omp parallel for
		for (int i = 0; i < n; ++i)
			agents_[i]->UpdateNeighborsWithoutSearching(this);

	}

	// 3. compute a preferred velocity for each agent
#pragma omp parallel for
	for (int i = 0; i < n; ++i)
		agents_[i]->ComputePreferredVelocity(this);

	// 4. compute an actual acceleration for each agent
#pragma omp parallel for
	for (int i = 0; i < n; ++i)
		agents_[i]->ComputeNextAcceleration(this);

	// 4B. compute a new viewing direction for each agent
#pragma omp parallel for
	for (int i = 0; i < n; ++i)
		agents_[i]->ComputeNextViewingDirection(this);



	
	//Compute predicted position and predicted viewing direction
#pragma omp parallel for
	for (int j = 0; j < 10; j++) {
		for (int i = 0; i < n; ++i) {
			agents_[i]->ComputePredictionPositions(this,j);
			agents_[i]->ComputePredictionViewingDirections(this, j);
		}		
	}
	

}

#pragma region [Finding, adding, and removing agents]

Agent* WorldBase::GetAgent(size_t id)
{
	// find out if the agent with this ID exists
	auto positionInList = agentPositionsInVector.find(id);
	if (positionInList == agentPositionsInVector.end())
		return nullptr;

	// return the agent who's at the correct position in the list
	return agents_[positionInList->second];
}

const Agent* WorldBase::GetAgent_const(size_t id) const
{
	// find out if the agent with this ID exists
	auto positionInList = agentPositionsInVector.find(id);
	if (positionInList == agentPositionsInVector.end())
		return nullptr;

	// return the agent who's at the correct position in the list
	return agents_[positionInList->second];
}

Obstacle* WorldBase::GetObstacle(size_t id)
{
	for (Obstacle* obstacle : obstacles_)
		if (obstacle->getID() == id)
			return obstacle;

	return nullptr;
}

const Obstacle* WorldBase::GetObstacle_const(size_t id) const
{
	for (Obstacle* obstacle : obstacles_)
		if (obstacle->getID() == id)
			return obstacle;

	return nullptr;
}

Agent* WorldBase::AddAgent(const Vector2D& position, const Agent::Settings& settings, size_t desiredID, float startTime)
{
	// determine the right ID for the agent
	size_t agentID;
	if (desiredID != std::numeric_limits<size_t>::max() && GetAgent(desiredID) == nullptr) // custom ID specified by the user; use it only if it's not already taken
		agentID = desiredID;
	else // determine a good ID automatically
		agentID = nextUnusedAgentID;

	// create the agent and set its position
	Agent* agent = new Agent(agentID, settings);
	agent->setPosition(position);

	// initilaze the future positions and orientations
	agent->InitializePredictions(10);
	//agent->setFuturePosition1(position);
	//agent->setFuturePosition2(position);
	//agent->setFuturePosition3(position);
	//agent->setFutureOrientation1(agent->getViewingDirection());
	//agent->setFutureOrientation2(agent->getViewingDirection());
	//agent->setFutureOrientation3(agent->getViewingDirection());
	
	// if the new ID is the highest one so far, update the next available ID
	if (agentID >= nextUnusedAgentID)
		nextUnusedAgentID = agentID + 1;

	// if desired, add the agent immediately
	if (startTime <= time_)
		addAgentToList(agent);

	// otherwise, schedule the agent for insertion at a later time
	else
		agentsToAdd.push({ agent, startTime });

	return agent;
}

void WorldBase::addAgentToList(Agent* agent)
{
	// add the agent to the list, and store where in the list it is located
	agentPositionsInVector[agent->getID()] = agents_.size();
	agents_.push_back(agent);
}

bool WorldBase::RemoveAgent(size_t id)
{
	// find out if the agent with this ID exists
	auto positionInList = agentPositionsInVector.find(id);
	if (positionInList == agentPositionsInVector.end())
		return false;

	removeAgentAtListIndex(positionInList->second);
	return true;
}

void WorldBase::removeAgentAtListIndex(size_t index)
{
	const auto removedAgentID = agents_[index]->getID();
	
	// if the agent is at the end of the list, simply remove it
	if (index + 1 == agents_.size())
	{
		agents_.pop_back();
		agentPositionsInVector.erase(removedAgentID);
	}
	
	// if the agent is not at the end of the list, do some extra management
	else
	{
		// delete the requested agent
		delete agents_[index];

		// move the last agent in the list to the position that has now become free
		auto lastAgent = agents_.back();
		agents_[index] = lastAgent;
		agents_.pop_back();

		// update the position map:
		// - the requested agent is now gone
		agentPositionsInVector.erase(removedAgentID);
		// - another agent has moved
		agentPositionsInVector[lastAgent->getID()] = index;
	}

	agentRemoved_ = true;
}

#pragma endregion

#pragma region [Policies]

bool WorldBase::AddPolicy(Policy* policy)
{
	// if a policy with this ID already existed, don't add the policy
	if (policies_.find(policy->GetID()) != policies_.end())
		return false;

	// store the policy
	policies_[policy->GetID()] = policy;
	return true;
}

Policy* WorldBase::GetPolicy(int id)
{
	auto findIt = policies_.find(id);
	if (findIt == policies_.end())
		return nullptr;
	return findIt->second;
}

#pragma endregion

void WorldBase::SetDeltaTime(float dt_coarse, float dt_fine)
{
	delta_time_coarse_ = dt_coarse;
	delta_time_fine_ = dt_fine;
	nrFramesPerCoarseStep_ = (int)round(dt_coarse / dt_fine);

	// check policies: if they explicitly refer to the coarse/fine timestep, update their deltaTime values
	for (auto& idAndPolicy : policies_)
		idAndPolicy.second->SynchronizeDeltaTimes(dt_coarse, dt_fine);
}

void WorldBase::AddObstacle(size_t desiredID, Obstacle* obstacle)
{
	size_t obstacleID;
	if (desiredID != std::numeric_limits<size_t>::max() && GetObstacle(desiredID) == nullptr) // custom ID specified by the user; use it only if it's not already taken
		obstacleID = desiredID;
	else // determine a good ID automatically
		obstacleID = nextUnusedObstacleID;

	// if the new ID is the highest one so far, update the next available ID
	if (obstacleID >= nextUnusedObstacleID)
		nextUnusedObstacleID = obstacleID + 1;
	obstacle->setID(obstacleID);
	obstacles_.push_back(obstacle);
}

bool WorldBase::AddInteractionField(int id, InteractionField* intField)
{
	// if a IF with this ID already existed, don't add the IF
	if (interactionFields_.find(id) != interactionFields_.end())
		return false;

	// store the IF
	interactionFields_[id] = intField;
	return true;
}

InteractionField* WorldBase::GetInteractionField(int id)
{
	auto findIt = interactionFields_.find(id);
	if (findIt == interactionFields_.end())
		return nullptr;
	return findIt->second;
}

WorldBase::~WorldBase()
{
	// delete the KD tree
	if (agentKDTree != nullptr)
		delete agentKDTree;

	// delete all agents
	for (Agent* agent : agents_)
		delete agent;
	agents_.clear();

	// delete all obstacles
	for (Obstacle* obstacle : obstacles_)
		delete obstacle;
	obstacles_.clear();

	// delete all policies
	for (auto& policy : policies_)
		delete policy.second;
	policies_.clear();

	// delete all interaction fields
	for (auto& interactionField : interactionFields_)
		delete interactionField.second;
	interactionFields_.clear();

	// delete all agents that were scheduled for insertion
	while (!agentsToAdd.empty())
	{
		delete agentsToAdd.top().first;
		agentsToAdd.pop();
	}

	// delete the mapping from IDs to agents
	agentPositionsInVector.clear();
	nextUnusedAgentID = 0;
	nextUnusedObstacleID = 0;
}

/*#pragma region [OSC communication - should be (re)moved]

//---------------------------------
//	OSC
//--------------------------------- 

#include "oscpack/osc/OscReceivedElements.h" 
#include "oscpack/osc/OscPacketListener.h" 
#include "oscpack/osc/OscOutboundPacketStream.h" 
#include "oscpack/ip/UdpSocket.h" 
#include "oscpack/ip/IpEndpointName.h"


void WorldBase::sendOSCMessage(int id, float x, float y)
{
	// Set IPAddress and Port
	const std::string ipAddress = "127.0.0.1";
	//const std::string ipAddress = "131.154.17.21";
	//const std::string ipAddress = "10.0.0.2";
	const int port = 7000;

	UdpTransmitSocket transmitSocket(IpEndpointName(ipAddress.c_str(), port));
	//Buffer
	char buffer[6144];
	osc::OutboundPacketStream p(buffer, 6144);
	p << osc::BeginBundleImmediate
		//Head
		<< osc::BeginMessage("/position")<< id << x << y << osc::EndMessage
		<< osc::EndBundle;
	transmitSocket.Send(p.Data(), p.Size());

}

void WorldBase::sendOSCMessageVector(std::vector<Vector2D, float> positions)
{
	
	// Set IPAddress and Port
//const std::string ipAddress = "127.0.0.1";
	const std::string ipAddress = "131.154.17.21";
	const int port = 7000;

	UdpTransmitSocket transmitSocket(IpEndpointName(ipAddress.c_str(), port));
	//Buffer
	char buffer[6144];
	osc::OutboundPacketStream p(buffer, 6144);
	p << osc::BeginBundleImmediate
		<< osc::BeginMessage("/position")<< positions[0][0]<< positions[0][1]<< positions[1][0] << positions[1][1] << osc::EndMessage
		<< osc::EndBundle;

	transmitSocket.Send(p.Data(), p.Size());
	

}

#pragma endregion*/
