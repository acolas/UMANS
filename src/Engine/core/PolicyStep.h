/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettr�
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#ifndef LIB_POLICYSTEP_H
#define LIB_POLICYSTEP_H

#include <tools/vector2D.h>

class WorldBase;
class Agent;

/// <summary>One step of a navigation policy that agents can use for local navigation.</summary>
/// <remarks>In each frame of the simulation loop, an agent uses a *policy* to compute an acceleration vector to apply in the upcoming frame.
/// In turn, a policy can consist of multiple *steps* that are performed in sequence.
/// Each step of a policy contains the following main ingredients:<list>
/// <item>One or more *cost functions* that assign costs to (hypothetical) velocities.</item>
/// <item>An *optimization method* that describes how to use these cost functions to compute an acceleration.</item>
/// <item>(Optionally) Additional parameters that describe how to apply this acceleration to the agent.</item></list>
/// The UMANS library can reproduce many local navigation algorithms by making particular choices for each ingredient.</remarks>
class PolicyStep
{
public:
	/// <summary>An enum describing the possible "delta time" values used by a PolicyStep.</summary>
	enum class DeltaTimeType
	{
		/// <summary>Indicates that a PolicyStep uses the coarse simulation timestep.</summary>
		COARSE,
		/// <summary>Indicates that a PolicyStep uses the fine simulation timestep.</summary>
		FINE,
		/// <summary>Indicates that a PolicyStep uses a custom "delta time" value, unrelated to the coarse and fine simulation timesteps.</summary>
		CUSTOM
	};

protected:
	DeltaTimeType deltaTimeType_;
	float deltaTime_;
	size_t id_;

	/// <summary>A "relaxation time" parameter used for interpolating between an agent's current and new velocity.
	/// Not all versions of PolicyStep use it.</summary>
	float relaxationTime_ = 0;

public:
	virtual Vector2D ComputeAcceleration(Agent* agent, WorldBase* world) { return Vector2D(0, 0); }
	virtual Vector2D ComputeNewViewingDirection(Agent* agent, WorldBase* world) { return Vector2D(0, 0); }
	virtual Vector2D ComputePredictionPosition(Agent* agent, WorldBase *world, int number) { return Vector2D(0, 0); }
	virtual Vector2D ComputePredictionViewingDirection(Agent* agent, WorldBase *world, int number) { return Vector2D(0, 0); }

	~PolicyStep() {}

	void SetID(size_t id) { id_ = id; }
	inline size_t GetID() const { return id_; }

	/// <summary>Sets this Policy's relaxation time to the given value.</summary>
	/// <param name="t">The desired new relaxation time. 
	/// Use 0 or less to let agents use their new velocity immediately.
	/// Use a higher value to let agents interpolate between their current and new velocity.</param>
	inline void setRelaxationTime(float t) { relaxationTime_ = t; }
	/// <summary>Returns the relaxation time of this Policy.</summary>
	inline float getRelaxationTime() const { return relaxationTime_; }

	/// <summary>Finds and returns the range of interaction of this PolicyStep.</summary>
	/// <remarks>Subclasses of PolicyStep should implement this method.</remarks>
	virtual float getInteractionRange() const = 0;

	inline float GetDeltaTime() const { return deltaTime_; }

	void SynchronizeDeltaTime(float newDeltaTime_coarse, float newDeltaTime_fine)
	{
		if (deltaTimeType_ == DeltaTimeType::COARSE)
			deltaTime_ = newDeltaTime_coarse;
		else if (deltaTimeType_ == DeltaTimeType::FINE)
			deltaTime_ = newDeltaTime_fine;
	}

protected:
	PolicyStep(DeltaTimeType deltaTimeType, float deltaTime)
		: deltaTimeType_(deltaTimeType), deltaTime_(deltaTime) {}

};

#endif //LIB_POLICYSTEP_H
