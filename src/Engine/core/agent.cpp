/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettr�
** 
** Permission is hereby granted, free of charge, to any person obtaining 
** a copy of this software and associated documentation files (the 
** "Software"), to deal in the Software without restriction, including 
** without limitation the rights to use, copy, modify, merge, publish, 
** distribute, sublicense, and/or sell copies of the Software, and to 
** permit persons to whom the Software is furnished to do so, subject 
** to the following conditions:
** 
** The above copyright notice and this permission notice shall be 
** included in all copies or substantial portions of the Software.
** 
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
** 
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#include <core/agent.h>
#include <core/worldBase.h>
#include <core/PolicyStep.h>
#include <core/policy.h>

using namespace std;

Agent::Agent(size_t id, const Agent::Settings& settings) :
	id_(id), settings_(settings),
	position_(0, 0),
	velocity_(0, 0),
	acceleration_(0, 0),
	goal_(0, 0),
	viewing_direction_(0, 0),
	preferred_velocity_(0, 0),
	next_acceleration_(0, 0),
	next_viewing_direction_(0, 0),
	positionAtLastCoarseTime_(0, 0),
	lastCoarseTime_(0),
	futurePosition1_(0, 0),
	futurePosition2_(0, 0),
	futurePosition3_(0, 0),
	futureViewingDirection1_(0, 0),
	futureViewingDirection2_(0, 0),
	futureViewingDirection3_(0, 0)
{
	// set the seed for random-number generation
	RNGengine_.seed((unsigned int)id);

	setPolicy(settings.policy_);
}

void Agent::setPolicy(Policy* policy)
{
	settings_.policy_ = policy;

	// prepare the list of policy step results
	size_t nrPolicySteps = settings_.policy_->GetNumberOfSteps();
	policyStepResults_.resize(nrPolicySteps);
	for (size_t i = 0; i < nrPolicySteps; ++i)
		policyStepResults_[i] = Vector2D(0, 0);
}

#pragma region [Simulation-loop methods]

void Agent::ComputeNeighbors(WorldBase* world)
{
	// get the query radius
	float range = getPolicy()->GetInteractionRange();

	// perform the query and store the result
	neighbors_ = world->ComputeNeighbors(position_, range, this);
}

void Agent::UpdateNeighborsWithoutSearching(WorldBase* world)
{
	// check if neighboring agents have disappeared from the simulation
	for (auto& neighbor : neighbors_.first)
		neighbor.UpdatePositionAndDistance(position_);
}

Vector2D Agent::runPolicySteps(const std::vector<PolicyStep*>* steps, WorldBase* world, PolicyStepResultType resultType, bool computeAverage, int number)
{
	Vector2D result(0, 0);
	size_t frameNr = world->GetCurrentFrameNumber();
	float dt_fine = world->GetDeltaTime_Fine();
	size_t nrResults = 0;

	// iterate over all policy steps
	if (steps != nullptr)
	{
		
		for (PolicyStep* step : *steps)
		{
			size_t stepID = step->GetID();

			// Check if this policy step should be executed, based on its deltaTime.
			// If so, execute the step, and store the result
			const float dt_step = step->GetDeltaTime();
			int everyNSteps = std::max(1, (int)round(dt_step / dt_fine));
			if (frameNr % everyNSteps == 0)
			{
				
				if (resultType == PolicyStepResultType::ViewingDirection)
					policyStepResults_[stepID] = step->ComputeNewViewingDirection(this, world);
				else if (resultType == PolicyStepResultType::PredictionPosition) {
					
					policyStepResults_[stepID] = step->ComputePredictionPosition(this, world, number);
				}
				else if (resultType == PolicyStepResultType::PredictionViewingDirection) {
					
					policyStepResults_[stepID] = step->ComputePredictionViewingDirection(this, world, number);
				}
				else 
					policyStepResults_[stepID] = step->ComputeAcceleration(this, world);
			}

			// add the result to a running total
			++nrResults;
			result += policyStepResults_[stepID];
		}
	}

	// if desired, average between the subresults
	if (computeAverage && nrResults > 1)
		result = result / (float)nrResults;

	return result;
}

void Agent::ComputePreferredVelocity(WorldBase* world)
{
	if (isUserControlled())
		return;

	// try to compute an acceleration towards a new preferred velocity
	const Vector2D& preferred_acceleration = runPolicySteps(settings_.policy_->GetStepsForPhase("preferred velocity"), world, PolicyStepResultType::Acceleration, true, 0);
	
	// apply the result
	preferred_velocity_ = velocity_ + preferred_acceleration * world->GetDeltaTime_Fine();
}

void Agent::ComputeNextAcceleration(WorldBase* world)
{
	next_acceleration_ = runPolicySteps(settings_.policy_->GetStepsForPhase("acceleration"), world, PolicyStepResultType::Acceleration, false, 0);
	

}


void Agent::ComputePredictionPositions(WorldBase* world, int numberPred)
{
	if (isUserControlled()) {
		if (numberPred == 0) {
			positions_[numberPred] = position_ + velocity_ * 0.33;
		}
		else {
			positions_[numberPred] = positions_[numberPred - 1] + velocity_ * 0.33;
		}
		
	}
	else {
		positions_[numberPred] = runPolicySteps(settings_.policy_->GetStepsForPhase("prediction positions"), world, PolicyStepResultType::PredictionPosition, false, numberPred);
		Vector2D vector = positions_[numberPred];
	}

}

void Agent::ComputeNextViewingDirection(WorldBase* world)
{
	if (isUserControlled())
		return;

	// try to compute a new viewing direction
	next_viewing_direction_ = runPolicySteps(settings_.policy_->GetStepsForPhase("viewing direction"), world, PolicyStepResultType::ViewingDirection, false,0);
	//erase all viewing direction predictions for this frame

	

	// if no viewing direction could be computed, resort to a default implementation
	if (next_viewing_direction_.isZero())
	{
		// weighted average of the preferred and actual velocity. Use it only if it is decisive enough
		const Vector2D& vDiff = (2 * velocity_ + preferred_velocity_) / 3;
		if (!preferred_velocity_.isZero() && vDiff.sqrMagnitude() > 0.01)
			next_viewing_direction_ = vDiff.getnormalized();

		// otherwise, use the agent's previous viewing direction, if it exists
		else if (!viewing_direction_.isZero())
			next_viewing_direction_ = viewing_direction_;

		// otherwise, use the velocity itself
		else
			next_viewing_direction_ = velocity_.getnormalized();
	}

}

void Agent::ComputePredictionViewingDirections(WorldBase* world, int numberPred)
{

	Vector2D view;
	if (isUserControlled()) {
		if (numberPred == 0) {
			view = next_viewing_direction_;
		}
		else {
			view = (positions_[numberPred] - positions_[numberPred - 1]).getnormalized();
		}
		
	}
		
	else {
		Vector2D pred = runPolicySteps(settings_.policy_->GetStepsForPhase("prediction viewing direction"), world, PolicyStepResultType::PredictionViewingDirection, false, numberPred);
		if (pred.isZero()) {
			if (numberPred == 0) {
				view = next_viewing_direction_;
			}
			else {
				view = (positions_[numberPred] - positions_[numberPred - 1]).getnormalized();
			}
		}
			
		else
			view = pred;
	}
	orientations_[numberPred] = view;
}



void Agent::UpdateVelocityAndPosition_Euler(WorldBase* world)
{
	// if the agent must follow a predefined trajectory, do this and ignore everything else
	if (!predefinedTrajectory_.empty())
	{
		updateVelocityAndPosition_PredefinedPath(world);
		updateViewingDirection();
		return;
	}
	const float dt = world->GetDeltaTime_Fine();

	// clamp the acceleration
	acceleration_ = clampVector(next_acceleration_, getMaximumAcceleration());

	// integrate the velocity; clamp to a maximum speed
	velocity_ = clampVector(velocity_ + (acceleration_*dt), getMaximumSpeed());

	// update the position
	position_ += velocity_ * dt;

	updateViewingDirection();
}

void Agent::UpdatePosition_Verlet(WorldBase* world)
{
	const float dt = world->GetDeltaTime_Fine();
	position_ += velocity_ * dt + acceleration_ * (0.5f * dt * dt);
}

void Agent::UpdateVelocity_Verlet(WorldBase* world)
{
	const float dt = world->GetDeltaTime_Fine();

	// clamp the acceleration
	next_acceleration_ = clampVector(next_acceleration_, getMaximumAcceleration());

	// integrate the velocity based on the previous and current "a"; clamp to a maximum speed
	velocity_ = clampVector(velocity_ + (acceleration_ + next_acceleration_)*0.5f*dt, getMaximumSpeed());

	// store the acceleration for the next step
	acceleration_ = next_acceleration_;

	updateViewingDirection();
}

void Agent::InitializePredictions(int size)
{
	positions_ = vector<Vector2D>(size);
	orientations_ = vector<Vector2D>(size);
}


void Agent::updateVelocityAndPosition_PredefinedPath(WorldBase* world)
{
	// update the iterator along the agent's predefined trajectory
	const double currentTime = world->GetCurrentTime();
	const float dt = world->GetDeltaTime_Fine();

	while (trajectory_iterator_ + 1 != predefinedTrajectory_.end() && (trajectory_iterator_ + 1)->time < currentTime)
		++trajectory_iterator_;

	// find the exact point on the trajectory for the current time
	Vector2D targetPos, targetDir;
	if (trajectory_iterator_ == predefinedTrajectory_.end() || trajectory_iterator_ + 1 == predefinedTrajectory_.end())
	{
		// end of the trajectory
		targetPos = predefinedTrajectory_.back().position;
		targetDir = predefinedTrajectory_.back().orientation;
	}
	else
	{
		// interpolated point on the trajectory
		const Vector2D& p1 = trajectory_iterator_->position;
		const Vector2D& p2 = (trajectory_iterator_ + 1)->position;
		double t1 = trajectory_iterator_->time;
		double t2 = (trajectory_iterator_ + 1)->time;

		float frac = (float)((currentTime - t1) / (t2 - t1));
		targetPos = (1 - frac) * p1 + frac * p2;

		const Vector2D& d1 = trajectory_iterator_->orientation;
		const Vector2D& d2 = (trajectory_iterator_ + 1)->orientation;

		targetDir = ((1 - frac) * d1 + frac * d2).getnormalized();
	}

	// snap the agent to this position
	velocity_ = (targetPos - position_) / dt;
	if(!velocity_.isZero())
		viewing_direction_ = velocity_.getnormalized();

	position_ = targetPos;
	setViewingDirection(targetDir);
}


void Agent::updateViewingDirection()
{
	viewing_direction_ = next_viewing_direction_;
}

#pragma endregion

#pragma region [Advanced getters]

bool Agent::hasReachedGoal() const
{
	return (goal_ - position_).sqrMagnitude() <= 0.25*getRadius() * getRadius();

}

#pragma endregion

#pragma region [Basic setters]

void Agent::setPosition(const Vector2D &position)
{
	position_ = position;
}

void Agent::setPosition(const Vector2D &position, double currentTime)
{
	position_ = position;
	positionAtLastCoarseTime_ = position;
	lastCoarseTime_ = currentTime;

}



void Agent::setFuturePosition1(Vector2D position1)
{
	futurePosition1_ = position1;

	
}
void Agent::setFuturePosition2(Vector2D position2)
{
	futurePosition2_ = position2;


}
void Agent::setFuturePosition3(Vector2D position3)
{
	futurePosition3_ = position3;


}



void Agent::setFutureOrientation1(Vector2D orientation1)
{
	futureViewingDirection1_ = orientation1;


}
void Agent::setFutureOrientation2(Vector2D orientation2)
{
	futureViewingDirection2_ = orientation2;


}
void Agent::setFutureOrientation3(Vector2D orientation3)
{
	futureViewingDirection3_ = orientation3;


}


void Agent::setVelocity_ExternalApplication(const Vector2D &velocity, const Vector2D &viewingDirection)
{
	velocity_ = velocity;
	viewing_direction_ = viewingDirection;
	// TODO: is this still sufficient, now that we have an acceleration-based simulation loop?
}

void Agent::setGoal(const Vector2D &goal)
{
	goal_ = goal;

	/*// look straight towards the goal
	if (goal_ != position_)
		viewing_direction_ = (goal_ - position_).getnormalized();*/
}

void Agent::setPreferredVelocity(const Vector2D& prefVelocity)
{
	preferred_velocity_ = prefVelocity;
}


void Agent::setViewingDirection(const Vector2D& dir)
{
	if (!dir.isZero())
		next_viewing_direction_ = dir.getnormalized();
}

#pragma endregion

float Agent::ComputeRandomNumber(float min, float max)
{
	auto distribution = std::uniform_real_distribution<float>(min, max);
	return distribution(RNGengine_);
}


bool Agent::setClock(int id, double clock)
{
	// store the IF
	IFclocks_[id] = clock;
	return true;
}

double Agent::getClock(int id)
{
	auto findIt = IFclocks_.find(id);
	if (findIt == IFclocks_.end())
		return 0;
	return findIt->second;
}

void Agent::setTrajectory(const Trajectory& traj)
{
	predefinedTrajectory_ = traj;
	trajectory_iterator_ = predefinedTrajectory_.begin();
	setPosition(traj.front().position);
	setViewingDirection(traj.front().orientation);
	setGoal(traj.back().position);
}

bool Agent::isUserControlled() const
{
	return settings_.policy_->GetID() == 9999;
}