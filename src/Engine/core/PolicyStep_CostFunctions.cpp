/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettré
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#include <core/PolicyStep_CostFunctions.h>

#include <core/costFunction.h>
#include <core/agent.h>

#include <algorithm>

PolicyStep_CostFunctions::~PolicyStep_CostFunctions()
{
	// delete all cost functions
	for (auto& costFunction : cost_functions_)
		delete costFunction.first;
	cost_functions_.clear();
}

float PolicyStep_CostFunctions::getInteractionRange() const
{
	float range = 0;
	for (const auto& costFunction : cost_functions_)
		range = std::max(range, costFunction.first->GetRange());

	return range;
}

Vector2D PolicyStep_CostFunctions::ComputeAcceleration(Agent* agent, WorldBase * world)
{
	// if the agent should stop moving, return a deceleration vector
	if (stopAtGoal_ && agent->hasReachedGoal())
		return -agent->getVelocity() / deltaTime_;
	
	// otherwise, compute a new acceleration using a certain optimization method

	// a) Gradient following: compute acceleration from cost-function gradients
	if (optimizationMethod_ == OptimizationMethod::GRADIENT)
		return getAccelerationFromGradient(agent, world);

	// b) Global optimization or sampling

	// - compute the ideal velocity according to the cost functions
	const Vector2D& bestVelocity = (optimizationMethod_ == OptimizationMethod::GLOBAL
		? getBestVelocityGlobal(agent, world)
		: getBestVelocitySampling(agent, world, samplingParameters_));

	// - convert this to an acceleration using a relaxation time
	//   Note: the relaxation time should be at least the length of a frame.
	return (bestVelocity - agent->getVelocity()) / std::max(relaxationTime_, deltaTime_);
}

Vector2D PolicyStep_CostFunctions::ComputePredictionPosition(Agent * agent, WorldBase * world, int indexPrediction)
{
		return (agent->getPosition() + agent->getVelocity()*std::max(relaxationTime_, deltaTime_));

}

Vector2D PolicyStep_CostFunctions::getAccelerationFromGradient(Agent* agent, WorldBase* world)
{
	const Vector2D& CurrentVelocity = agent->getVelocity();

	// sum up the gradient of all cost functions
	Vector2D TotalGradient(0, 0);
	for (auto& costFunction : cost_functions_)
		TotalGradient += costFunction.second * costFunction.first->GetGradientFromCurrentVelocity(agent, world);

	// move in the opposite direction of this gradient
	return -1 * TotalGradient;
}

Vector2D PolicyStep_CostFunctions::getBestVelocityGlobal(Agent* agent, WorldBase * world)
{
	// Note: True global optimization only works if this policy has a single cost function, 
	// because it requires a closed-form solution that has to be implemented per function.
	// If no such closed-form solution is given (or if there are multiple cost functions), we have to resort to sampling.

	return cost_functions_.size() == 1
		? cost_functions_[0].first->GetGlobalMinimum(agent, world)
		: getBestVelocitySampling(agent, world, SamplingParameters::ApproximateGlobalOptimization());
}

Vector2D PolicyStep_CostFunctions::getBestVelocitySampling(Agent* agent, WorldBase * world, const SamplingParameters& params)
{
	return ApproximateGlobalMinimumBySampling(agent, world, params, cost_functions_, deltaTime_);
}

Vector2D PolicyStep_CostFunctions::ApproximateGlobalMinimumBySampling(Agent* agent, const WorldBase* world,
	const SamplingParameters& params, const CostFunctionList& costFunctions, const float dt)
{
	// --- Compute the range in which samples will be taken.

	// compute the base of the cone, or center of the circle
	Vector2D base(0, 0);
	if (params.base == SamplingParameters::Base::ZERO) base = Vector2D(0, 0);
	else if (params.base == SamplingParameters::Base::CURRENT_VELOCITY) base = agent->getVelocity();

	// compute the radius of the cone/circle
	float radius;
	if (params.radius == SamplingParameters::Radius::PREFERRED_SPEED) radius = agent->getPreferredSpeed();
	else if (params.radius == SamplingParameters::Radius::MAXIMUM_SPEED) radius = agent->getMaximumSpeed();
	else if (params.radius == SamplingParameters::Radius::MAXIMUM_ACCELERATION)
		radius = std::min(2.0f*agent->getMaximumSpeed(), agent->getMaximumAcceleration() * dt);

	// compute the base direction (a unit vector)
	Vector2D baseDirection(1, 0);
	if (params.baseDirection == SamplingParameters::BaseDirection::UNIT) baseDirection = Vector2D(1, 0);
	else if (params.baseDirection == SamplingParameters::BaseDirection::CURRENT_VELOCITY) baseDirection = agent->getVelocity().getnormalized();
	else if (params.baseDirection == SamplingParameters::BaseDirection::PREFERRED_VELOCITY) baseDirection = agent->GetPreferredVelocity().getnormalized();

	// compute the maximum angle to the base direction, in radians
	float maxAngle = (float)(params.angle / 360.0 * PI); // params.angle stores the full range (in deg); we want half of it (in rad)

	// --- Option 1: Random sampling

	Vector2D bestVelocity(0, 0);
	float bestCost = MaxFloat;

	if (params.type == SamplingParameters::Type::RANDOM)
	{
		for (int i = 0; i < params.randomSamples; ++i)
		{
			// create a random velocity in the cone/circle
			float randomAngle = agent->ComputeRandomNumber(-maxAngle, maxAngle);
			float randomLength = agent->ComputeRandomNumber(0, radius);
			const Vector2D& velocity = base + rotateCounterClockwise(baseDirection, randomAngle) * randomLength;

			// compute the cost for this velocity
			float totalCost = 0;
			for (auto& costFunction : costFunctions)
				totalCost += costFunction.second * costFunction.first->GetCost(velocity, agent, world);

			// check if this cost is better than the minimum so far
			if (totalCost < bestCost)
			{
				bestVelocity = velocity;
				bestCost = totalCost;
			}
		}
	}

	// --- Option 2: Regular sampling

	else if (params.type == SamplingParameters::Type::REGULAR)
	{
		// compute the difference in angle and length per iteration
		const float startAngle = -maxAngle;
		const float endAngle = maxAngle;
		const float deltaAngle = (endAngle - startAngle) / (params.angle == 360 ? params.angleSamples : (params.angleSamples - 1));
		const float deltaLength = radius / (params.includeBaseAsSample ? (params.speedSamples - 1) : params.speedSamples);

		// speed samples
		for (int s = 1; s <= params.speedSamples; ++s)
		{
			const float candidateLength = deltaLength * (params.includeBaseAsSample ? s - 1 : s);
			float candidateAngle = startAngle;

			// angle samples
			for (int a = 0; a < params.angleSamples; ++a, candidateAngle += deltaAngle)
			{
				// construct the candidate velocity
				const Vector2D& velocity = base + rotateCounterClockwise(baseDirection, candidateAngle) * candidateLength;

				// compute the cost for this velocity
				float totalCost = 0;
				for (auto& costFunction : costFunctions)
					totalCost += costFunction.second * costFunction.first->GetCost(velocity, agent, world);

				// check if this cost is better than the minimum so far
				if (totalCost < bestCost)
				{
					bestVelocity = velocity;
					bestCost = totalCost;
				}

				// if we are currently checking the base velocity, we don't have to sample any more angles
				if (params.includeBaseAsSample && s == 1)
					break;
			}
		}
	}

	// --- Return the velocity with the lowest cost

	return bestVelocity;
}

void PolicyStep_CostFunctions::AddCostFunction(CostFunction* costFunction, const CostFunctionParameters &params)
{
	float coefficient = 1;
	params.ReadFloat("coeff", coefficient);
	costFunction->parseParameters(params);
	cost_functions_.push_back({ costFunction, coefficient });
}
bool PolicyStep_CostFunctions::OptimizationMethodFromString(const std::string &method, PolicyStep_CostFunctions::OptimizationMethod& result)
{
	if (method == "gradient")
		result = OptimizationMethod::GRADIENT;
	else if (method == "sampling")
		result = OptimizationMethod::SAMPLING;
	else if (method == "global")
		result = OptimizationMethod::GLOBAL;
	else
		return false;
	return true;
}

bool PolicyStep_CostFunctions::SamplingParameters::BaseFromString(const std::string &method, SamplingParameters::Base& result)
{
	if (method == "zero")
		result = Base::ZERO;
	else if (method == "current velocity")
		result = Base::CURRENT_VELOCITY;
	else
		return false;
	return true;
}

bool PolicyStep_CostFunctions::SamplingParameters::BaseDirectionFromString(const std::string &method, SamplingParameters::BaseDirection& result)
{
	if (method == "unit")
		result = BaseDirection::UNIT;
	else if (method == "current velocity")
		result = BaseDirection::CURRENT_VELOCITY;
	else if (method == "preferred velocity")
		result = BaseDirection::PREFERRED_VELOCITY;
	else
		return false;
	return true;
}

bool PolicyStep_CostFunctions::SamplingParameters::TypeFromString(const std::string &method, SamplingParameters::Type& result)
{
	if (method == "regular")
		result = Type::REGULAR;
	else if (method == "random")
		result = Type::RANDOM;
	else
		return false;
	return true;
}

bool PolicyStep_CostFunctions::SamplingParameters::RadiusFromString(const std::string &method, SamplingParameters::Radius& result)
{
	if (method == "preferred speed")
		result = Radius::PREFERRED_SPEED;
	else if (method == "maximum speed")
		result = Radius::MAXIMUM_SPEED;
	else if (method == "maximum acceleration")
		result = Radius::MAXIMUM_ACCELERATION;
	else
		return false;
	return true;
}
