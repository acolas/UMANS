/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettr�
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#ifndef LIB_AGENT_NEIGHBOR_H
#define LIB_AGENT_NEIGHBOR_H

#include <tools/vector2D.h>

class Agent;
class Obstacle;

/// <summary>A reference to an agent in the simulation, possibly with a transposed position.
/// Nearest-neighbor queries in WorldBase return results of this type.</summary>
struct PhantomAgent
{
private:
	/// <summary>The amount by which the neighboring Agent's position should be offset, e.g. to account for wrap-around in a toric world.</summary>
	Vector2D positionOffset;
	Vector2D position;
	float distSqr;

public:
	/// <summary>A pointer to the original Agent in the simulation.</summary>
	const Agent* realAgent;

	/// <summary>Creates a PhantomAgent with the given details.</summary>
	/// <param name="agent">A pointer to the original Agent in the simulation.</param>
	/// <param name="queryPosition">The query position used for finding neighbors; used for precomputing the distance to this PhantomAgent.</param>
	/// <param name="posOffset">The amount by which the neighboring Agent's position should be offset, e.g. to account for wrap-around in a toric world.</param>
	PhantomAgent(const Agent* agent, const Vector2D& queryPosition, const Vector2D& posOffset)
		: realAgent(agent), positionOffset(posOffset)
	{
		UpdatePositionAndDistance(queryPosition);
	}

	PhantomAgent() : realAgent(nullptr) {}

	/// <summary>Returns the (precomputed) position of this neighboring agent, translated by the offset of this PhantomAgent.</summary>
	inline Vector2D GetPosition() const { return position; }
	/// <summary>Returns the velocity of this neighboring agent.</summary>
	// TODO: remove
	Vector2D GetVelocity() const;
	/// <summary>Returns the (precomputed) squared distance from this PhantomAgent to the query position that was used to find it.</summary>
	inline float GetDistanceSquared() const { return distSqr; }

	/// <summary>Pre-computes (or re-computes) the translated position of this PhantomAgent, as well as its distance to a query point.
	/// Use this method if you want to correct the PhantomAgent's data in a new frame, without having search the AgentKDTree again.</summary>
	void UpdatePositionAndDistance(const Vector2D& queryPosition);

};

typedef std::vector<PhantomAgent> AgentNeighborList;
typedef std::vector<const Obstacle*> ObstacleNeighborList;
typedef std::pair<AgentNeighborList, ObstacleNeighborList> NeighborList;

#endif // LIB_AGENT_NEIGHBOR_H