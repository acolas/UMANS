//---------------------------------
//	OSC
//--------------------------------- 
#include <cstdlib>
#include <cstring>
#include <iostream>

#include ".\3rd-party\oscpack\osc\OscReceivedElements.h" 
#include ".\3rd-party\oscpack\osc\OscPacketListener.h" 
#include ".\3rd-party\oscpack\osc\OscOutboundPacketStream.h" 
#include ".\3rd-party\oscpack\ip\UdpSocket.h" 
#include ".\3rd-party\oscpack\ip\UdpSocket.h"
#include "vector2D.h"

namespace osc{

class OSCReceiverHandle : public osc::OscPacketListener {
private:
	Vector2D position_;
	int id_;
public:
	inline Vector2D getPosition(){ return position_; };
	inline int getID() { return id_; };

	OSCReceiverHandle() {}
	~OSCReceiverHandle() {}
protected:
	bool isOSCReceiveBegan = false;
	virtual void ProcessMessage(const osc::ReceivedMessage& m,
		const IpEndpointName& remoteEndpoint)
	{
		ReceivedMessageArgumentStream args = m.ArgumentStream();
		(void)remoteEndpoint;
		try {
			if (strcmp(m.AddressPattern(), "/position") == 0) {
				// if address is "/position"
				// parse an expected format using the argument stream interface:

				float x;
				float y;
				args >> x >> y >> osc::EndMessage;

				std::cout << "received '/position' message with arguments: "
					<< x << " " << y << "\n";
				position_ = Vector2D(x, y);

			}
			else if (strcmp(m.AddressPattern(), "/id") == 0) {
				// if address is "/id"
				// parse an expected format using the argument stream interface:
				osc::int32 id;
				args >> id >> osc::EndMessage;

				std::cout << "received '/id' message with arguments: "
					<< id << "\n";
				id_ = id;

			}
		}
		catch (osc::Exception& e) {
			std::cout << "OSC Receive: Error while parsing message: "
				<< m.AddressPattern() << ": " << e.what() << "\\n";
		}
	}
};
} // namespace osc