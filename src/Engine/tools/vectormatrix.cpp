
#include <tools/vectormatrix.h>

#include <iostream>

using namespace std;

VectorMatrix::VectorMatrix(int rows, int cols) : rows_(rows), cols_(cols)
{
    allocSpace();
}

VectorMatrix::VectorMatrix() : rows_(1), cols_(1)
{
    allocSpace();
}

VectorMatrix::~VectorMatrix()
{
    for (int i = 0; i < rows_; ++i)
        delete[] mat[i];
    delete[] mat;
}

void VectorMatrix::setAt(int i, int j,const Vector2D &vect)
{
    if (i>rows_ || i<0 || j>cols_ || j<0 ){
        throw std::out_of_range("Out of range value set VectorMatrix::setAt");
    }
    else{
        mat[i][j] = vect;
    }

}
/*
VectorMatrix::VectorMatrix(const VectorMatrix& m) : rows_(m.rows_), cols_(m.cols_)
{
    allocSpace();
    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            mat[i][j] = m.mat[i][j];

        }
    }
}

VectorMatrix& VectorMatrix::operator=(const VectorMatrix& m)
{
    if (this == &m) {
        return *this;
    }

    if (rows_ != m.rows_ || cols_ != m.cols_) {
        for (int i = 0; i < rows_; ++i) {
            delete[] mat[i];
        }
        delete[] mat;

        rows_ = m.rows_;
        cols_ = m.cols_;
        allocSpace();
    }

    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            mat[i][j] = m.mat[i][j];
        }
    }
    return *this;
}


*/

VectorMatrix& VectorMatrix::operator+=(const VectorMatrix& m)
{
    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            mat[i][j] += m.mat[i][j];
        }
    }
    return *this;
}

VectorMatrix& VectorMatrix::operator-=(const VectorMatrix& m)
{
    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            mat[i][j] -= m.mat[i][j];
        }
    }
    return *this;
}



VectorMatrix& VectorMatrix::operator*=(float num)
{
    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            mat[i][j] = mat[i][j]*num;
        }
    }
    return *this;
}

VectorMatrix& VectorMatrix::operator/=(float num)
{
    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            mat[i][j] = mat[i][j]/num;
        }
    }
    return *this;
}

// Access the individual elements

//Vector2D& VectorMatrix::operator()(const unsigned& row, const unsigned& col) {
//  return this->mat[row][col];
//}

//// Access the individual elements (const)

//const Vector2D& VectorMatrix::operator()(const unsigned& row, const unsigned& col) const {
//  return this->mat[row][col];
//}

void VectorMatrix::swapRows(int r1, int r2)
{
    Vector2D *temp = mat[r1];
    mat[r1] = mat[r2];
    mat[r2] = temp;
}

VectorMatrix VectorMatrix::transpose()
{
    VectorMatrix ret(cols_, rows_);
    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            ret.mat[j][i] = mat[i][j];
        }
    }
    return ret;
}



double VectorMatrix::dotProduct( const VectorMatrix &a,const  VectorMatrix &b)
{
    double sum = 0;
    for (int i = 0; i < a.rows_; ++i) {
        sum += (a.at(i, 0).dot(b.at(i, 0)));
    }
    return sum;
}




int VectorMatrix::getRows() const
{
	return rows_;
}

int VectorMatrix::getCols() const
{
	return cols_;
}

bool VectorMatrix::isNul() const
{
	for (int i = 0; i < rows_; i++)
		for (int j = 0; j < cols_; j++)
			if (!mat[i][j].isZero())
				return false;

	return true;
}

bool VectorMatrix::isInside(float x, float y) const
{
	return (x >= -0.5f && y >= -0.5f && x <= cols_ - 0.5f && y <= rows_ - 0.5f);
}

/* PRIVATE HELPER FUNCTIONS
 ********************************/

void VectorMatrix::allocSpace()
{
	mat = new Vector2D*[rows_];
	for (int i = 0; i < rows_; ++i)
	{
		mat[i] = new Vector2D[cols_];
		for (int j=0; j<cols_; ++j)
			mat[i][j] = Vector2D(0, 0);
	}
}



/* NON-MEMBER FUNCTIONS
 ********************************/

VectorMatrix operator+(const VectorMatrix& m1, const VectorMatrix& m2)
{
    VectorMatrix temp(m1);
    return (temp += m2);
}

VectorMatrix operator-(const VectorMatrix& m1, const VectorMatrix& m2)
{
    VectorMatrix temp(m1);
    return (temp -= m2);
}



VectorMatrix operator*(const VectorMatrix& m, float num)
{
    VectorMatrix temp(m);
    return (temp *= num);
}



VectorMatrix operator/(const VectorMatrix& m, float num)
{
    VectorMatrix temp(m);
    return (temp /= num);
}
