#ifndef VECTORMATRIX_H
#define VECTORMATRIX_H

#include <vector>
#include <array>
#include <tools/vector2D.h>

class VectorMatrix
{
public:
	VectorMatrix(int rows, int columns);

	//VectorMatrix(const VectorMatrix &mat);
	VectorMatrix();
	~VectorMatrix();

	void setAt(int i, int j, const Vector2D &);

	//VectorMatrix& operator =(const VectorMatrix&);


	//inline Vector2D& operator()(int x, int y) { return mat[x][y]; }

	inline const Vector2D& at(int i, int j) const { return mat[i][j]; }

	VectorMatrix& operator+=(const VectorMatrix&);
	VectorMatrix& operator-=(const VectorMatrix&);
	VectorMatrix& operator*=(float);
	VectorMatrix& operator/=(float);

	//    Vector2D& operator()(const unsigned& row, const unsigned& col);
	//    const Vector2D& operator()(const unsigned& row, const unsigned& col)const;

	void swapRows(int, int);
	VectorMatrix transpose();



	// functions on vectors
	static double dotProduct(const VectorMatrix &,const VectorMatrix &);

	int getRows() const;
	int getCols() const;

	bool isNul() const;

	bool isInside(float x, float y) const;

	//void setSource(const std::array<int, 2> &value);


private:
	int rows_;
	int cols_;
	bool start;
	Vector2D **mat;
	std::string tag;
	void allocSpace();

	//std::array<int, 2> source;
};


VectorMatrix operator+(const VectorMatrix&, const VectorMatrix&);
VectorMatrix operator-(const VectorMatrix&, const VectorMatrix&);
VectorMatrix operator*(const VectorMatrix&, float);
//VectorMatrix operator*(float, const VectorMatrix&);
VectorMatrix operator/(const VectorMatrix&, float);


#endif // VECTORMATRIX_H
