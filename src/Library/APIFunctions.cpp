/* UMANS: Unified Microscopic Agent Navigation Simulator
** MIT License
** Copyright (C) 2018-2020  Inria Rennes Bretagne Atlantique - Rainbow - Julien Pettré
**
** Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject
** to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
** OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
** LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** Contact: crowd_group@inria.fr
** Website: https://project.inria.fr/crowdscience/
** See the file AUTHORS.md for a list of all contributors.
*/

#define DLL_EXPORT

#include "APIFunctions.h"

#include <core/crowdSimulator.h>
#include <algorithm>
#include <omp.h>
#include <cstring>
#include <string>
#include <clocale>
#include <iostream>

extern "C" 
{
	CrowdSimulator* cs;
	AgentData* agentData;
	size_t agentDataSize;

	void resizeAgentData(bool deleteOldData = true)
	{
		if (deleteOldData)
			delete[] agentData;
		agentDataSize = (size_t)pow(2, 1 + (int)ceil(log2(cs->GetWorld()->GetAgents().size())));
		agentData = new AgentData[agentDataSize];
	}

	API_FUNCTION bool StartSimulation(const char* configFileName, int numberOfThreads)
	{
    	omp_set_num_threads(numberOfThreads);

		// initialize a new crowd simulation; we'll fill it with the contents of the given config file
		cs = CrowdSimulator::FromConfigFile(configFileName);
		if (cs == nullptr)
		{
			return false;
		}

		cs->GetWorld()->SetNumberOfThreads(numberOfThreads);

		// prepare the agentData array
		resizeAgentData(false);

		return true;
	}

	API_FUNCTION bool GetSimulationTimeStep(float& result_dt)
	{
		if (cs == nullptr)
			return false;

		result_dt = cs->GetWorld()->GetDeltaTime_Coarse();
		return true;
	}

	API_FUNCTION bool SetSimulationTimeStep(float dt)
	{
		if (cs == nullptr)
			return false;

		float dt_fine = cs->GetWorld()->GetDeltaTime_Fine();
		cs->GetWorld()->SetDeltaTime(dt, dt_fine);	
		return true;	
	}

	API_FUNCTION bool DoSimulationSteps(int nrSteps)
	{
		if (cs == nullptr)
			return false;

		cs->RunCoarseSimulationSteps(nrSteps);
		return true;
	}

	API_FUNCTION bool GetAgentPositions(AgentData*& result_agentData, int& result_nrAgents)
	{
		if (cs == nullptr)
			return false;

		const auto& agents = cs->GetWorld()->GetAgents();

		// check if the AgentData array is large enough; resize it if necessary
		if (agents.size() > agentDataSize || agents.size() * 4 < agentDataSize)
			resizeAgentData(true);

		// fill the AgentData array with the current agent data
#pragma omp parallel for
		for (int i = 0; i < (int)agents.size(); ++i)
		{
			agentData[i].id = (int)agents[i]->getID();
			agentData[i].position_x = agents[i]->getPosition().x;
			agentData[i].position_y = agents[i]->getPosition().y;
			agentData[i].velocity_x = agents[i]->getVelocity().x;
			agentData[i].velocity_y = agents[i]->getVelocity().y;
			agentData[i].viewingDirection_x = agents[i]->getViewingDirection().x;
			agentData[i].viewingDirection_y = agents[i]->getViewingDirection().y;
			agentData[i].position1_x = agents[i]->getFuturePos1().x;
			agentData[i].position1_y = agents[i]->getFuturePos1().y;
			agentData[i].position2_x = agents[i]->getFuturePos2().x;
			agentData[i].position2_y = agents[i]->getFuturePos2().y;
			agentData[i].position3_x = agents[i]->getFuturePos3().x;
			agentData[i].position3_y = agents[i]->getFuturePos3().y;
			agentData[i].viewingDirection1_x = agents[i]->getFutureOrient1().x;
			agentData[i].viewingDirection1_y = agents[i]->getFutureOrient1().y;
			agentData[i].viewingDirection2_x = agents[i]->getFutureOrient2().x;
			agentData[i].viewingDirection2_y = agents[i]->getFutureOrient2().y;
			agentData[i].viewingDirection3_x = agents[i]->getFutureOrient3().x;
			agentData[i].viewingDirection3_y = agents[i]->getFutureOrient3().y;
		}

		// store references to these results, for the client program to use
		result_agentData = agentData;
		result_nrAgents = (int)agents.size();

		return true;
	}

	API_FUNCTION bool SetAgentPositions(AgentData* agentData, int nrAgents)
	{
#pragma omp parallel for
		for (int i = 0; i < nrAgents; ++i)
		{
			// find the agent with the given ID
			auto agent = cs->GetWorld()->GetAgent(agentData[i].id);
			if (agent != nullptr)
			{
				Vector2D position(agentData[i].position_x, agentData[i].position_y);
				Vector2D velocity(agentData[i].velocity_x, agentData[i].velocity_y);
				Vector2D viewingDirection(agentData[i].viewingDirection_x, agentData[i].viewingDirection_y);

				// override the agent's position, velocity, and viewing direction
				agent->setPosition(position);
				agent->setVelocity_ExternalApplication(velocity, viewingDirection);
			}

		}
		return true;
	}

	API_FUNCTION bool AddAgent(float x, float y, float radius, float prefSpeed, float maxSpeed, float maxAcceleration, int policyID, int& result_id, int group, int customID)
	{
		if (cs == nullptr)
			return false;

		// fill in the agent's settings
		Agent::Settings settings;
		settings.radius_ = radius;
		settings.preferred_speed_ = prefSpeed;
		settings.max_speed_ = maxSpeed;
		settings.max_acceleration_ = maxAcceleration;
		settings.policy_ = cs->GetWorld()->GetPolicy(policyID);
		settings.group_ = group;
	

		// if the policy could not be found, then we cannot add the agent
		if (settings.policy_ == nullptr)
			return false;

		// try to add an agent
		auto agent = cs->GetWorld()->AddAgent(
			Vector2D(x, y), 
			settings,
			(customID >= 0 ? customID : std::numeric_limits<size_t>::max()));
		
		if (agent != nullptr)
		{
			// fill in the ID that the agent received
			result_id = (int)agent->getID();

			// set the agent's goal to its current position, so that it doesn't start moving
			agent->setGoal(agent->getPosition());
		}


		return (agent != nullptr);
	}

	API_FUNCTION bool RemoveAgent(int id)
	{
		if (cs == nullptr)
			return false;

		// try to remove the agent
		return cs->GetWorld()->RemoveAgent(id);
	}

	API_FUNCTION bool SetAgentGoal(int id, float x, float y)
	{
		if (cs == nullptr)
			return false; 
		
		// try to find the agent
		auto* agent = cs->GetWorld()->GetAgent(id);
		if (agent == nullptr)
			return false;
		
		agent->setGoal(Vector2D(x, y));
		return true;
	}

	API_FUNCTION bool SetAgentPolicy(int agentID, int policyID)
	{
		if (cs == nullptr)
			return false;

		// try to find the agent
		auto* agent = cs->GetWorld()->GetAgent(agentID);
		if (agent == nullptr)
			return false;

		// try to find the policy
		auto* policy = cs->GetWorld()->GetPolicy(policyID);
		if (policy == nullptr)
			return false;

		agent->setPolicy(policy);
		return true;
	}

	API_FUNCTION bool GetAgentColor(int id, int& result_r, int& result_g, int& result_b)
	{
		if (cs == nullptr)
			return false;

		// try to find the agent
		auto* agent = cs->GetWorld()->GetAgent(id);
		if (agent == nullptr)
			return false;

		// get the color
		const Color& color = agent->getColor();
		result_r = color.r;
		result_g = color.g;
		result_b = color.b;

		return true;
	}

	API_FUNCTION bool SetAgentColor(int id, int r, int g, int b)
	{
		if (cs == nullptr)
			return false;

		// try to find the agent
		auto* agent = cs->GetWorld()->GetAgent(id);
		if (agent == nullptr)
			return false;

		// set the color
		agent->setColor(Color((unsigned short)r, (unsigned short)g, (unsigned short)b));

		return true;
	}

	API_FUNCTION bool CleanUp()
	{
		if (cs == nullptr)
			return false;

		delete cs;
		cs = nullptr;
		delete[] agentData;
		agentData = nullptr;

		return true;
	}

    API_FUNCTION bool AddObstacle(float* points, int nbr_points)
	{
		if (cs == nullptr)
			return false;

		std::vector<Vector2D> poly(nbr_points);

		int j = 0;
		for(int i = 0; i < nbr_points; i++)
		{
			Vector2D p = Vector2D(points[j], points[j+1]);
			poly[i] = p;
			j = j+2;
		}
		
		// try to add an wall obstacle
		size_t obstacleID = std::numeric_limits<size_t>::max();
		Obstacle* obstacle = new Obstacle(obstacleID, poly);
		cs->GetWorld()->AddObstacle(obstacleID, obstacle);
		
		return true;
	}




	/// --------------------
///	CROWDMP api function
/// --------------------
	CrowdSimulator* UMANS_CreateSimObject(const char* configFileName, int numberOfThreads) {
		try {
			std::setlocale(LC_NUMERIC, "en_US.UTF-8");
			omp_set_num_threads(numberOfThreads);

			// initialize a new crowd simulation; we'll fill it with the contents of the given config file
			CrowdSimulator* sim = CrowdSimulator::FromConfigFile(configFileName);
			if (sim == nullptr)
			{
				return NULL;
			}

			sim->GetWorld()->SetNumberOfThreads(numberOfThreads);

			return sim;
		}
		catch (const std::exception& e) {
			return NULL;
		}
	}

	void UMANS_DestroySimObject(CrowdSimulator* obj) {
		if (obj == nullptr)
			return;
		delete obj;
	}

	void UMANS_doStep(CrowdSimulator* obj, float deltaTime) {
		if (obj == nullptr)
			return;

		try {
			obj->GetWorld()->SetDeltaTime(deltaTime, deltaTime);
			obj->RunCoarseSimulationSteps(1);
		}
		catch (...) {
			std::cerr << "do step error" << std::endl;

		}

		return;
	}

	void UMANS_addObstacle(CrowdSimulator* obj, float s_startx, float s_starty, float s_endx, float s_endy) {
		if (obj == nullptr)
			return;
	}


	bool UMANS_removeAgent(CrowdSimulator* obj, int s_indPedestrian) {

		return obj->GetWorld()->RemoveAgent(s_indPedestrian);
	}

	int UMANS_addAgent(CrowdSimulator* obj, float x, float y, float radius, float prefSpeed, float maxSpeed, float maxAcceleration, int policyID, int group, float goalX, float goalY) {
		if (obj == nullptr)
			return -1;

		try {
			// fill in the agent's settings
			Agent::Settings settings;
			settings.radius_ = radius;
			settings.preferred_speed_ = prefSpeed;
			settings.max_speed_ = maxSpeed;
			settings.max_acceleration_ = maxAcceleration;
			settings.policy_ = obj->GetWorld()->GetPolicy(policyID);
			settings.group_=group;

			if (settings.policy_ == nullptr)
				return -1;

			// try to add an agent
			auto agent = obj->GetWorld()->AddAgent(
				Vector2D(x, y),
				settings,
				std::numeric_limits<size_t>::max());

			if (agent != nullptr)
			{
				// set the agent's goal to its current position, so that it doesn't start moving
				agent->setGoal(Vector2D(goalX, goalY));
				return agent->getID();
			}
		}
		catch (const std::exception& e) {
		}

		return -1;
	}

	int UMANS_addObst(CrowdSimulator* obj, float* Px, float* Py, int numPoint) {
		if (obj == nullptr)
			return -1;

		try {
			std::vector<Vector2D> obst;

			for (int i = 0; i < numPoint; i++) {
				obst.push_back(Vector2D(Px[i], Py[i]));
			}

			;
			Obstacle* pObst = new Obstacle(1, obst);
			obj->GetWorld()->AddObstacle(1, pObst);

			const std::vector<Obstacle*> test = obj->GetWorld()->GetObstacles();
			return test.size();
		}
		catch (const std::exception& e) {
		}

		return -1;
	}

	bool UMANS_IFtoAgent(CrowdSimulator* obj, int s_indPedestrian, int IF, int* linkedAgent, int numLinkedAgent, int* linkedObst, int numLinkedObst) {
		if (obj == nullptr)
			return false;

		try {
			// find the agent with the given ID
			auto agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				// interaction fields
				IFWithLinkObjects result;
				if (obj->FromID_loadSingleInteractionFieldReference(IF, result)) {
					for (int i = 0; i < numLinkedAgent; i++) {
						obj->FromID_assignAgentToIF(result, linkedAgent[i]);
					}
					for (int i = 0; i < numLinkedObst; i++) {
						obj->FromID_assignObstToIF(result, linkedObst[i]);
					}

					if (result.field->getIsVelocity())
						agent->addInteractionFieldVelocity(result);
					else
						agent->addInteractionFieldOrientation(result);
				}


				return true;
			}
		}
		catch (const std::exception& e) {
		}

		return false;
	}

	bool UMANS_IFtoObst(CrowdSimulator* obj, int s_indObst, int IF, int* linkedAgent, int numLinkedAgent, int* linkedObst, int numLinkedObst) {
		if (obj == nullptr)
			return false;

		try {
			// find the agent with the given ID
			auto obst = obj->GetWorld()->GetObstacle(s_indObst);
			if (obst != nullptr)
			{
				// interaction fields
				IFWithLinkObjects result;
				if (obj->FromID_loadSingleInteractionFieldReference(IF, result)) {
					for (int i = 0; i < numLinkedAgent; i++) {
						obj->FromID_assignAgentToIF(result, linkedAgent[i]);
					}
					for (int i = 0; i < numLinkedObst; i++) {
						obj->FromID_assignObstToIF(result, linkedObst[i]);
					}

					if (result.field->getIsVelocity())
						obst->addInteractionFieldVelocity(result);
					else
						obst->addInteractionFieldOrientation(result);
				}




				return true;
			}
		}
		catch (const std::exception& e) {
		}

		return false;
	}



	bool UMANS_setPosition(CrowdSimulator* obj, int s_indPedestrian, float s_x, float s_y) {
		if (obj == nullptr)
			return false;

		try {
			// find the agent with the given ID
			auto agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				Vector2D position(s_x, s_y);
				// override the agent's position, velocity, and viewing direction
				agent->setPosition(position);
				return true;
			}
		}
		catch (const std::exception& e) {
		}

		return false;
	}

	bool UMANS_setOrientation(CrowdSimulator* obj, int s_indPedestrian, float s_x, float s_y) {
		if (obj == nullptr)
			return false;

		try {
			// find the agent with the given ID
			auto agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				Vector2D orientation(s_x, s_y);
				// override the agent's position, velocity, and viewing direction
				agent->setViewingDirection(orientation);
				return true;
			}
		}
		catch (const std::exception& e) {
		}

		return false;
	}

	bool UMANS_setVelocity(CrowdSimulator* obj, int s_indPedestrian, float s_x, float s_y) {
		if (obj == nullptr)
			return false;

		try {
			// find the agent with the given ID
			auto agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				Vector2D velocity(s_x, s_y);

				// override the agent's position, velocity, and viewing direction
				agent->setVelocity_ExternalApplication(velocity, agent->getViewingDirection());
				return true;
			}
		}
		catch (const std::exception& e) {
		}

		return false;
	}

	bool UMANS_setGoal(CrowdSimulator* obj, int s_indPedestrian, float s_x, float s_y, float speed) {
		if (obj == nullptr)
			return false;

		try {
			// try to find the agent
			auto* agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				agent->setGoal(Vector2D(s_x, s_y));
				agent->setPreferredSpeed(speed);
				return true;
			}
		}
		catch (const std::exception& e) {
		}
		return false;
	}

	float UMANS_getAgentPositionX(CrowdSimulator* obj, int s_indPedestrian) {
		if (obj == nullptr)
			return 0;

		try {
			// try to find the agent
			auto* agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				return agent->getPosition().x;
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}
	float UMANS_getAgentPositionY(CrowdSimulator* obj, int s_indPedestrian) {
		if (obj == nullptr)
			return 0;

		try {
			// try to find the agent
			auto* agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				return agent->getPosition().y;
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}
	float UMANS_getAgentVelX(CrowdSimulator* obj, int s_indPedestrian) {
		if (obj == nullptr)
			return 0;
		try {
			// try to find the agent
			auto* agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				return agent->getVelocity().x;
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}
	float UMANS_getAgentVelY(CrowdSimulator* obj, int s_indPedestrian) {
		if (obj == nullptr)
			return 0;
		try {
			// try to find the agent
			auto* agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				return agent->getVelocity().y;
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}

	API_FUNCTION float UMANS_getAgentOrientationX(CrowdSimulator * obj, int s_indPedestrian)
	{
		if (obj == nullptr)
			return 0;
		try {
			// try to find the agent
			auto* agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				return agent->getViewingDirection().x;
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}
	
	API_FUNCTION float UMANS_getAgentOrientationY(CrowdSimulator * obj, int s_indPedestrian)
	{
		if (obj == nullptr)
			return 0;
		try {
			// try to find the agent
			auto* agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				return agent->getViewingDirection().y;
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}

	API_FUNCTION float UMANS_getAgentPosPredictionX(CrowdSimulator * obj, int s_indPedestrian, int numPred) {
		if (obj == nullptr)
			return 0;
		try {
			// try to find the agent
			auto* agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				return agent->getFuturePos(numPred).x;
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}

	API_FUNCTION float UMANS_getAgentPosPredictionY(CrowdSimulator * obj, int s_indPedestrian, int numPred) {
		if (obj == nullptr)
			return 0;
		try {
			// try to find the agent
			auto* agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				return agent->getFuturePos(numPred).y;
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}

	API_FUNCTION float UMANS_getAgentOrientPredictionX(CrowdSimulator * obj, int s_indPedestrian, int numPred) {
		if (obj == nullptr)
			return 0;
		try {
			// try to find the agent
			auto* agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				return agent->getFutureOrient(numPred).x;
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}

	API_FUNCTION float UMANS_getAgentOrientPredictionY(CrowdSimulator * obj, int s_indPedestrian, int numPred) {
		if (obj == nullptr)
			return 0;
		try {
			// try to find the agent
			auto* agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				return agent->getFutureOrient(numPred).y;
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}
	
	API_FUNCTION int UMANS_getLengthVelocityInteractionFields(CrowdSimulator * obj, int s_indPedestrian, bool isAgent)
	{
		if (obj == nullptr)
			return 0;
		try {
			// try to find the source
			IFSource* source;
			if (isAgent) {
				source = obj->GetWorld()->GetAgent(s_indPedestrian);
			}
			else {
				source = obj->GetWorld()->GetObstacle(s_indPedestrian);
			}
			if (source != nullptr)
			{
				return size(source->getInteractionFieldsVelocity());
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}

	API_FUNCTION int UMANS_getLengthOrientationInteractionFields(CrowdSimulator * obj, int s_indPedestrian, bool isAgent)
	{
		if (obj == nullptr)
			return 0;
		try {
			// try to find the source
			IFSource* source;
			if (isAgent) {
				source = obj->GetWorld()->GetAgent(s_indPedestrian);
			}
			else {
				source = obj->GetWorld()->GetObstacle(s_indPedestrian);
			}
			if (source!= nullptr)
			{
				return size(source->getInteractionFieldsOrientation());
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}

	API_FUNCTION int UMANS_getIDAgentOrientationInteractionFields(CrowdSimulator * obj, int s_indPedestrian, int numberField )
	{
		if (obj == nullptr)
			return 0;
		try {
			// try to find the agent
			auto* agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			if (agent != nullptr)
			{
				auto* InteractionField = agent->getInteractionFieldsOrientation()[numberField].field;

			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}

	API_FUNCTION float UMANS_fieldsVelocityParameterValue(CrowdSimulator * obj, int s_indPedestrian, int IDField, bool isAgent)
	{
		if (obj == nullptr)
			return 0;
		try {
			// try to find the source
			IFSource* source;
			if (isAgent) {
				source = obj->GetWorld()->GetAgent(s_indPedestrian);
			}
			else {
				source = obj->GetWorld()->GetObstacle(s_indPedestrian);
			}
			
			if (source!=nullptr)
			{
				//agent->getInteractionFieldsVelocity()[IDField].field->getParameterValue(agent, obj->GetWorld(), agent->getInteractionFieldsVelocity()[IDField].linkObjects)
				auto field = source->getInteractionFieldsVelocity()[IDField];
				auto result = field.field->getParameterValue(source, obj->GetWorld(), field.linkObjects);
				return result;
			}

		}
		catch (const std::exception& e) {
			
		}
		return 0;
	}

	API_FUNCTION float UMANS_fieldsOrientationParameterValue(CrowdSimulator * obj, int s_indPedestrian, int IDField, bool isAgent)
	{
		if (obj == nullptr)
			return 0;
		try {
			// try to find the agent
			IFSource* source;
			if (isAgent) {
				source = obj->GetWorld()->GetAgent(s_indPedestrian);
			}
			else {
				source = obj->GetWorld()->GetObstacle(s_indPedestrian);
			}
			
			if (source != nullptr)
			{

				return source->getInteractionFieldsOrientation()[IDField].field->getParameterValue(source, obj->GetWorld(), source->getInteractionFieldsOrientation()[IDField].linkObjects);
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}

	API_FUNCTION float UMANS_fieldsOrientationAngleValue(CrowdSimulator * obj, int s_indPedestrian, int IDField, bool isAgent)
	{
		if (obj == nullptr)
			return 0;
		try {
			// try to find the agent
			IFSource* source;
			if (isAgent) {
				source = obj->GetWorld()->GetAgent(s_indPedestrian);
			}
			else {
				source = obj->GetWorld()->GetObstacle(s_indPedestrian);
			}
			if (source != nullptr)
			{
			

			return source->getInteractionFieldsOrientation()[IDField].field->getRotationAngle(source, source->getInteractionFieldsOrientation()[IDField].linkObjects, obj->GetWorld());

					
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}


	API_FUNCTION float UMANS_fieldsVelocityAngleValue(CrowdSimulator * obj, int s_indPedestrian, int IDField, bool isAgent)
	{
		if (obj == nullptr)
			return 0;
		try {
			// try to find the agent
			IFSource* source;
			if (isAgent) {
				source = obj->GetWorld()->GetAgent(s_indPedestrian);
			}
			else {
				source = obj->GetWorld()->GetObstacle(s_indPedestrian);
			}
			if (source != nullptr)
			{


			return source->getInteractionFieldsVelocity()[IDField].field->getRotationAngle(source, source->getInteractionFieldsVelocity()[IDField].linkObjects, obj->GetWorld());

			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}



	API_FUNCTION int UMANS_fieldsOrientationID(CrowdSimulator * obj, int s_indPedestrian, int numberField, bool isAgent)
	{
		if (obj == nullptr)
			return 0;
		try {
			// try to find the source
			IFSource* source;
			if (isAgent) {
				source = obj->GetWorld()->GetAgent(s_indPedestrian);
			}
			else {
				source = obj->GetWorld()->GetObstacle(s_indPedestrian);
			}
			if (source != nullptr)
			{

				return source->getInteractionFieldsOrientation()[numberField].field->getID();
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}

	API_FUNCTION int UMANS_fieldsVelocityID(CrowdSimulator * obj, int s_indPedestrian, int numberField, bool isAgent)
	{
		if (obj == nullptr)
			return 0;
		try {
			// try to find the source
			IFSource* source;
			if (isAgent) {
				source = obj->GetWorld()->GetAgent(s_indPedestrian);
			}
			else {
				source = obj->GetWorld()->GetObstacle(s_indPedestrian);
			}
			if (source != nullptr)
			{

				return source->getInteractionFieldsVelocity()[numberField].field->getID();
			}
		}
		catch (const std::exception& e) {
		}
		return 0;
	}

	API_FUNCTION float UMANS_fieldParameterValue(CrowdSimulator * obj, int s_indPedestrian, int IDField)
	{
		if (obj == nullptr)
			return 0;
		try {
			// try to find the agent
			auto* agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			for (const IFWithLinkObjects InteractionField: agent->getInteractionFieldsVelocity()) {
				if (IDField == InteractionField.field->getID() ) {
					return InteractionField.field->getParameterValue(agent, obj->GetWorld(), InteractionField.linkObjects);
				}
			}
			for (const IFWithLinkObjects InteractionField : agent->getInteractionFieldsOrientation()) {
				if (IDField == InteractionField.field->getID()) {
					return InteractionField.field->getParameterValue(agent,obj->GetWorld(), InteractionField.linkObjects);
				}
			}
			
		}
		catch (const std::exception& e) {
		}
		return 0;
	}
	API_FUNCTION float UMANS_fieldAngleValue(CrowdSimulator * obj, int s_indPedestrian, int IDField)
	{
		if (obj == nullptr)
			return 0;
		try {
			// try to find the agent
			auto* agent = obj->GetWorld()->GetAgent(s_indPedestrian);
			for (const IFWithLinkObjects InteractionField : agent->getInteractionFieldsVelocity()) {
				if (IDField == InteractionField.field->getID()) {
					return InteractionField.field->getRotationAngle(agent,InteractionField.linkObjects, obj->GetWorld());
				}
			}
			for (const IFWithLinkObjects InteractionField : agent->getInteractionFieldsOrientation()) {
				if (IDField == InteractionField.field->getID()) {
					return InteractionField.field->getRotationAngle(agent, InteractionField.linkObjects, obj->GetWorld());
				}
			}

		}
		catch (const std::exception& e) {
		}
		return 0;
	}
}
